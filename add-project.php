<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>

<link rel="stylesheet" href="vendor/select2/dist/css/select2.min.css">

<link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<style type="text/css">
.error{ color: red; }
</style>


			<div class="site-content">
				<!-- Content -->
				<div class="content-area py-1">
					<div class="container-fluid">
						<h4> Create a Project</h4>
						<ol class="breadcrumb no-bg mb-1">
							<li class="breadcrumb-item"><a href="#">Home </a></li>
							<!-- <li class="breadcrumb-item"><a href="#">Forms</a></li> -->
							<li class="breadcrumb-item active">Create a Project</li>
						</ol>


						<div id="animated_image"></div>
						<div id="result_container"></div>

						<div class="box box-block bg-white">
							<!-- <h5>Create new project</h5> -->
							<!-- <p class="font-90 text-muted mb-1">To use, add <code>.material-*</code> to the form.</p> -->
							<form id="new_project" name="new_project" class="form-material material-primary">

								<div class="form-group row">
									<label for="project_division" class="col-sm-2 col-form-label">Divison </label>
									<div class="col-sm-4">
										<select id="select2-demo-1" name="project_division" class="form-control project_division" data-plugin="select2" data-placeholder="Select Divison" required>
											<option value=""> --Select-- </option>

											<?php 

												$sql_fetch_division = mysql_query("SELECT * FROM `$gd`.`project_division_dropdown`  ", $connect_db);

												if(mysql_num_rows($sql_fetch_division) > 0 ) {

													while ($row_fetch_division = mysql_fetch_array($sql_fetch_division)) {
												   	extract($row_fetch_division);
											?>

													<option value="<?php echo $project_division; ?>"><?php echo $project_division; ?></option>

												<?php } // END OF WHILE ?>

											<?php } else { ?>

													<option value=""> Please add a Division </option>

											<?php } ?>

										</select>
									</div>

								</div>



								<div class="form-group row">
									<label for="input_2" class="col-sm-2 col-form-label"> Order Number </label>
									<div class="col-sm-4">
										<input type="text" class="form-control" name="order_number" placeholder="IN00001" required>
									</div>
								</div>



								<div class="form-group row">
									<label for="project_name" class="col-sm-2 col-form-label"> Location </label>
									<div class="col-sm-4">
										<select id="select2-demo-1" name="location" class="form-control" data-plugin="select2" data-placeholder="Select Location" required>
											<option value=""> --Select-- </option>

											<?php 

												$sql_fetch_location = mysql_query("SELECT * FROM `$gd`.`location_dropdown`  ", $connect_db);

												if(mysql_num_rows($sql_fetch_location) > 0 ) {

													while ($row_fetch_location = mysql_fetch_array($sql_fetch_location)) {
												   	extract($row_fetch_location);
											?>

													<option value="<?php echo $country; ?>"><?php echo $country; ?></option>

												<?php } // END OF WHILE ?>

											<?php } else { ?>

													<option value=""> Please add a Country </option>

											<?php } ?>

										</select>
									</div>

								</div>





 								<div class="form-group row">
									<label for="input_3" class="col-sm-2 col-form-label"> Request From </label>
									<div class="col-sm-4">
										<input type="text" class="form-control" name="request_from" placeholder="Request From" required>
									</div>
								</div> 





								<div class="form-group row">
									<label for="project_name" class="col-sm-2 col-form-label">Project Title </label>
									<div class="col-sm-4">
										<input type="text" class="form-control" name="project_name" required>
									</div>
								</div>






								<div class="form-group row">
									<label for="select2-demo-1" class="col-sm-2 form-control-label"> Project Type  </label>
									<div class="col-sm-4">
										<select id="select2-demo-1" name="project_type" class="form-control" data-plugin="select2" data-placeholder="Select Project Type" required>
											<option value=""> --Select-- </option>

											<?php 

												$sql_fetch_project_type = mysql_query("SELECT * FROM `$gd`.`project_type_dropdown`  ", $connect_db);

												if(mysql_num_rows($sql_fetch_project_type) > 0 ) {

													while ($row_fetch_project_type = mysql_fetch_array($sql_fetch_project_type)) {
												   	extract($row_fetch_project_type);
											?>

													<option value="<?php echo $project_type; ?>"><?php echo $project_type; ?></option>

												<?php } // END OF WHILE ?>

											<?php } else { ?>

													<option value=""> Please add an Project Type </option>

											<?php } ?>

										</select>
									</div>
								</div>





								<div class="form-group row">
									<label for="project_name" class="col-sm-2 col-form-label"> Description/Activity  </label>
									<div class="col-sm-4">
<!-- 										<input type="text" class="form-control" name="project_name" placeholder="Eg., Sap Work" required> -->
										<textarea class="form-control" cols="5" rows="3" placeholder="Description/Activity to be carried out" name="description"></textarea>
									</div>
								</div>






<!-- 								<div class="form-group row">
									<label for="select2-demo-1" class="col-sm-2 form-control-label"> Type of Work  </label>
									<div class="col-sm-4">
										<select id="select2-demo-1" name="type_of_work" class="form-control" data-plugin="select2" required>
											<option value=""> --Select-- </option>
											<?php 

												$sql_fetch_work_type = mysql_query("SELECT * FROM `$gd`.`type_of_work_dropdown`  ", $connect_db);

												if(mysql_num_rows($sql_fetch_work_type) > 0 ) {

													while ($row_fetch_work_type = mysql_fetch_array($sql_fetch_work_type)) {
												   	extract($row_fetch_work_type);
											?>

													<option value="<?php echo $type_of_work; ?>"><?php echo $type_of_work; ?></option>

												<?php } // END OF WHILE ?>

											<?php } else { ?>

													<option value=""> Please add type of Work </option>

											<?php } ?>

										</select>
									</div>
								</div> -->






								<div class="form-group row">
									<label for="select2-demo-1" class="col-sm-2 form-control-label">Assign to  </label>
									<div class="col-sm-4">
										<select id="select2-demo-1" name="assign_to[]" class="form-control project_division" data-plugin="select2" multiple data-placeholder="Select Assign To" required>
											<option value=""> --Select-- </option>

											<?php 

												$sql_fetch_emp = mysql_query("SELECT * FROM `$gd`.`employees`  ", $connect_db);

												if(mysql_num_rows($sql_fetch_emp) > 0 ) {

													while ($row_fetch_emp = mysql_fetch_array($sql_fetch_emp)) {
												   	extract($row_fetch_emp);
											?>

													<option value="<?php echo $full_name; ?>"><?php echo $full_name; ?></option>

												<?php } // END OF WHILE ?>

											<?php } else { ?>

													<option value=""> Please add a Division </option>

											<?php } ?>

										</select>
									</div>
								</div>





								<div class="form-group row">
									<label for="input_4" class="col-sm-2 col-form-label"> Order Recieved Date  </label>
									<div class="col-sm-4">
		                   					<input type="text" class="form-control mydatepicker" id="order_received_date" autocomplete="off" name="order_received_date" placeholder="dd/mm/yyyy" required>
									</div>
								</div>





								<div class="form-group row">
									<label for="select2-demo-1" class="col-sm-2 form-control-label"> Status </label>
									<div class="col-sm-4">
										<select id="select2-demo-1" name="project_status" class="form-control" data-plugin="select2" data-placeholder="Select Status" required>
											<option value=""> --Select-- </option>

											<?php 

												$sql_fetch_status = mysql_query("SELECT * FROM `$gd`.`project_status_dropdown`  ", $connect_db);

												if(mysql_num_rows($sql_fetch_status) > 0 ) {

													while ($row_fetch_status = mysql_fetch_array($sql_fetch_status)) {
												   	extract($row_fetch_status);
											?>

													<option value="<?php echo $project_status; ?>"><?php echo $project_status; ?></option>

												<?php } // END OF WHILE ?>

											<?php } else { ?>

													<option value=""> Please add a Status </option>

											<?php } ?>

										</select>
									</div>
								</div>




								<div class="form-group row">
									<label for="project_name" class="col-sm-2 col-form-label"> Comments  </label>
									<div class="col-sm-4">
										<input type="text" class="form-control" name="comments" placeholder="Eg., Sap Work" >
									</div>
								</div>



								<div class="form-group row">
									<label for="project_name" class="col-sm-2 col-form-label"> Customer Want Date  </label>
									<div class="col-sm-4">
											<input type="text" class="form-control mydatepicker" name="customer_want_date" autocomplete="off" id="customer_want_date" placeholder="dd/mm/yyyy" required>
									</div>
								</div>


								<div class="form-group row">
									<label for="input_5" class="col-sm-2 col-form-label"> Actual Completion Date </label>
									<div class="col-sm-4">
		                   					<input type="text" class="form-control mydatepicker" name="actual_completion_date" autocomplete="off" id="actual_completion_date" placeholder="dd/mm/yyyy">
									</div>
								</div>


								<div class="form-group row">
									<label for="select2-demo-1" class="col-sm-2 form-control-label"> Complexity Level </label>
									<div class="col-sm-4">
										<select id="select2-demo-1" name="complexity_level" class="form-control" data-plugin="select2" data-placeholder="Select Complexity Level" required>
											<option value=""> --Select-- </option>
											<option value="1"> 1 </option>
											<option value="2"> 2 </option>
											<option value="3"> 3 </option>


										</select>
									</div>
								</div>



								<div class="form-group row">
									<label for="input_5" class="col-sm-2 col-form-label"> Hourly Charge </label>
									<div class="col-sm-4">
		                   					<input type="text" class="form-control" name="hourly_charge" autocomplete="off" id="hourly_charge">
									</div>
								</div>


								<button type="submit" class="btn btn-primary w-min-sm mb-0-25 waves-effect waves-light">Submit</button>

							</form>
						</div>
					</div>
				</div>

<?php include $backend_footer_file; ?>

<script type="text/javascript" src="vendor/select2/dist/js/select2.min.js"></script>


<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>

<!--- new_project form validation --> 


<script>   
$(document).ready(function(){
  $("#new_project").validate({
    debug: false,
    submitHandler: function(form) {


    	scrollToTop();

      	$("#animated_image").show();
      	$("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

      	$.post('<?php echo $file_path[30]; ?>', $("#new_project").serialize() + '&addNewProject=addNewProject', function(response) {

            $('#new_project').find("input[type=text],input[type=password],input[type=email],textarea,select").val("");

			$("#animated_image").hide();
			$("#result_container").html(response);

	      	$.post("php/trigger_email.php");

      	}); // END OF POST REQUEST 


    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>


<script type="text/javascript">
	$('[data-plugin="select2"]').select2($(this).attr('data-options'));
</script>

<script type="text/javascript" src="js/forms-pickers.js"></script>

<script type="text/javascript" src="vendor/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="vendor/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script type="text/javascript" src="vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="vendor/moment/moment.js"></script>
<script type="text/javascript" src="vendor/bootstrap-daterangepicker/daterangepicker.js"></script>

<script type="text/javascript">
$('#order_received_date, #customer_want_date, #actual_completion_date').datepicker({
   todayHighlight : true,
   orientation: "left",
   autoclose: true,
   format: 'dd/mm/yyyy'
});	
</script>