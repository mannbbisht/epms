<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>


<link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<link rel="stylesheet" href="vendor/DataTables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.bootstrap4.min.css">


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.dataTables.min.css">


<!-- MULTI SELECT CSS -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.2/multiple-select.min.css">


<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" />
 -->
<style type="text/css">
.text-center{
	text-align: center;
}
.multiselect-container>li>a>label.checkbox{
	padding: 5px 10px !important;	
}
.col-md-1{
	margin: 0 !important;
	width: auto !important;
}
#report_container{
	width: auto !important;

/*	height: 800px;
	overflow-y: scroll !important;
*/
}	
#filter_option_form select{
	width: 130px !important;
}
.hide{
	display: none !important;
}
</style>



<?php 

$all_months_name = array_reverse(GetLast12MonthsName());
$all_months_number = array_reverse(GetLast12MonthsNumber());

?>



<?php /********************************************* APPLY FILTER ON PAGE SUBMIT *********************************************/ ?>




			<div class="site-content">
				<!-- Content -->
				<div class="content-area py-1">
					<div class="container-fluid">
						<h4> Offsite Charges </h4>
						<ol class="breadcrumb no-bg mb-1">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<li class="breadcrumb-item active"> Offsite Charges </li>
						</ol>




						<div id="animated_image"></div>
						<div id="result_container"></div>


						<div class="box box-block bg-white">
							<!-- <h5>Create new project</h5> -->
							<!-- <p class="font-90 text-muted mb-1">To use, add <code>.material-*</code> to the form.</p> -->

							<div id="report_container">

								<table class="table table-striped table-bordered dataTable" id="table-2">
									<thead>
										<tr>

											<th> Location  </th>

											<?php foreach ($all_months_name as $key => $value) { ?>

											<th> <?php echo $value; ?> </th>

											<?php } ?>
	


										</tr>
									</thead>
									<tbody>



										<?php 

										$sql_fetch_location = mysql_query("SELECT * FROM `$gd`.`location_dropdown` where country != 'India'   order by country asc  ", $connect_db);

											while ($row_fetch_location = (mysql_fetch_array($sql_fetch_location)) ){
											extract($row_fetch_location);	

										?>



										<tr id="<?php echo $pk_project_id; ?>">

											<td> <?php print_value($country); ?> </td>

											<?php 

											foreach ($all_months_number as $x => $y) {
											


												$sql_fetch_hours = mysql_query(" SELECT * FROM `$gd`.`activity` where country = '$country' and date_year = '$y'  ", $connect_db);

													while ($row_fetch_hours = (mysql_fetch_array($sql_fetch_hours)) ){
													extract($row_fetch_hours);	


													$sql_fetch_rates = mysql_query(" SELECT hourly_charge FROM `$gd`.`projects` where order_number = '$order_number'   ", $connect_db);
													while ($row_fetch_rates = (mysql_fetch_array($sql_fetch_rates)) ){
														extract($row_fetch_rates);	

														$final_total_2[] = $working_hours * $hourly_charge;

													}



													// $sql_fetch_rates = mysql_query(" SELECT rate FROM `$gd`.`project_rates` where project_type = '$project_type'   ", $connect_db);
													// while ($row_fetch_rates = (mysql_fetch_array($sql_fetch_rates)) ){
													// 	extract($row_fetch_rates);	

													// 	$final_total_2[] = $working_hours * $rate;

													// }

														// if($project_type == "Analysis") {

														// 	$final_total = $working_hours * $hourly_charges['Analysis'];

														// } else if($project_type == "Programming" ) {

														// 	$final_total = $working_hours * $hourly_charges['Programming'];

														// } else if($project_type == "Design" ) {

														// 	$final_total = $working_hours * $hourly_charges['Design'];

														// } else if($project_type == 'SAP/Documentation' ) {

														// 	$final_total = $working_hours * $hourly_charges['SAP/Documentation'];

														// } else if($project_type == 'Modeling / Drawing' ) {

														// 	$final_total = $working_hours * $hourly_charges['Modeling / Drawing'];

														// } else if($project_type == 'i2V' ) {

														// 	$final_total = $working_hours * $hourly_charges['i2V'];

														// }

														// $final_total_2[] = "$final_total";

												}

											?>
												
												
											<td> 

												<?php  

													$charge = number_format(array_sum($final_total_2), 2); 														

													echo " &dollar; "; 
													print_value($charge);


												?> 

											</td> 

											<!-- <td> <?php echo $record_date; ?> </td>  -->

											<?php unset($final_total_2); } ?>

										</tr>

										<?php } ?>


									</tbody>
								</table>	

							</div>


						</div> <!-- box-block -->
					</div>
				</div>

			</div>



<?php include $backend_footer_file; ?>




<script type="text/javascript">
$(document).ready(function(){
    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel ");  
});
</script>
