<?php 
include "php/global_constants.php"; 
include $dynamic_links; 
?>

<!DOCTYPE HTML>
<html lang="zxx">

<head>
  <title> Login </title>
  <!-- Meta tag Keywords -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="UTF-8" />


  <!-- Style-CSS -->
  <link href="https://demo.w3layouts.com/demos_new/template_demo/14-02-2019/latest_login_form-demo_Free/1335508922/web/css/font-awesome.min.css" rel="stylesheet">
  <!-- Font-Awesome-Icons-CSS -->
  <!-- //css files -->

  <!-- web-fonts -->
  <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
   rel="stylesheet">
  <!-- //web-fonts -->

  <style type="text/css">
    .company_logo{
      height: 50px;
    }

/*--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
--*/

/* reset */
html,
body,
div,
span,
applet,
object,
iframe,
h1,
h2,
h3,
h4,
h5,
h6,
p,
blockquote,
pre,
a,
abbr,
acronym,
address,
big,
cite,
code,
del,
dfn,
em,
img,
ins,
kbd,
q,
s,
samp,
small,
strike,
strong,
sub,
sup,
tt,
var,
b,
u,
i,
dl,
dt,
dd,
ol,
nav ul,
nav li,
fieldset,
form,
label,
legend,
table,
caption,
tbody,
tfoot,
thead,
tr,
th,
td,
article,
aside,
canvas,
details,
embed,
figure,
figcaption,
copyright,
header,
hgroup,
menu,
nav,
output,
ruby,
section,
summary,
time,
mark,
audio,
video {
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
}

article,
aside,
details,
figcaption,
figure,
copyright,
header,
hgroup,
menu,
nav,
section {
    display: block;
}

ol,
ul {
    list-style: none;
    margin: 0px;
    padding: 0px;
}

blockquote,
q {
    quotes: none;
}

blockquote:before,
blockquote:after,
q:before,
q:after {
    content: '';
    content: none;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
}

/* start editing from here */
a {
    text-decoration: none;
}

.txt-rt {
    text-align: right;
}

/* text align right */
.txt-lt {
    text-align: left;
}

/* text align left */
.txt-center {
    text-align: center;
}

/* text align center */
.float-rt {
    float: right;
}

/* float right */
.float-lt {
    float: left;
}

/* float left */
.clear {
    clear: both;
}

/* clear float */
.pos-relative {
    position: relative;
}

/* Position Relative */
.pos-absolute {
    position: absolute;
}

/* Position Absolute */
.vertical-base {
    vertical-align: baseline;
}

/* vertical align baseline */
.vertical-top {
    vertical-align: top;
}

/* vertical align top */
nav.vertical ul li {
    display: block;
}

/* vertical menu */
nav.horizontal ul li {
    display: inline-block;
}

/* horizontal menu */
img {
    max-width: 100%;
}

/* //end reset */
body {
    font-size: 100%;
    font-family: 'Source Sans Pro', sans-serif;
    overflow-y: hidden;
}

.main-bg {
    background: url(img/img3.png) no-repeat center;
    background-size: cover;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    -ms-background-size: cover;
    min-height: 100vh;
}

/* title */
h1 {
    font-size: 2.8vw;
    color: #fff;
    text-align: center;
    padding: 1.5vw 1vw 2vw;
    letter-spacing: 3px;
    text-transform: uppercase;
}

/* //title */

/* content */
.sub-main-w3 {
    margin: 1.5vw 5vw;
}

.bg-content-w3pvt {
    max-width: 400px;
    margin: 0 auto;
    background: #fff;
    text-align: center;
}

.top-content-style {
    padding: 2vw 4vw 4vw;
    background: #1cc7d0;
}

.top-content-style img {
    -webkit-border-radius: 50%;
    -o-border-radius: 50%;
    -ms-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
}

.sub-main-w3 form {
    background: #ffff;
    padding: 2em;
    -webkit-box-shadow: 2px 5px 16px 2px rgba(16, 16, 16, 0.18);
    -moz-box-shadow: 2px 5px 16px 2px rgba(16, 16, 16, 0.18);
    box-shadow: 2px 5px 16px 2px rgba(16, 16, 16, 0.18);
    margin: -2.5em 2.5em 2em;
    -webkit-border-radius: 4px;
    -o-border-radius: 4px;
    -ms-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
}

p.legend {
    color: #4e4d4d;
    font-size: 24px;
    text-align: center;
    margin-bottom: 1.2em;
}

p.legend span {
    color: #000;
    margin-left: 10px;
}

.input {
    position: relative;
    margin: 20px auto;
    width: 100%
}

.input span {
    position: absolute;
    display: block;
    color: #1cc7d0;
    left: 10px;
    top: 12px;
    font-size: 16px;
}

.input input {
    width: 100%;
    padding: 13px 10px 13px 34px;
    display: block;
    border: none;
    border: 1px solid #1cc7d0;
    color: #000;
    box-sizing: border-box;
    font-size: 13px;
    outline: none;
    letter-spacing: 1px;
    background: #fff;
    -webkit-box-shadow: 2px 5px 16px 2px rgba(16, 16, 16, 0.18);
    -moz-box-shadow: 2px 5px 16px 2px rgba(16, 16, 16, 0.18);
    box-shadow: 2px 5px 16px 2px rgba(16, 16, 16, 0.18);
}

.submit {
    width: 45px;
    height: 45px;
    display: block;
    margin: 2.5em auto 0;
    background: #1cc7d0;
    -webkit-border-radius: 10px;
    -o-border-radius: 10px;
    -ms-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    border: none;
    cursor: pointer;
    -webkit-transition: 0.5s all;
    -o-transition: 0.5s all;
    -moz-transition: 0.5s all;
    -ms-transition: 0.5s all;
    transition: 0.5s all;
}

.submit span {
    color: #fff;
    font-size: 20px;
}

.submit:hover {
    opacity: .8;
    -webkit-transition: 0.5s all;
    -o-transition: 0.5s all;
    -moz-transition: 0.5s all;
    -ms-transition: 0.5s all;
    transition: 0.5s all;
}

a.bottom-text-w3ls {
    color: #757474;
    font-size: 16px;
    display: inline-block;
    margin: 0em 1em 2em;
    letter-spacing: 1px;
}

/* //content */

/* copyright */
.copyright {
    margin-top: 3.08vw;
    padding-bottom: 1.5vw;
}

.copyright h2 {
    font-size: 16px;
    color: #fff;
    letter-spacing: 1px;
    text-align: center;
    line-height: 1.8;
}

.copyright h2 a {
    color: #1cc7d0;
    -webkit-transition: 0.5s all;
    -o-transition: 0.5s all;
    -moz-transition: 0.5s all;
    -ms-transition: 0.5s all;
    transition: 0.5s all;
}

.copyright h2 a:hover {
    opacity:.8;
    -webkit-transition: 0.5s all;
    -o-transition: 0.5s all;
    -moz-transition: 0.5s all;
    -ms-transition: 0.5s all;
    transition: 0.5s all;
}

/* //copyright */

/* responsive */
@media(max-width:1280px) {
    .top-content-style {
        padding: 3vw 4vw 5vw;
    }
}

@media(max-width:1080px) {
    h1 {
        font-size: 3.5vw;
    }
}

@media(max-width:991px) {
    h1 {
        font-size: 4vw;
    }

    .top-content-style {
        padding: 3vw 4vw 6vw;
    }
}

@media(max-width:800px) {
    h1 {
        font-size: 5vw;
        padding: 2.5vw 1vw 3vw;
    }

    .top-content-style {
        padding: 4vw 4vw 9vw;
    }

    .copyright {
        margin-top: 3em;
        padding-bottom: 1.5em;
    }
}

@media(max-width:640px) {
    h1 {
        font-size: 6vw;
        padding: 3vw 1vw 4vw;
    }
}

@media(max-width:600px) {
    .copyright h2 {
        letter-spacing: 1px;
    }
}

@media(max-width:480px) {
    h1 {
        font-size: 2em;
        letter-spacing: 1px;
    }

    .top-content-style {
        padding: 2em 1em 4em;
    }

    .copyright h2 {
        font-size: 15px;
    }

    .copyright {
        margin-top: 2em;
        padding-bottom: 1em;
    }

    p.legend {
        font-size: 23px;
    }

    a.bottom-text-w3ls {
        font-size: 15px;
    }
}

@media(max-width:384px) {
    .sub-main-w3 form {
        margin: -2.5em 2em 2em;
    }

    h1 {
        padding: 5vw 1vw 6vw;
    }
}

@media(max-width:320px) {
    h1 {
        font-size: 1.7em;
    }

    .sub-main-w3 form {
        margin: -2em 1em 1.5em;
        padding: 1.5em;
    }

    .top-content-style {
        padding: 1.5em 1em 3em;
    }

    .copyright h2 {
        font-size: 14px;
    }
}

/* //responsive */
  </style>

</head>

<body>






<body>
  <div class="main-bg">
    <!-- title -->
    <h1> </h1>
    <!-- //title -->
<!---728x90--->

    <!-- content -->
    <div class="sub-main-w3">
      <div class="bg-content-w3pvt">

        <div id="result_container"> </div>

        <div class="top-content-style">
          <img src="images/user.jpg" alt="" />
        </div>
        <form id="login_form" name="login_form">

            

        <p class="legend" <?php if ((strpos($user_ip, '27.109.9.91') !== false)) { ?> style="display: none;" <?php } ?> >  <img src="<?php echo $domain; ?>/img/company-logo.png" alt="Gardner Denver" class="company_logo" /> </p>

          <div class="input">
            <input type="email" placeholder="Email" name="email" required />
            <span class="fa fa-envelope"></span>
          </div>
          <div class="input">
            <input type="password" placeholder="Password" name="password" required />
            <span class="fa fa-unlock"></span>
          </div>
          <button type="submit" class="btn submit">
            <span class="fa fa-sign-in"></span>
          </button>
        </form>
        <a href="forgot-password.php" class="bottom-text-w3ls"> Forgot Password? </a>

        <p style="padding: 10px;color: #757474"> <?php echo date("Y"); ?> © <?php echo $company_name; ?>  </p>

      </div>
    </div>
    <!-- //content -->
<!---728x90--->


  </div>
</body>

</html>


<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>


<!-- validation libs -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>


<!--- new_project form validation --> 
<script>   
$(document).ready(function(){
  $("#login_form").validate({
    debug: false,
    submitHandler: function(form) {

        $("#animated_image").show();
        $("#result_container").empty();
        $("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

        $.post('<?php echo $file_path[30]; ?>', $("#login_form").serialize() + '&login_event=login_event', function(response) {

            $("#animated_image").hide();
            $("#result_container").html(response);


        }); // END OF POST REQUEST 

    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>





<script type="text/javascript">
$(document).ready(function(){
    $.post('email-reminder.php');
    $.post('email-reminder-admin.php');
    $.post('insert_activity.php');

    setTimeout(function() {
        $.post("php/trigger_email.php");
    }, 1000);
})
</script>


<!-- perform backup on login -->
<script type="text/javascript">
$(function() {
    $.post("<?php echo "$domain/backup"; ?>");
});
</script>



