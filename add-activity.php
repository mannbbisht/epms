<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>

<link rel="stylesheet" href="vendor/select2/dist/css/select2.min.css">


<link rel="stylesheet" href="vendor/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
<link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css">



<style type="text/css">
.error{ color: red; }
</style>


<?php

$sql_fetch_projects = mysql_query("SELECT * FROM `$gd`.`projects` where assign_to = '$session_full_name'  ", $connect_db);

?>


			<div class="site-content">
				<!-- Content -->
				<div class="content-area py-1">
					<div class="container-fluid">
						<h4> Add your activity </h4>
						<ol class="breadcrumb no-bg mb-1">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<!-- <li class="breadcrumb-item"><a href="#">Forms</a></li> -->
							<li class="breadcrumb-item active">Activity Tracker</li>
						</ol>

						<div id="animated_image"></div>
						<div id="result_container"></div>

						<div class="box box-block bg-white">
							<!-- <h5>Create new project</h5> -->
							<!-- <p class="font-90 text-muted mb-1">To use, add <code>.material-*</code> to the form.</p> -->
							<form id="new_activity" name="new_activity"  class="form-material material-primary">
								<div class="form-group row">
									<label for="select2-demo-1" class="col-sm-2 form-control-label">Choose a Project:</label>

										<?php if(mysql_num_rows($sql_fetch_projects) > 0 ) { ?>

											<div class="col-sm-4">
												<select id="select2-demo-1" name="project_title" class="form-control" data-plugin="select2" >

										 											<option value="">--select--</option>


										<?php 

													while ($row_fetch_projects = mysql_fetch_array($sql_fetch_projects)) {
												   	extract($row_fetch_projects);

										?>

														<option value="<?php echo $project_name; ?>"><?php echo $project_name; ?></option>

										<?php 		} // END OF WHILE ?>

												</select>
											</div>


										<?php } else { ?>

											<div class="col-sm-4">
												<label> Project not assigned.  </label>
											</div>

										<?php } ?>

								</div>


								<div class="form-group row">
									<label for="select2-demo-1" class="col-sm-2 form-control-label">Type of work:</label>
										<div class="col-sm-4">
											<select id="select2-demo-1" name="type_of_work" class="form-control" data-plugin="select2" >
 											<option value="">--select--</option>

											<?php 

												$sql_fetch_work = mysql_query("SELECT * FROM `$gd`.`type_of_work`  ", $connect_db);

													if(mysql_num_rows($sql_fetch_work) > 0 ) {

														while ($row_fetch_work = mysql_fetch_array($sql_fetch_work)) {
														extract($row_fetch_work);
												?>

													<option value="<?php echo $type_of_work; ?>"><?php echo $type_of_work; ?></option>

												<?php } // END OF WHILE ?>

											
												<?php } else { ?>

													<option value=""> Please add a Type of work </option>

											<?php } ?>

										</select>
									</div>
								</div>



								<div class="form-group row">
									<label for="input_2" class="col-sm-2 col-form-label"> No. of Hours </label>
									<div class="col-sm-4">
										<input type="number" class="form-control" id="working_hours" name="working_hours" placeholder="Eg., 3 hours" required onchange="verifyForm();">
									</div>
								</div>



								<div class="form-group row">
									<label for="select2-demo-1" class="col-sm-2 form-control-label"> Country </label>
										<div class="col-sm-4">
											<select id="select2-demo-1" name="country" class="form-control" data-plugin="select2" >
 											<option value="">--select--</option>

											<?php 

												$sql_fetch_countries = mysql_query("SELECT * FROM `$gd`.`location_dropdown`  ", $connect_db);

													if(mysql_num_rows($sql_fetch_countries) > 0 ) {

														while ($row_fetch_countries = mysql_fetch_array($sql_fetch_countries)) {
														extract($row_fetch_countries);
												?>

													<option value="<?php echo $country; ?>"><?php echo $country; ?></option>

												<?php } // END OF WHILE ?>

											
												<?php } else { ?>

													<option value=""> Please add a Location </option>

											<?php } ?>

										</select>
									</div>
								</div>





								<div class="form-group row">
									<label for="input_3" class="col-sm-2 col-form-label"> Activity Carried Out </label>
									<div class="col-sm-4">
										<!-- <input type="email" class="form-control" name="input_3" placeholder="Success"> -->
										<textarea class="form-control" id="activity_carried_out" name="activity_carried_out" rows="3"></textarea>
									</div>
								</div>











 								<input type="hidden" id="employee_name" name="employee_name" value="<?php echo $session_full_name; ?>" />

								<button type="submit" class="btn btn-primary w-min-sm mb-0-25 waves-effect waves-light add_disabled" disabled>Submit</button>

							</form>
						</div>
					</div>
				</div>


<?php include $backend_footer_file; ?>




<!-- validation -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>

<!--- new_project form validation --> 
<script>   
$(document).ready(function(){
  $("#new_activity").validate({
    debug: false,
    submitHandler: function(form) {

      	$("#animated_image").show();
      	$("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

      	$.post('<?php echo $file_path[30]; ?>', $("#new_activity").serialize() + '&addNewActivity=addNewActivity', function(response) {

            // $('#new_activity').find("input[type=text],input[type=password],input[type=email],textarea,select").val("");

			$("#animated_image").hide();
			$("#result_container").html(response);

			setTimeout(function(){ 
				location.reload();
			}, 2000);



      	}); // END OF POST REQUEST 


    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>



<script type="text/javascript">
<?php /*
$('#datetimepicker').datetimepicker({
	format: 'm/d/Y H:i:s',
	maxDate:'+1970/01/02'
	// weekends:['01.01.2019','02.01.2019','03.01.2019','04.01.2019','05.01.2019','06.01.2014']
});
*/ ?>
</script>






<!-- styling for drop down -->
<script type="text/javascript" src="vendor/select2/dist/js/select2.min.js"></script>
<script type="text/javascript">
	$('[data-plugin="select2"]').select2($(this).attr('data-options'));
</script>


<!-- styling for time textbox -->
<script type="text/javascript" src="vendor/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="vendor/clockpicker/dist/jquery-clockpicker.min.js"></script>

<!-- styling for date textbox -->
<script type="text/javascript" src="vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<!-- custom js -->
<!-- <script type="text/javascript" src="js/forms-pickers.js"></script> -->

<!-- custom js -->
<script type="text/javascript">
	$('#datepicker-autoclose').datepicker({
		autoclose: true,
		todayHighlight: true
	});	
</script>

<script type="text/javascript">
$("#start_time").click(function() {
	$(".clockpicker-button").html('Select');
});
</script>


<!-- custom validation -->
<script type="text/javascript">
function verifyForm() {	

	var working_hours = $("#working_hours").val();

	if(working_hours !== '') {
		$(".add_disabled").removeAttr("disabled");
	} else {
		$(".add_disabled").attr("disabled", "disabled");
	}

}
</script>


<script type="text/javascript">
$('#start_time').clockpicker({
	// placement: 'bottom',
	// align: 'left',
	autoclose: true,
	'default': 'now',
	twelvehour: false,
	donetext: 'Select'
});	
</script>

<!-- https://stackoverflow.com/questions/18640051/check-if-html-form-values-are-empty-using-javascript -->