<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>

<style type="text/css">
.error{ color: red; }
</style>


			<div class="site-content">
				<!-- Content -->
				<div class="content-area py-1">
					<div class="container-fluid">
						<h4> Add new user </h4>
						<ol class="breadcrumb no-bg mb-1">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<!-- <li class="breadcrumb-item"><a href="#">Forms</a></li> -->
							<li class="breadcrumb-item active"> Add new user </li>
						</ol>

						<div id="animated_image"></div>
						<div id="result_container"></div>

						<div class="box box-block bg-white">
							<!-- <h5>Create new project</h5> -->
							<!-- <p class="font-90 text-muted mb-1">To use, add <code>.material-*</code> to the form.</p> -->
							<form id="new_user_form" name="new_user_form">
 								<div class="form-group row">
									<label for="input_3" class="col-sm-2 col-form-label"> Full Name: </label>
									<div class="col-sm-4">
										<input type="text" class="form-control" name="full_name" required>
									</div>
								</div> 

 								<div class="form-group row">
									<label for="input_3" class="col-sm-2 col-form-label"> Email-ID: </label>
									<div class="col-sm-4">
										<input type="email" class="form-control" name="email_id" required autocomplete="no">
									</div>
								</div> 


 								<div class="form-group row">
									<label for="input_3" class="col-sm-2 col-form-label"> Default Password: </label>
									<div class="col-sm-4">
										<input type="text" id="default_password" class="form-control" value="gardner@365" readonly required>
									</div>
								</div> 


 								<div class="form-group row">
									<label for="input_3" class="col-sm-2 col-form-label">Role:</label>
									<div class="col-sm-4">
										<select id="select2-demo-1" name="role" class="form-control" data-plugin="select2" >
											<option value="admin" > Admin </option>
											<option value="user" selected> User </option>
										</select>
									</div>
								</div> 



								<button type="submit" class="btn btn-primary w-min-sm mb-0-25 waves-effect waves-light">Submit</button>

							</form>
						</div>
					</div>
				</div>



<?php include $backend_footer_file; ?>


<!-- validation libs -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>


<!--- new_user_form form validation --> 
<script>   
$(document).ready(function(){
  $("#new_user_form").validate({
    debug: false,
    submitHandler: function(form) {

      	$("#animated_image").show();
      	$("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[76]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

      	$.post('<?php echo $file_path[30]; ?>', $("#new_user_form").serialize() + '&new_user_account=new_user_account', function(response) {

            $('#new_user_form').find("input[type=text],input[type=password],input[type=email],textarea,select").val("");
            $('#default_password').val('gardner@365');

			$("#animated_image").hide();
			$("#result_container").html(response);

      	}); // END OF POST REQUEST 


    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>
