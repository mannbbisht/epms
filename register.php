<?php 
include "php/global_constants.php"; 
include $dynamic_links; 
?>

<title>Register</title>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<style type="text/css">
/* Credit to bootsnipp.com for the css for the color graph */
.colorgraph {
  height: 5px;
  border-top: 0;
  background: #c4e17f;
  border-radius: 5px;
  background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
}    
.error{
  color: red;
}
</style>


<?php

/********************************************** if user is logged in then login.php and register.php shouldn't open **********************************************/ 

if (  (strpos($complete_url, 'login') == true) || (strpos($complete_url, 'register') == true) )  {  

   if(!empty($session_employee_id)) {
      echo " <script type='text/javascript'> window.location = '$domain'; </script>";
   }

}

?>



<div class="container">

    <div class="row" style="margin-top:20px">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form id="register_form" name="register_form">
                <fieldset>

                    <h2>Register</h2>
                    <hr/>

                    <div id="animated_image"></div>
                    <div id="result_container"></div>

                    <hr class="colorgraph">
                    <div class="form-group">
                        <input type="text" name="full_name" id="full_name" class="form-control input-lg" placeholder="Full Name" autocomplete="off" required>
                    </div>

                    <div class="form-group">
                        <input type="email" name="email_id" id="email_id" class="form-control input-lg" placeholder="Email id" autocomplete="off" required>
                    </div>


                    <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" required>
                    </div>

                    <div class="form-group">
                        <input type="password" name="confirm_password" id="confirm_password" class="form-control input-lg" placeholder="Confirm Password" required>
                    </div>



<!--                     <span class="button-checkbox">
                        <a href="" class="btn btn-link pull-right">Forgot Password?</a>
                    </span> -->

                    <hr class="colorgraph">

                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <input type="login.php" class="btn btn-lg btn-success btn-block" value="Sign In">
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <input type="submit" class="btn btn-lg btn-primary btn-block" value="Register" />
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>

</div>


<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>


<!-- validation libs -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>


<!--- new_project form validation --> 
<script>   
$(document).ready(function(){
  $("#register_form").validate({
    debug: false,
    submitHandler: function(form) {

        $("#animated_image").show();
        $("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

        $.post('<?php echo $file_path[30]; ?>', $("#register_form").serialize() + '&register=register', function(response) {

            $("#animated_image").hide();
            $("#result_container").html(response);

        }); // END OF POST REQUEST 


    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>
