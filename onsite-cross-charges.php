<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>


<link rel="stylesheet" href="vendor/select2/dist/css/select2.min.css">

<link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="vendor/DataTables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.bootstrap4.min.css">

<style type="text/css">
.error{ color: red; }
.dt-buttons{ float: right !important; margin: 0 0 0 20px; }
.buttons-copy{display: none}
/*.buttons-excel::before { Content: 'Download as ' }
.buttons-csv::before { Content: 'Download as ' }
.buttons-pdf::before { Content: 'Download as ' }
*/td,th {
	text-align: center;
}
#table-2{
	width: auto !important;
}
.bg-white{
	width: 100% !important;
	overflow-x: scroll;
}
.hide{
	display: none !important;
}
</style>

<style type="text/css">
.error{ color: red; }
select{height: 40px !important;}
</style>


<?php
$month_names = GetLast12MonthsName();
?>

			<div class="site-content">
				<!-- Content -->
				<div class="content-area py-1">
					<div class="container-fluid">
						<h4> Onsite Cross Charges </h4>
						<ol class="breadcrumb no-bg mb-1">
							<li class="breadcrumb-item"><a href="#">Home </a></li>

							<li class="breadcrumb-item active"> Onsite Cross Charges </li> 


						</ol>


						<div id="animated_image"></div>
						<div id="result_container"></div>






							<div class="box box-block bg-white">

                                <div class="dt-buttons btn-group">

									<button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#AddUser" style="margin-left: 15px;" > <i class="fa fa-plus"></i> &nbsp; Add User </button>

									<button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#DeleteUser" style="margin-left: 15px;" > <i class="fa fa-remove"></i> &nbsp; Delete User </button>

								</div>



								<table class="table table-striped table-bordered dataTable" id="table-2">

									<thead>


										<tr class="noExport">

											<th> Employee Name </th>

											<?php 

											$sql_fetch_employee_list = mysql_query("SELECT *  FROM `$gd`.`users_list_for_invoice` inner join employees on employees.pk_employee_id = users_list_for_invoice.fk_employee_id  ", $connect_db);

											while ($row_fetch_employee_list = (mysql_fetch_array($sql_fetch_employee_list)) ){
											extract($row_fetch_employee_list);	
											?>


											<td colspan="6"> <?php echo $full_name; ?> </td>


											<?php } ?>


										</tr>


										<tr class="noExport">
											<th> Month </th>

											<?php 

											$sql_fetch_employee_list = mysql_query("SELECT *  FROM `$gd`.`users_list_for_invoice`  ", $connect_db);

											while ($row_fetch_employee_list = (mysql_fetch_array($sql_fetch_employee_list)) ){
											extract($row_fetch_employee_list);	
											?>

											<th> Working Hours </th>
											<th> Tickets, Visa </th>
											<th> Hotel, Ground Travel, Etc </th>
											<th> Allowances </th>
											<th> Other Expense </th>
											<th> Final Total </th>

											<th class="noExport"> Edit </th>

											<?php } ?>


										</tr>
									</thead>


									<tbody>




										<tr class="hide"> 

											<td> Employee Name </td>

											<?php 

											$sql_fetch_employee_list = mysql_query("SELECT *  FROM `$gd`.`users_list_for_invoice` inner join employees on employees.pk_employee_id = users_list_for_invoice.fk_employee_id  ", $connect_db);

											while ($row_fetch_employee_list = (mysql_fetch_array($sql_fetch_employee_list)) ){
											extract($row_fetch_employee_list);	

												echo "<td> $full_name </td>";

												echo "<td> </td>";
												echo "<td> </td>";
												echo "<td> </td>";
												echo "<td> </td>";
												echo "<td> </td>";
												echo "<td class='noExport'>  </td>";


											} 



											?>


										</tr>




										<?php 

										foreach ($month_names as $key => $value) { 

										?>

										<tr>

											<td> <?php echo $value; ?> </td>

											<?php


												$sql_fetch_employee_list_2 = mysql_query("SELECT *  FROM `$gd`.`users_list_for_invoice`  ", $connect_db);

												while ($row_fetch_employee_list_2 = (mysql_fetch_array($sql_fetch_employee_list_2)) ){
												extract($row_fetch_employee_list_2);	



													$sql_fetch_onsite_charges = mysql_query("SELECT *  FROM `$gd`.`onsite_charges` where fk_employee_id = '$fk_employee_id' and month = '$value' ", $connect_db);

													while ($row_fetch_onsite_charges = (mysql_fetch_array($sql_fetch_onsite_charges)) ){
													extract($row_fetch_onsite_charges);	

													}



											?>



											<td> <?php echo $working_hours; ?> </td>


											<td> <?php if(!empty($tickets_visa)) {  echo " &dollar; " . $tickets_visa; } ?> </td>

											<td> <?php if(!empty($hotel_ground_travel)) {  echo " &dollar; " . $hotel_ground_travel; } ?> </td>

											<td> <?php if(!empty($allowances)) {  echo " &dollar; " . $allowances; } ?> </td>

											<td> <?php if(!empty($other_expense)) {  echo " &dollar; " . $other_expense; } ?> </td>

											<td> <?php if(!empty($final_total)) {  echo " &dollar; " . $final_total; } ?> </td>

											<td> 
												<a 
												href="#!" 
												onclick="load_charges_details(
												'<?php echo $full_name; ?>',
												<?php echo $fk_employee_id; ?>,
												'<?php echo $value; ?>',
												<?php echo $pk_onsite_charge_id; ?>); "> Edit 
												</a> 
											</td>





											<?php 
											unset($pk_onsite_charge_id);
											unset($working_hours);  
											unset($tickets_visa);
											unset($hotel_ground_travel);
											unset($allowances);
											unset($other_expense);
											unset($final_total);
											}  ?>

										</tr>

											<?php 
											unset($pk_onsite_charge_id);
											unset($working_hours);  
											unset($tickets_visa);
											unset($hotel_ground_travel);
											unset($allowances);
											unset($other_expense);
											unset($final_total);
											$i++;

											}  ?>


									</tbody>
								</table>	




						</div>


					</div>
				</div>



<?php include $backend_footer_file; ?>

<script type="text/javascript" src="vendor/select2/dist/js/select2.min.js"></script>


<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>





<!-- Modal -->
<div id="AddUser" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">


				<form id="add_new_invoice" name="add_new_invoice" class="form-material material-primary">

	            	<div class="modal-header">

						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="exampleModalLabel"> Add User  </h4>

	            	</div>

	            	<div class="modal-body">




						<div class="form-group" >
							<div class="row">
							                                       

								<div class="col-sm-6">
									<label class="control-label"> Employee Name:  </label>
									<select id="pk_employee_id" name="pk_employee_id" class="form-control">

										<option value=""> --Select-- </option>

										<?php 
												
											$sql_fetch_employee = mysql_query("SELECT *  FROM `$gd`.`employees` order by full_name asc  ", $connect_db);

												while ($row_fetch_employee = (mysql_fetch_array($sql_fetch_employee)) ){
												extract($row_fetch_employee);	

										?>


										<option value="<?php echo $pk_employee_id; ?>"> <?php echo $full_name; ?> </option>

										<?php } ?>


									</select>
								</div>

								<div class="col-sm-6"> 

								</div>

								                                    
							</div>
						</div>





	            	</div>


				    <div class="modal-footer">
				    	<button type="submit" class="btn btn-primary">Submit</button>
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>

				</form>




    </div>

  </div>
</div>




<!--- new_project form validation --> 
<script>   
$(document).ready(function(){
  $("#add_new_invoice").validate({
    debug: false,
    submitHandler: function(form) {

    	$("#AddUser").modal('hide');

    	scrollToTop();

      	$("#animated_image").show();
      	$("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

      	$.post('<?php echo $file_path[30]; ?>', $("#add_new_invoice").serialize() + '&add_new_user_invoice=add_new_user_invoice', function(response) {

            $('#add_new_invoice').find("input[type=text],input[type=password],input[type=email],textarea,select").val("");

			$("#animated_image").hide();
			$("#result_container").html(response);


      	}).done(function() {

      		setTimeout(function(){
      			RefreshPage();
      		}, 1000);

      	}); // END OF POST REQUEST 



    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>




<!-- own download functionality -->
<script type="text/javascript">
$(document).ready(function(){
    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel ");  
});



$(document).ready(function(){
    $(".dt-buttons a").click(function() {

        // window.open('phpexcel/Examples/export.php?file=onsite-cross-charges');

    });
});

</script>



<script type="text/javascript">
function load_charges_details(employee_name,employee_id,month_name,pk_id) {

	$("#update_onsite_charges input[type='text'],[type='number'],select").val('');

	$("#update_employee_name").val(employee_id);
	$("#update_month").val(month_name).change();

	$("#name_in_header").html(employee_name);

	if(pk_id === undefined) {

		$("#EditOnsiteChargesInfo").modal();
	        
		<?php 

		$sql_fetch_last_pk_id = mysql_query("SELECT *  FROM `$gd`.`onsite_charges` order by pk_onsite_charge_id desc limit 1 ", $connect_db);
		$row_fetch_last_pk_id = mysql_fetch_array($sql_fetch_last_pk_id);

		$new_pk_id = $row_fetch_last_pk_id['pk_onsite_charge_id'] + 1;

		?>

		$("#updated_id").val(<?php echo $new_pk_id; ?>);


	} else {

		$.post('php/get_value.php', '&FetchOnsiteValues=FetchOnsiteValues' + '&onsite_charges_id='+pk_id, function(response1){

	        // $("#result_container").html(response1);

	        var parse_values = JSON.parse(response1);


	        $("#update_employee_name").val(parse_values.full_name);
	        $("#name_in_header").html(parse_values.full_name);

	        $("#update_month").val(parse_values.month);
	        $("#update_working_hours").val(parse_values.working_hours);
	        $("#update_comments_working_hours").val(parse_values.working_hours_comments);
	        $("#update_ticket_and_visa_charges").val(parse_values.tickets_visa);
	        $("#update_comments_ticket_and_visa_charges").val(parse_values.tickets_visa_comments);
	        $("#update_hotel_ground_travel_etc").val(parse_values.hotel_ground_travel);
	        $("#update_comments_hotel_ground_travel_etc").val(parse_values.hotel_ground_travel_comments);
	        $("#update_allowances").val(parse_values.allowances);
	        $("#update_comments_allowances").val(parse_values.allowances_comments);
	        $("#update_other_expense").val(parse_values.other_expense);
	        $("#update_other_expense_comments").val(parse_values.other_expense_comments);

	        $("#update_total_cost").val(parse_values.final_total);
	        $("#update_comments_total").val(parse_values.final_total_comments);


	        $("#updated_id").val(parse_values.pk_onsite_charge_id);

		});

		$("#EditOnsiteChargesInfo").modal();

	}



}
</script>








<!-- load project edit details in modal -->
<div id="EditOnsiteChargesInfo" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

				<form id="update_onsite_charges" name="update_onsite_charges" class="form-material material-primary">

	            	<div class="modal-header">

						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="exampleModalLabel"> Update onsite charges  </h4>

	            	</div>

	            	<div class="modal-body">



						<div class="form-group" style="display: none;">
							<div class="row">
							                                       

								<div class="col-sm-6"> 
									<label class="control-label"> Employee Name:  </label>
									<input type="text" id="update_employee_name" name="update_employee_name" class="form-control" readonly>


								</div>

								<div class="col-sm-6">
									<label class="control-label"> Month:  </label>
									<select id="update_month" name="update_month" class="form-control">

										<option value=""> --Select-- </option>

										<?php foreach ($month_names as $key => $value) {  ?>

										<option value="<?php echo $value; ?>"> <?php echo $value; ?> </option>

										<?php } ?>


									</select>
								</div>
								                                    
							</div>
						</div>






						<div class="form-group">
							<div class="row">

								<div class="col-sm-6"> 
												<label for="" class="col-form-label"> Working Hours: </label>
												<div class="">
													<input type="number" class="form-control" id="update_working_hours" name="update_working_hours" placeholder="8 hours" required>
												</div>

								</div>


								<div class="col-sm-6">

												<label for="" class="col-form-label"> Comments: </label>
												<div class="">
													<input type="text" class="form-control" id="update_comments_working_hours" name="update_comments_working_hours" placeholder="Comments for working hours">
												</div>


								</div>

							</div>
						</div>




						<div class="form-group">
							<div class="row">

								<div class="col-sm-6">

												<label for="" class="col-form-label"> Ticket & Visa: </label>
												<div class="">
													<input type="number" class="form-control" id="update_ticket_and_visa_charges" name="update_ticket_and_visa_charges" placeholder="visa costs" required>
												</div>

								</div>


								<div class="col-sm-6">

												<label for="" class="col-form-label"> Comments: </label>
												<div class="">
													<input type="text" class="form-control" id="update_comments_ticket_and_visa_charges"  name="update_comments_ticket_and_visa_charges" placeholder="Comments for ticket and visa charges">
												</div>
								</div>

							</div>
						</div>



						<div class="form-group">
							<div class="row">

								<div class="col-sm-6">
												<label for="" class="col-form-label"> Hotel, Ground travel, Food etc: </label>
												<div class="">
													<input type="number" class="form-control" id="update_hotel_ground_travel_etc" name="update_hotel_ground_travel_etc" placeholder="food expenses" required>
												</div>

								</div>


								<div class="col-sm-6">

												<label for="" class="col-form-label"> Comments: </label>
												<div class="">
													<input type="text" class="form-control" id="update_comments_hotel_ground_travel_etc" name="update_comments_hotel_ground_travel_etc" placeholder="Comments for `nd travel etc">
												</div>
								</div>

							</div>
						</div>







						<div class="form-group">
							<div class="row">

								<div class="col-sm-6"> 
												<label for="" class="col-form-label"> Allowances: </label>
												<div class="">
													<input type="number" class="form-control" id="update_allowances" name="update_allowances" placeholder="other allowances" required>
												</div>

								</div>

								<div class="col-sm-6"> 

												<label for="" class="col-form-label"> Comments: </label>
												<div class="">
													<input type="text" class="form-control" id="update_comments_allowances" name="update_comments_allowances" placeholder="Comments for allowances">
												</div>
								</div>


							</div>
						</div>	



						<div class="form-group">
							<div class="row">

								<div class="col-sm-6"> 
												<label for="" class="col-form-label"> Other Expense: </label>
												<div class="">
													<input type="number" class="form-control" id="update_other_expense" name="update_other_expense" placeholder="other expenses" required>
												</div>

								</div>

								<div class="col-sm-6"> 

												<label for="" class="col-form-label"> Comments: </label>
												<div class="">
													<input type="text" class="form-control" id="update_other_expense_comments" name="update_other_expense_comments" placeholder="Comments for other expenses">
												</div>
								</div>


							</div>
						</div>	



						<div class="form-group">
							<div class="row">

								<div class="col-sm-6"> 

												<label for="" class="col-form-label"> Total: </label>
												<div class="">
													<input type="number" class="form-control" id="update_total_cost" name="update_total_cost" required>
												</div>
								</div>




								<div class="col-sm-6"> 
												<label for="" class="col-form-label"> Comments: </label>
												<div class="">
													<input type="text" class="form-control" id="update_comments_total" name="update_comments_total">
												</div>

								</div>

							</div>
						</div>	

						<input type="hidden" id="updated_id" name="updated_id" value="">


	            	</div>


				    <div class="modal-footer">
				    	<button type="submit" class="btn btn-primary">Submit</button>
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>

				</form>

            </div>
        </div>
</div>




<!--- new_project form validation --> 
<script>   
$(document).ready(function(){
  $("#update_onsite_charges").validate({
    debug: false,
    submitHandler: function(form) {


    	$("#EditOnsiteChargesInfo").modal('hide');

    	scrollToTop();

      	$("#animated_image").show();
      	$("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

      	$.post('<?php echo $file_path[31]; ?>', $("#update_onsite_charges").serialize() + '&update_onsite_charges=update_onsite_charges', function(response) {

            $('#onsite_charges').find("input[type=text],input[type=password],input[type=email],textarea,select").val("");

			$("#animated_image").hide();
			$("#result_container").html(response);

			// console.log(xhr);
			// console.log(status);
			// console.log(response);

			// if(status.status) {
			// 	alert('done');
			// }


      	}).done(function(response1) {

      		console.log(response1);
      		setTimeout(function(){
      			RefreshPage();
      		}, 1000);

      	}); // END OF POST REQUEST 



    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>




<!-- Delete User Modal -->
<div id="DeleteUser" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">


				<form id="delete_user_from_invoice" name="delete_user_from_invoice" class="form-material material-primary">

	            	<div class="modal-header">

						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="exampleModalLabel"> Delete User  </h4>

	            	</div>

	            	<div class="modal-body">




						<div class="form-group" >
							<div class="row">
							                                       

								<div class="col-sm-6">
									<label class="control-label"> Employee Name:  </label>
									<select id="pk_employee_id" name="pk_employee_id" class="form-control">

										<option value=""> --Select-- </option>

										<?php 
												
											$sql_fetch_employee2 = mysql_query("SELECT *  FROM `$gd`.`users_list_for_invoice` inner join employees on employees.pk_employee_id = users_list_for_invoice.fk_employee_id ", $connect_db);

												while ($row_fetch_employee2 = (mysql_fetch_array($sql_fetch_employee2)) ){
												extract($row_fetch_employee2);	

										?>


										<option value="<?php echo $pk_employee_id; ?>"> <?php echo $full_name; ?> </option>

										<?php } ?>


									</select>
								</div>

								<div class="col-sm-6"> 

								</div>

								                                    
							</div>
						</div>





	            	</div>


				    <div class="modal-footer">
				    	<button type="submit" class="btn btn-primary">Submit</button>
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>

				</form>




    </div>

  </div>
</div>





<!--- delete user form validation --> 
<script>   
$(document).ready(function(){
  $("#delete_user_from_invoice").validate({
    debug: false,
    submitHandler: function(form) {


    	$("#DeleteUser").modal('hide');

    	scrollToTop();

      	$("#animated_image").show();
      	$("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

      	$.post('<?php echo $file_path[32]; ?>', $("#delete_user_from_invoice").serialize() + '&delete_user_from_invoice=delete_user_from_invoice', function(response) {

            $('#onsite_charges').find("input[type=text],input[type=password],input[type=email],textarea,select").val("");

			$("#animated_image").hide();
			$("#result_container").html(response);

      	}).done(function(response1) {

      		console.log(response1);
      		setTimeout(function(){
      			RefreshPage();
      		}, 1000);

      	}); // END OF POST REQUEST 



    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>




<script type="text/javascript">
	$('[data-plugin="select2"]').select2($(this).attr('data-options'));
</script>

<script type="text/javascript" src="js/forms-pickers.js"></script>

<script type="text/javascript">
function RefreshPage(){
	location.reload();
}
</script>


