<style type="text/css">
select{height: 40px !important;}
</style>


<?php /************************************************ assigned-projects.php ************************************************/ ?>

<?php 
include 'php/global_constants.php'; 
include $dynamic_links;

$selected_order_number = $_POST['order_number'];


// all employees list
$emp_list = array();
$sql_get_employees = mysql_query("SELECT pk_employee_id,full_name from  `$gd`.`employees` order by pk_employee_id asc " , $connect_db);
	// $i = 1;
	while ($row_get_employees = (mysql_fetch_array($sql_get_employees)) ){
	extract($row_get_employees);
	$emp_list[$pk_employee_id] .= $full_name;

	// $i++;

}


// get project details
$sql_get_ProjectInfo = mysql_query("SELECT * from  `$gd`.`projects` where order_number = '$selected_order_number' " , $connect_db);
	while ($row_get_ProjectInfo = (mysql_fetch_array($sql_get_ProjectInfo)) ){
	extract($row_get_ProjectInfo);

	$order_received_date = $order_recieved_date; 
	$current_project_status = $status;
}

?>


	<div class="modal-header">

		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel"> Change Project Status for: <?php echo $selected_order_number; ?> </h4>
			      
	</div>


	<div class="modal-body">

			<div class="form-group row">
				<label for="project_name" class="col-sm-3 col-form-label"> Project Status: </label>
				<div class="col-sm-6">
					<select id="select2-demo-1" name="project_status" class="form-control project_status" data-plugin="select2" required>
					<option value=""> --Select-- </option>



					<?php if($current_project_status == "Scope Clarity") { ?>

							<option value="To be started"> To be started </option>

					<?php } ?>





					<?php if($current_project_status == "To be started") { ?>

						<option value="In Progress"> In Progress </option>
<!-- 						<option value="Under Customer Review"> Under Customer Review </option>
						<option value="Completed"> Completed </option>
 -->
					<?php } ?>






					<?php if($current_project_status == "In Progress") { ?>

						<option value="Under Customer Review"> Under Customer Review </option>

					<?php } ?>




					<?php if($current_project_status == "Under Customer Review") { ?>

						<option value="Scope Clarity"> Scope Clarity </option>
						<option value="To be started 1"> To be started </option>
						<option value="In Progress 1"> In Progress </option>
						<option value="Completed"> Completed </option>

					<?php } ?>





					</select>
				</div>

			</div>



			<?php if($current_project_status == "Scope Clarity") { ?>


				<div class="form-group row complete_date_field">
					<label for="project_name" class="col-sm-3 col-form-label"> Scope Cleared Date:  </label>


					<div class="col-sm-6">
						<div class="input-group">
							<input type="text" class="form-control mydatepicker" name="scope_clear_date" autocomplete="off" placeholder="dd/mm/yyyy" required>
					    </div>
					</div>

				</div>

			<?php } else if ($current_project_status == "To be started") { ?>

				<div class="form-group row complete_date_field">

					<label for="project_name" class="col-sm-3 col-form-label"> Project Start Date:  </label>

					<div class="col-sm-6">
						<div class="input-group">
							<input type="text" class="form-control mydatepicker" name="project_start_date" autocomplete="off" placeholder="dd/mm/yyyy" required>
					    </div>
					</div>

				</div>

				<div class="form-group row complete_date_field">

					<label for="project_name" class="col-sm-3 col-form-label"> Engineer Commitment Date:  </label>

					<div class="col-sm-6">
						<div class="input-group">
							<input type="text" class="form-control mydatepicker" name="engineer_commitment_date" autocomplete="off" placeholder="dd/mm/yyyy" required>
					    </div>
					</div>

				</div>

			<?php } else if ($current_project_status == "Under Customer Review") { ?>
			
					<div class="form-group row complete_date_field">

						<label for="project_name" class="col-sm-3 col-form-label"> Customer Feedback Date:  </label>
				
						<div class="col-sm-6">
							<div class="input-group">
								<input type="text" class="form-control mydatepicker" name="customer_feedback_date" autocomplete="off" placeholder="dd/mm/yyyy" required>
						    </div>
						</div>

					</div>

					<div class="form-group row complete_date_field ShowStartDateField">

						<label for="project_name" class="col-sm-3 col-form-label"> Project Start Date:  </label>

						<div class="col-sm-6">
							<div class="input-group">
								<input type="text" class="form-control mydatepicker" name="project_start_date" autocomplete="off" placeholder="dd/mm/yyyy" required>
						    </div>
						</div>

					</div>

					<div class="form-group row complete_date_field ShowSCommitmentDateField">

						<label for="project_name" class="col-sm-3 col-form-label"> Engineer Commitment Date:  </label>

						<div class="col-sm-6">
							<div class="input-group">
								<input type="text" class="form-control mydatepicker" name="engineer_commitment_date" autocomplete="off" placeholder="dd/mm/yyyy" required>
						    </div>
						</div>

					</div>

			<?php } else if ($current_project_status == "In Progress") { ?>
			
					<div class="form-group row complete_date_field">

						<label for="project_name" class="col-sm-3 col-form-label"> Actual Submission Date:  </label>

						<div class="col-sm-6">
							<div class="input-group">
								<input type="text" class="form-control mydatepicker" name="submission_date" autocomplete="off" placeholder="dd/mm/yyyy" required>
						    </div>
						</div>
					
					</div>

			<?php } ?>








			<?php if($current_project_status == "Completed") { ?>
			<div class="form-group row complete_date_field">

				<label for="project_name" class="col-sm-3 col-form-label"> Customer Feedback Date:  </label>

				<div class="col-sm-6">
					<div class="input-group">
						<input type="text" class="form-control mydatepicker" name="customer_feedback_date" autocomplete="off" placeholder="dd/mm/yyyy" required>
				    </div>
				</div>

			</div>
			<?php } ?>







<!-- 			<div class="form-group row commitment_date_field">
				<label for="project_name" class="col-sm-3 col-form-label"> Self-Commitment Date:  </label>
				<div class="col-sm-6">
					<div class="input-group">
						<input type="text" class="form-control mydatepicker" name="commitment_date" autocomplete="off" placeholder="dd/mm/yyyy" required>
				        <span class="input-group-addon"><i class="fa fa-calendar-o removeIconOnClick"></i></span>
				    </div>
				</div>
			</div> -->


			<input type="hidden" name="project_id" value="<?php echo $pk_project_id; ?>">
			<input type="hidden" name="order_number" value="<?php echo $selected_order_number; ?>">

			<?php if($current_project_status != "Scope Clarity") { ?>
			<p class="toggleBtn"> Show project history <i class="fa fa-angle-down" aria-hidden="true"></i> </p>
			<?php } ?>

			<div class="toggleDiv">

				<table class="table table-hover">
				  <thead>
				    <tr>
				      <th scope="col">Status</th>
				      <th scope="col">Date</th>
				      <th scope="col"> Changed by </th>
				    </tr>
				  </thead>
 				<tbody>
				  	<?php

				  		$status_changed_by = array();

					  	$fetch_status = mysql_query(" SELECT * FROM `$gd`.`project_status_dropdown` order by field(project_status, 'Scope Clarity', 'To be started', 'In Progress', 'Under Customer Review', 'Completed', 'Opportunity Lost', 'New Requirement', 'Hold', 'Proposal Under Devlpt') ", $connect_db );
						while ($row_status = (mysql_fetch_array($fetch_status)) ){
						extract($row_status);

							if($project_status == "Completed") {
								$project_status = "Customer Feedback Date";
							}

							$all_status[] = "$project_status";

						}

						$fetch_date = mysql_query(" SELECT * FROM `$gd`.`project_revision` where fk_project_id = '$pk_project_id'  ", $connect_db );
						while ($row_date = (mysql_fetch_array($fetch_date)) ){
						extract($row_date);



							if(!empty($scope_cleared_date)) { 
								$to_be_started_date[] = $scope_cleared_date; 
								$status_changed_by[0] = $changed_by;
							}

							if(!empty($project_start_date)) { 
								$in_prog_date[] = $project_start_date; 
								if(!empty($to_be_started_date)) {
									$status_changed_by[1] = $changed_by;
								}
							}
							
							if(!empty($self_commitment_date)) { 
								$in_prog_date[] .= $self_commitment_date; 
								if(!empty($in_prog_date)) {
									$status_changed_by[2] = $changed_by;
								}
							}

							if(!empty($submission_date)) { 
								$project_submitted_date[] = $submission_date; 
								if(!empty($in_prog_date)) {
									$status_changed_by[3] = $changed_by;
								}
							}

							if(!empty($customer_feedback_date)) { 
								$complete_date[] = $customer_feedback_date; 
								if(!empty($customer_feedback_date)) {
									$status_changed_by[4] = $changed_by;
								}
							}

						}


						    foreach ($all_status as $key => $value) { 


						    	echo " <tr>   ";
						    	echo " <td> $value </td> ";

						    	if($value == "Scope Clarity") {
							    	echo " <td> " .  $order_received_date   . " </td> ";
							    	echo " <td>   </td> ";
						    	} else if($value == "To be started") {
							    	echo " <td> " .  implode(", ", $to_be_started_date)   . " </td> ";
							    	echo " <td> " . $emp_list[$status_changed_by[1]]  . "  </td> ";
						    	} else if($value == "In Progress") {
							    	echo " <td> " .  implode(", ", $in_prog_date)   . " </td> ";
							    	echo " <td> " . $emp_list[$status_changed_by[2]]  . "  </td> ";
						    	} else if($value == "Under Customer Review") {
							    	echo " <td> " .  implode(", ", $project_submitted_date)   . " </td> ";
							    	echo " <td> " . $emp_list[$status_changed_by[3]]  . "  </td> ";
						    	} else if($value == "Customer Feedback Date") {
							    	echo " <td> " .  implode(", ", $complete_date)   . " </td> ";
							    	echo " <td> " . $emp_list[$status_changed_by[4]]  . "  </td> ";
						    	} 
						    	echo " </tr> ";

							} 



					?>
				  </tbody>



 				</table>

			</div>				


		    <div class="modal-footer">
		    	<button type="submit" class="btn btn-primary">Submit</button>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>

	</div> <!-- modal body -->






<script type="text/javascript">
$(document).ready(function(){
	$(".toggleDiv").hide();

		$('.ShowStartDateField').hide();
		$('.ShowSCommitmentDateField').hide();

	// $('.complete_date_field , .commitment_date_field').hide();
});
</script>




<script type="text/javascript" src="vendor/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="vendor/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script type="text/javascript" src="vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="vendor/moment/moment.js"></script>
<script type="text/javascript" src="vendor/bootstrap-daterangepicker/daterangepicker.js"></script>

<script type="text/javascript">
$('.mydatepicker').datepicker({
   todayHighlight : true,
   autoclose: true,
   format: 'dd/mm/yyyy'

});	
</script>



<script type="text/javascript">
$('select').on("change", function(){

	var new_status = $(".project_status").find(':selected').val();

	// console.log(new_status);
	if(new_status == 'In Progress 1'){
		$('.ShowStartDateField').show();
		$('.ShowSCommitmentDateField').show();
	} else {
		$('.ShowStartDateField').hide();
		$('.ShowSCommitmentDateField').hide();
	}


});
</script>

<!-- <script type="text/javascript">
function showOtherField() {
	var new_status = $(".project_status").find(':selected').text();
	if(new_status == 'In Progress'){
		$('.commitment_date_field').show();
		$('.complete_date_field').hide();
	} else if(new_status == 'Completed') {
		$('.complete_date_field').show();
		$('.commitment_date_field').hide();
	} else {
		$('.commitment_date_field').hide();
		$('.complete_date_field').hide();
	}
}
</script> -->



<script type="text/javascript">
	$(".toggleBtn").click(function(){
		$(".toggleDiv").toggle();
	});
</script>


