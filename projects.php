<?php 

if(isset($_POST['reset_filters'])) {
	header("Location: $domain/projects.php");
}


include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>


<link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<link rel="stylesheet" href="vendor/DataTables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.bootstrap4.min.css">


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.dataTables.min.css">


<!-- MULTI SELECT CSS -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.2/multiple-select.min.css">


<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" />
 -->
<style type="text/css">
.text-center{
	text-align: center;
}
.multiselect-container>li>a>label.checkbox{
	padding: 5px 10px !important;	
}
.col-md-1{
	margin: 0 !important;
	width: auto !important;
}
#report_container{
	width: 100% !important;

/*	width: 100%;
	overflow-x: scroll;*/

/*	height: 800px;
	overflow-y: scroll !important;
*/
}	
#filter_option_form select{
	width: 130px !important;
}
.hide{
	display: none !important;
}
#report_container{
	overflow-x: scroll;
}
.table-striped tbody tr:nth-of-type(odd) {
    background-color: rgba(0,0,0,.05);
}
table{
	width: 100% !important;
	table-layout: fixed;
}
table tr td{
	word-break: break-all !important;
}
</style>

<?php

$hourly_charges = array(
	'Analysis' => 16.39, 
	'Programming' => 15.17, 
	'Design' => 10.68, 
	'SAP/Documentation' => 10.68, 
	'Modeling / Drawing' => 10.68, 
	'i2V' => 10.68
);


?>


<?php /********************************************* APPLY FILTER ON PAGE SUBMIT *********************************************/ ?>

<?php

if(isset($_POST['submit'])) {

	$sel_month = $_POST['filter_month'];
	$sel_division = $_POST['filter_division'];
	$sel_order_number = $_POST['filter_order_number'];
	$sel_status = $_POST['filter_status'];
	$sel_project_type = $_POST['filter_project_type'];
	$sel_emp_name = $_POST['filter_emp_name'];
	$sel_country = $_POST['choose_country'];

	// print_r($sel_month);



    // SPLIT UP ELEMENTS OF ARRAY
    $all_months = implode(",", $sel_month);
    $all_divisions = implode(",", $sel_division);
    $all_order_number = implode(",", $sel_order_number);
    $all_status = implode(",", $sel_status);
    $all_types = implode(",", $sel_project_type);
    $all_employees = implode(",", $sel_emp_name);
    $all_countries = implode(",", $sel_country);

    // echo "monhs: $all_months";




	$filters;
	if(!empty($all_months)) {
		$filters .= "month=$all_months";
	}


	if(!empty($all_divisions)) {

		if(!empty($filters)) {
			$filters .= "&a";			
		}

		$filters .= "division=$all_divisions";
	}


	if(!empty($all_order_number)) {

		if(!empty($filters)) {
			$filters .= "&a";			
		}

		$filters .= "order_number=$all_order_number";
	}

	if(!empty($all_status)) {

		if(!empty($filters)) {
			$filters .= "&";			
		}

		$filters .= "&status=$all_status";
	}

	if(!empty($all_types)) {

		if(!empty($filters)) {
			$filters .= "&";			
		}

		$filters .= "&project_type=$all_types";
	}

	if(!empty($all_employees)) {

		if(!empty($filters)) {
			$filters .= "&";			
		}

		$filters .= "&employees=$all_employees";
	}

	if(!empty($all_countries)) {

		if(!empty($filters)) {
			$filters .= "&";			
		}

		$filters .= "&countries=$all_countries";
	}



} 



?>



			<div class="site-content">
				<!-- Content -->
				<div class="content-area py-1">
					<div class="container-fluid">
						

						<?php if($member_role == "global") { ?> 

						

						<?php } ?>

						<?php if($member_role != 'global') { ?>

						<ol class="breadcrumb no-bg mb-1">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<li class="breadcrumb-item active">Projects</li>

						</ol>

						<?php } ?>

						<?php if(!empty($_POST)) { ?>

						<div class="alert alert-success-fill alert-dismissible fade in">

							<?php 

								$message = "You have selected: ";

								if(!empty($sel_month) && $sel_month != "0all"  ) {
									// $message .= "Month: " . date("F", mktime(0, 0, 0, $all_months, 10));

									$message .= "Month: " ;

									foreach ($sel_month as $key => $value) {
										$message .= date("F", mktime(0, 0, 0, $sel_month[$key], 10));
		
										$count = count($sel_month);
										$count = $count - 1;
										
										if($key < $count) {
											$message .= ", ";
										}


									}

									$showComma = 1;

								} 


								if(!empty($sel_order_number)) {

									if($showComma === 1) {
										$message .= ",";
									}

									if(in_array("all", $sel_order_number)) {
										$message .= " Order Number: All";
									} else {
										$message .= " Order Number: " . $all_order_number;
									}

									$showComma = 1;

	
								}



								if(!empty($sel_status)) {

									if($showComma === 1) {
										$message .= ",";
									}

									if(in_array("all", $sel_status)) {
										$message .= " Status: All";
									} else {
										$message .= " Status: " . $all_status;
									}
									$showComma = 1;

	
								}

								if(!empty($sel_project_type)) {

									if($showComma === 1) {
										$message .= ",";
									}

									if(in_array("all", $sel_project_type)) {
										$message .= "Project Type : All";
									} else {
										$message .= " Project Type : " . $all_types;
									}
									$showComma = 1;

	
								}


								if(!empty($sel_emp_name)) {

									if($showComma === 1) {
										$message .= ",";
									}

									if(in_array("all", $sel_emp_name)) {
										$message .= "Employee: All";
									} else {
										$message .= " Employee: " . $all_employees;
									}

									$showComma = 1;

								}

								if(!empty($sel_country)) {

									if($showComma === 1) {
										$message .= ",";
									}
									if(count($sel_country) === 16) {
										$message .= " Countries: All";
									} else {
										$message .= " Country: " . implode(",", $sel_country);
									}

									$showComma = 1;

								}


								if(!empty($sel_division)) {

									if($showComma === 1) {
										$message .= ",";
									}

									$message .= " Division: " . implode(",", $sel_division);


									$showComma = 1;

								}

								echo $message;

							?>

						</div>

						<?php } ?>




						<nav class="box box-block bg-white">
							

							<form id="filter_option_form" name="filter_option_form" method="post">
	                            <div class="row">


	                            	<?php if($member_role == "admin") { ?>

	                                <div class="col-md-1">
										<select name="filter_month[]" data-placeholder="Select Month" multiple>

											<?php echo Last12Months_Dropdown(); ?>

										</select>
									</div>

									<?php } else if($member_role == "global") { ?>

	                                <div class="col-md-1">
										<select name="filter_division[]" data-placeholder="Select Division" multiple>

											<?php $sql_filter_division = mysql_query("SELECT * FROM `$gd`.`project_division_dropdown`  order by project_division asc ", $connect_db); ?>                        	
				                            <?php while($row_filter_division = mysql_fetch_array($sql_filter_division)){ ?>
				                            <option value="<?php echo $project_division = $row_filter_division['project_division']; ?>" >
				                            <?php echo $project_division; ?> 
				                            </option>
				                            <?php } ?>

										</select>
									</div>

									<?php } ?>


	                                <div class="col-md-1">
										<select name="filter_order_number[]" data-placeholder="Select Order Number" multiple>
<!--											<option value="Leave"> Leave </option>
											<option value="Training"> Training </option>
-->
											<?php $sql_filter_order_number = mysql_query("SELECT order_number FROM `$gd`.`projects`  where order_number != 'Leave' and order_number != 'Training' order by order_number asc", $connect_db); ?>                        	
				                            <?php while($row_filter_order_number = mysql_fetch_array($sql_filter_order_number)){ ?>
				                            <option value="<?php echo  $order_number = $row_filter_order_number['order_number']; ?>" >
				                            <?php echo $order_number; ?> 
				                            </option>
				                            <?php } ?>
										</select>
									</div>



	                                <div class="col-md-1">
										<select name="filter_status[]" data-placeholder="Select Status" multiple>
											<?php $sql_filter_status = mysql_query("SELECT * FROM `$gd`.`project_status_dropdown` ", $connect_db); ?>                        	
				                            <?php while($row_filter_status = mysql_fetch_array($sql_filter_status)){ ?>
				                            <option value="<?php echo  $project_status = $row_filter_status['project_status']; ?>" >
				                            <?php echo $project_status; ?> 
				                            </option>
				                            <?php } ?>
										</select>
									</div>




	                            	<?php if($member_role == "admin") { ?>

 	                                <div class="col-md-1">
										<select name="filter_emp_name[]" data-placeholder="Select Employee" multiple>

											<!-- <option value="">-- Select -- </option>											 -->
											<?php $sql_fetch_name = mysql_query("SELECT * FROM `$gd`.`employees` order by full_name asc  ", $connect_db); ?>                        	
				                            <?php while($row_fetch_name = mysql_fetch_array($sql_fetch_name)){ ?>
				                            <option value="<?php echo  $full_name = $row_fetch_name['full_name']; ?>" >
				                            <?php echo $full_name; ?> 
				                            </option>
				                            <?php } ?>

										</select>
									</div>

								<?php } ?>


	                                <div class="col-md-1">
										<select name="filter_project_type[]" data-placeholder="Select Project Type" multiple>

											<!-- <option value="">-- Select -- </option>											 -->
											<?php $sql_filter_project_type = mysql_query("SELECT * FROM `$gd`.`project_type_dropdown`   ", $connect_db); ?>                        	
				                            <?php while($row_filter_project_type = mysql_fetch_array($sql_filter_project_type)){ ?>
				                            <option value="<?php echo  $project_type = $row_filter_project_type['project_type']; ?>" >
				                            <?php echo $project_type; ?> 
				                            </option>
				                            <?php } ?>

										</select>
									</div>




	                                <div class="col-md-1">
										<select id="dates-field2" name="choose_country[]" data-placeholder="Select Location"  multiple>


											<!-- <option value=""> -- Select Employee Name -- </option> -->
											<?php 
												$sql_fetch_all_loc = mysql_query("SELECT * FROM `$gd`.`location_dropdown` order by country asc ", $connect_db);
												while ($row_fetch_all_loc = mysql_fetch_array($sql_fetch_all_loc)) {
													extract($row_fetch_all_loc);

												?>

												<option value="<?php echo $country; ?>"><?php echo $country; ?></option>

												<?php }
											?>


										</select>
									</div>



									<div class="col-sm-1">
										<input type="submit" name="submit" class="btn btn-primary" value="Submit">

										<!-- <input type="submit" class="btn btn-primary" id="reset_filters" name="reset_filters" value="Reset" />  -->

									</div>



									<?php if($member_role == "admin") { ?>

									<div class="col-sm-1 float-right">
										<button type="button" class="btn btn-primary btn-md NewWindowForProject" style="float: right;"> <i class="fa fa-plus"></i> &nbsp; Add project</button>
									</div>

									<?php } else { ?>

									<div id="custom_tableoptions" style="margin:10px;">

										<button id="download" type="button" name="submit" class="btn btn-secondary"  onclick="javascript:xport.toCSV('table-2');" style="float: right;"> <i class="fa fa-download"></i> Excel </button>


									<!-- <button type="button" class="btn btn-secondary" id="btnExport" onclick="javascript:xport.toCSV('table-2');"> Excel </button>  -->
									</div>

									<?php } ?>


								</div>
							</form>




						</nav>

						<div id="animated_image"></div>
						<div id="result_container"></div>


						<div class="box box-block bg-white">
							<!-- <h5>Create new project</h5> -->
							<!-- <p class="font-90 text-muted mb-1">To use, add <code>.material-*</code> to the form.</p> -->

							<div id="report_container">

								<table class="table table-striped table-bordered dataTable" id="table-2">
									<thead>
										<tr>

											<th> Division </th>
											<th> Order Number </th>
											<th> Location </th>
											<th> Request From </th>
											<th> Project Title </th>
											<th> Description </th>
											<th> Project Type </th>
											<th> Status </th>
											<th> Actual to Date Hrs </th>
											<th> Hourly Charges </th>
											<th> Total Charges </th>

											<th  id="order_recieved_date"> Order Received Date </th>
											<th id="customer_want_date"> Customer Want Date </th>

											<th <?php if($member_role != "admin") { ?> class="noExport" <?php } ?> > Assign To </th>
											<th <?php if($member_role != "admin") { ?> class="noExport" <?php } ?> > Engineer Commitment Date </th>

											<th id="actual_completion_date"> Actual Completion Date </th>
											<th> Complexity Level </th>

											<th class="noExport"> Order Date Sort Column </th>
											<th class="noExport"> Customer Date Sort Column </th>
											<th class="noExport"> Completion Date Sort Column </th>


											<th class="noExport"> Edit </th>
											<th class="noExport"> Delete </th>


											<th> <?php echo date("Y", strtotime("-1 year")) . " Hours"; ?> </th>




 											<th> <?php echo date("F Y", strtotime("-11 month")); ?> </th>
											<th> <?php echo date("F Y", strtotime("-10 month")); ?> </th>
											<th> <?php echo date("F Y", strtotime("-9 month")); ?> </th>
											<th> <?php echo date("F Y", strtotime("-8 month")); ?> </th>
											<th> <?php echo date("F Y", strtotime("-7 month")); ?> </th>
											<th> <?php echo date("F Y", strtotime("-6 month")); ?> </th>
											<th> <?php echo date("F Y", strtotime("-5 month")); ?> </th>
											<th> <?php echo date("F Y", strtotime("-4 month")); ?> </th>
											<th> <?php echo date("F Y", strtotime("-3 month")); ?> </th>
											<th> <?php echo date("F Y", strtotime("-2 month")); ?> </th>
											<th> <?php echo date("F Y", strtotime("-1 month")); ?> </th>

											<th> <?php echo date("F Y", strtotime("now")); ?> </th>

										</tr>

								</table>	

							</div>


						</div> <!-- box-block -->
					</div>
				</div>

			</div>



<?php include $backend_footer_file; ?>


<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>









<script type="text/javascript">
$(document).ready(function() {
    $('#table-2').DataTable( {


        <?php if($member_role === "global") { ?>

        searching: false,

        <?php } ?>
        "ajax": 'php/projects_loadmore.php?&page=projects.php&report_type=<?php echo $member_role; ?>&<?php echo $filters; ?>',

        "columns": [
            { "data": "division" },
            { "data": "order_number" },
            { "data": "location" },
            { "data": "request_from" },
            { "data": "project_title" },
            { "data": "description" },
            { "data": "project_type" },
            { "data": "status" },
            { "data": "actual_to_date_hrs" },
            { "data": "hourly_charges" },
            { "data": "total_charges" },
            { "data": "order_recieved_date" },
            { "data": "customer_want_date" },
            { "data": "assign_to" },
            { "data": "engineer_commitment_date" },
            { "data": "actual_completion_date" },
            { "data": "complexity_level" },

            { "data": "order_date_sort_column" },
            { "data": "customer_date_sort_column" },
            { "data": "completion_date_sort_column" },

            { "data": "edit" },
            { "data": "delete" },

            { "data": "total_hours_2018" },
            { "data": "last11_month" },
            { "data": "last10_month" },
            { "data": "last09_month" },
            { "data": "last08_month" },
            { "data": "last07_month" },
            { "data": "last06_month" },
            { "data": "last05_month" },
            { "data": "last04_month" },
            { "data": "last03_month" },
            { "data": "last02_month" },
            { "data": "last01_month" },
            { "data": "current_month" }


        ],



		"aaSorting": [],

		dom: 'Bfrtip',

	    
	  
	scrollY:500,
        scrollX: true,
       


        


	    bLengthChange: false,





		buttons: [
        <?php if($member_role === "admin") { ?>

			{
				extend: 'excel',
			    // footer: true,
			    exportOptions: {
					columns: "thead th:not(.noExport)"
				}
			},
			{
			    extend: '',
				footer: false
			},
			{
				extend: '',
			footer: false
			}         
		<?php } ?>
		],


	    "iDisplayLength": 10000,

        "columnDefs": [

    		// { 
    		// 	"width": "50px", "targets": [3,4],
    		// },

    		{ 
    			"width": "200px", "targets": [4,5],
    		},
    		{ 
    			"width": "90px", "targets": [3],
    		},
    		{ 
    			"width": "50px", "targets": AllCols(),
    		},


            {
                 "targets": [ 17, 18, 19 ],
                "visible": false
            }


            <?php if($member_role == "global") { ?>
            	,
	            {
	                "targets": HideCols_Global(),
	                "visible": false
	            }

            <?php } else if ($member_role == "admin") { ?>

            	,
	            {
	                "targets": [ 22 ],
	                "visible": false
	            }

	        <?php } ?>


        ]	    

    } );
} );	


</script>





<!-- edit onclick function -->
<script type="text/javascript">
function load_project_details(project_id) {

	$.post('edit_projects.php', '&project_id='+project_id, function(response){
		$('#edit_project_details').html(response);
	});

    $('#EditProjectInfo').modal();

    window.project_id = project_id;
    // alert(window.project_id);

	// $("#90").click(function(){
		var tr = $(this).closest('tr').attr('class');
		// alert(tr);

		$("#"+window.project_id).closest('tr').attr('id', window.project_id);
	// });

}
</script>


<!-- load project edit details in modal -->
<div id="EditProjectInfo" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

				<form id="edit_project_details" name="edit_project_details">

<!-- 	                <div id="load_project_info"></div>
 -->
				</form>

            </div>
        </div>
</div>


<!-- edit modal validation -->
<script>   
function confirm_edit_project_details(){
  $("#edit_project_details").validate({
    debug: false,
    submitHandler: function(form) {

      	$("#animated_image").show();
      	$("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

      	$.post('<?php echo $file_path[31]; ?>', $("#edit_project_details").serialize() + '&update_project_details=update_project_details' , function(response) {

			$("#animated_image").hide();
			$("#result_container").html(response);


	      	$.post("php/trigger_email.php");


      	}).done(function() {

      		setTimeout(function(){
      			RefreshPage();
      		}, 1000);

      	}); // END OF POST REQUEST 

	    $('#EditProjectInfo').modal('hide');
    	scrollToTop();
    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
} // END OF FUNCTION 
</script>
		



<!-- delete onclick function -->
<script type="text/javascript">
function delete_project(project_id) {

	$.post('delete_project.php', '&project_id='+project_id, function(response){
		$('#delete_project_form').html(response);
	});

    $('#DeleteProjectModal').modal();

    window.project_id = project_id;

}
</script>


<!-- load project edit details in modal -->
<div id="DeleteProjectModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

				<form id="delete_project_form" name="delete_project_form">

<!-- 	                <div id="load_project_info"></div>
 -->
				</form>

            </div>
        </div>
</div>


<script type="text/javascript">
function confirm_delete(project_id) {
  $("#delete_project_form").validate({
    debug: false,
    submitHandler: function(form) {

      	$("#animated_image").show();
      	$("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

      	$.post('<?php echo $file_path[32]; ?>', $("#delete_project_form").serialize() + '&deleteProject=deleteProject' + '&project_id='+project_id , function(response) {

			$("#animated_image").hide();
			$("#result_container").html(response);

      	}); // END OF POST REQUEST 

	    $('#DeleteProjectModal').modal('hide');
	    // loadReport();
    	scrollToTop();

    	$('#'+window.project_id).remove();

		// loadReport();

    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}
</script>






<!-- Modal -->
<div id="NewProjectPopUp" class="modal fade" role="dialog">
 	<div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">

	    	<form id="addNewProject" name="addNewProject">

		    	<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title"> Add new project </h4>
		      	</div>

		      	<div class="modal-body">


					<div class="form-group">
						<div class="row">
						                                       
							<div class="col-sm-6">

								<label for="project_division" class="control-label"> Division: </label>
								<select name="project_division" class="form-control" required>
								<option value=""> --Select-- </option>

									<?php 

										$sql_fetch_division = mysql_query("SELECT * FROM `$gd`.`project_division_dropdown`  ", $connect_db);
										while ($row_fetch_division = mysql_fetch_array($sql_fetch_division)) {
											extract($row_fetch_division);
									?>

										<option value="<?php echo $project_division; ?>"><?php echo $project_division; ?></option>

									<?php } // END OF WHILE ?>


								</select>

							</div>
							                                    
							<div class="col-sm-6"> 

								<label class="control-label"> Order Number: </label>
								<input name="order_number" class="form-control" required>

							</div>

						</div>
					</div>



					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">


								<label for="location" class="control-label"> Location: </label>
								<select name="location" class="form-control" required>
								<option value=""> --Select-- </option>

									<?php 

										$sql_fetch_location = mysql_query("SELECT * FROM `$gd`.`location_dropdown`  ", $connect_db);
										while ($row_fetch_location = mysql_fetch_array($sql_fetch_location)) {
											extract($row_fetch_location);
									?>

											<option value="<?php echo $country; ?>"><?php echo $country; ?></option>

									<?php } // END OF WHILE ?>

								</select>


							</div>

							<div class="col-sm-6"> 
								<label class="control-label"> Request From: </label>
								<input type="text" name="request_from"  class="form-control" required>
							</div>
						</div>
					</div>



					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">

								<label class="control-label"> Project Title: </label>
								<input type="text" name="project_name"  class="form-control" required>

							</div>

							<div class="col-sm-6"> 

								<label for="project_type" class="control-label"> Project Type: </label>
								<select name="project_type" class="form-control" >
								<option value=""> --Select-- </option>

									<?php 

										$sql_fetch_project_type = mysql_query("SELECT * FROM `$gd`.`project_type_dropdown`  ", $connect_db);
										while ($row_fetch_project_type = mysql_fetch_array($sql_fetch_project_type)) {
											extract($row_fetch_project_type);
									?>

										<option value="<?php echo $project_type; ?>"><?php echo $project_type; ?></option>

									<?php } // END OF WHILE ?>

								</select>

							</div>
						</div>
					</div>



					<div class="form-group">
						<div class="row">

							<div class="col-sm-6"> 
								<label class="control-label"> Description/Activity: </label>
								<textarea name="description"  class="form-control" rows="5" required> </textarea>
							</div>


							<div class="col-sm-6">

								<label for="assign_to" class="control-label"> Assign To: </label>
								<!-- <input name="assigned_to"  class="form-control" value="<?php echo $assign_to; ?>"> -->
		 						<select multiple name="assign_to[]" class="form-control"required>
								<option value=""> --Select-- </option>

								<?php 

									$sql_fetch_emp = mysql_query("SELECT * FROM `$gd`.`employees`   ", $connect_db);
									while ($row_fetch_emp = mysql_fetch_array($sql_fetch_emp)) {
									extract($row_fetch_emp);
								?>

									<option value="<?php echo $full_name; ?>"><?php echo $full_name; ?></option>

								<?php } // END OF WHILE ?> 


								</select>


							</div>
						</div>
					</div>






					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								<div class="input-group">
									<label for="order_received_date" class="control-label"> Order Received Date: </label>
		                   			<input type="text" class="form-control mydatepicker" id="order_received_date" name="order_received_date" placeholder="mm/dd/yyyy" autocomplete="off" required>
		                    		<!-- <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span> -->
		                    	</div>
							</div>

							<div class="col-sm-6"> 
								<label for="project_status" class="control-label"> Status </label>
								<select name="project_status" class="form-control project_status" required>
								<option value=""> --Select-- </option>

									<?php 

										$sql_fetch_status = mysql_query("SELECT * FROM `$gd`.`project_status_dropdown`  ", $connect_db);
										while ($row_fetch_status = mysql_fetch_array($sql_fetch_status)) {
											extract($row_fetch_status);
									?>

											<option value="<?php echo $project_status; ?>"><?php echo $project_status; ?></option>

									<?php } // END OF WHILE ?>


								</select>
							</div>
						</div>
					</div>






					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								<label class="control-label"> Comments: </label>
								<input name="comments"  class="form-control" required>
							</div>

							<div class="col-sm-6"> 
								<div class="input-group">
									<label for="customer_want_date" class="control-label"> Customer Want Date: </label>
									<input type="text" class="form-control mydatepicker" name="customer_want_date" autocomplete="off" placeholder="mm/dd/yyyy" required>
			                    	<!-- <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span> -->
			                    </div>
							</div>
						</div>
					</div>





					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								<label class="control-label"> Complexity Level: </label>
								<select name="complexity_level" class="form-control" required>
									<option value=""> --Select-- </option>
									<option value="1"> 1 </option>
									<option value="2"> 2 </option>
									<option value="3"> 3 </option>
								</select>
							</div>

							<div class="col-sm-6"> 
							</div>
						</div>
					</div>



		      	</div> <!-- modal body -->



		      	<div class="modal-footer">
			    	<button type="submit" class="btn btn-primary">Save</button>
			    	<button type="button" class="btn" data-dismiss="modal">Close</button>
		      	</div>

			</form>
	    </div>

  	</div>
</div>




<script type="text/javascript">
$(document).ready(function(){
    $("#addNewProject").validate({
      debug: false,
       submitHandler: function(form) {
        // do other stuff for a valid form

	    $('#NewProjectPopUp').modal('hide');

        $("#animated_image").show();
        $("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');
    
        $.post('<?php echo $file_path[30]; ?>', $("#addNewProject").serialize() + '&addNewProject=addNewProject' , function(response) {

	        $("#animated_image").hide();
	        $('#result_container').empty();
	        $('#result_container').html(response);


	        // loadReport();

		}); // END OF POST REQUEST 

		} // END OF SUBMIT HANDLER

    });  // END OF VALIDATION

}); // END OF DOCUMENT READY FUNCTION 

</script>
  





<script type="text/javascript" src="vendor/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="vendor/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script type="text/javascript" src="vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="vendor/moment/moment.js"></script>
<script type="text/javascript" src="vendor/bootstrap-daterangepicker/daterangepicker.js"></script>

<script type="text/javascript">
$('#order_received_date').datepicker({
   todayHighlight : true,
   orientation: "left",
   autoclose: true
});	
$('.mydatepicker').datepicker({
   autoclose: true
});	
</script>


 

<script src="//cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.2/multiple-select.min.js"></script>

<!-- 
<script>
  init({
    title: 'selected/disabled options',
    desc: 'Multiple Select can take from selected and disabled options',
    links: ['multiple-select.css'],
    scripts: ['multiple-select.js']
  })
</script>

 -->

<script>
  // Here the mounted function is same as $(function() {})
$(document).ready(function(){
    $('select').multipleSelect({
    	filter: true
    })
});
</script>


<script type="text/javascript">
$(document).ready(function(){
    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel  ");  


    $(".NewWindowForProject").click(function(){
    	window.open("add-project.php");
    });
});
</script>







<script type="text/javascript" src="vendor/DataTables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Responsive/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Responsive/js/responsive.bootstrap4.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.bootstrap4.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/JSZip/jszip.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/pdfmake/build/pdfmake.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/pdfmake/build/vfs_fonts.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.print.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.colVis.min.js"></script>




<script type="text/javascript">

var counter = 1;
    
$(document).ready(function() {

	$("#order_recieved_date").click(function(){

	counter++;
	    
		// $("#result_container").text(counter);

		if(counter % 2 === 0) {

			var table = $('#table-2').DataTable();
			table.order.listener( '#order_recieved_date', 17 );

			table.order([17, "desc"]).draw();


		} else {

			var table = $('#table-2').DataTable();
			table.order.listener( '#order_recieved_date', 17 );

			table.order([17, "asc"]).draw();

		} 

	    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel  ");  

	}); // end of $("#sorter").click(function(){

}); // end of doc ready
</script>




<script type="text/javascript">

var counter = 1;
    
$(document).ready(function() {
    	

	$("#customer_want_date").click(function(){

	counter++;
	    
		// $("#result_container").text(counter);

		if(counter % 2 === 0) {

			var table = $('#table-2').DataTable();
			table.order.listener( '#customer_want_date', 18 );

			table.order([18, "desc"]).draw();


		} else {

			var table = $('#table-2').DataTable();
			table.order.listener( '#customer_want_date', 18 );

			table.order([18, "asc"]).draw();

		} 

	    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel  ");  

	}); // end of $("#sorter").click(function(){

}); // end of doc ready
</script>





<script type="text/javascript">

var counter = 1;
    
$(document).ready(function() {

	$("#actual_completion_date").click(function(){

	counter++;
	    
		// $("#result_container").text(counter);

		if(counter % 2 === 0) {

			var table = $('#table-2').DataTable();
			table.order.listener( '#actual_completion_date', 19 );

			table.order([19, "desc"]).draw();


		} else {

			var table = $('#table-2').DataTable();
			table.order.listener( '#sorter', 19 );

			table.order([19, "asc"]).draw();

		} 

	    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel  ");  

	}); // end of $("#sorter").click(function(){

}); // end of doc ready
</script>




<?php if($member_role === "global") { ?>

<script type="text/javascript">
(function($){
	$.fn.tableSearch = function(options){
		if(!$(this).is('table')){
			return;
		}
		var tableObj = $(this),
			divObj = $('<label>Search: </label>'),
			inputObj = $('<input class="form-control input-sm" type="text" />');
		inputObj.off('keyup').on('keyup', function(){
			var searchFieldVal = $(this).val();

			// var searchFieldVal = searchFieldVal.replace(' ', '');

			var searchFieldVal_1 = searchFieldVal.substr(0, 1).toUpperCase();
			var searchFieldVal_2 = searchFieldVal.substr(1);
			var searchFieldVal = searchFieldVal_1 + searchFieldVal_2;
			// console.log(searchFieldVal_2);
			tableObj.find('tbody tr').hide().each(function(){
				var currentRow = $(this);
				currentRow.find('td').each(function(){
					if($(this).html().indexOf(searchFieldVal)>-1){
						currentRow.show();
						return false;
					}
				});
			});
		});
		$("#custom_tableoptions button").after(divObj.append(inputObj).css({'float':'right', 'margin-top':'-17px', 'margin-right':'17px'}));
	}
}(jQuery));	
</script>


<script type="text/javascript">
$(document).ready(function(){
    $('#table-2').tableSearch();
});

</script>




<script>

var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = 'projects' + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};



</script>

<?php } ?>





<script type="text/javascript">
function HideCols_Global() {

	var fixedCols = [5, 13, 14, 16, 20, 21];

	var currentDate = new Date();
	var getMonth = currentDate.getMonth()+1;

	var Start=22; // Month wise data for 2018 starts from 20th col 
	for (var i = 12; getMonth < i; getMonth++) {

		Start+=1;
		// console.log(getMonth);
		fixedCols.push(Start);

	}

	// console.log(fixedCols);
	return fixedCols;
}


function AllCols() {

	var col = [];
	var total_td = $('#table-2 th').length - 1;
	console.log(total_td);
	for (var i = 0; i <= total_td; i++) {
		col.push(i);
	}

	// console.log(col);
	return col;
}
</script>
