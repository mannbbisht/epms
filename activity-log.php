<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>




<!-- DATEPICKER CSS -->
<link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<!-- DATATABLES CSS -->
<link rel="stylesheet" href="vendor/DataTables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.bootstrap4.min.css">


<!-- JS GRID CSS -->
<link rel="stylesheet" href="vendor/jsgrid/dist/jsgrid.css">
<link rel="stylesheet" href="vendor/jsgrid/dist/jsgrid-theme.min.css">

<!-- MULTI SELECT CSS -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.2/multiple-select.min.css">



<style type="text/css">
#jsGrid-basic .ms-drop ul>li{
    float: left !important;
    width: 100% !important;
    margin: 0 0 0 -24px !important;
}
#jsGrid-basic .ms-drop ul>li label{
    /*float: left !important;*/
    /*margin: 0 0 0 -50px !important;*/
}
#jsGrid-basic .ms-drop ul>li label{
    float: left !important;
    width: 65% !important;
}
input[type='radio'] {
/*    width: 100% !important;*/
}
#jsGrid-basic .ms-choice{
    border: 1px solid white;
    color: black;
    height: 23px;
    font-size: 13px;
}
#jsGrid-basic .ms-choice div{
    color: black;
}
#jsGrid-basic .ms-drop{
    width: auto !important;
}
.multiselect-container>li>a>label.checkbox{
    /*padding: 5px 10px !important;   */
}
.selectpicker1{
    position: absolute !important;
    width: 140px !important;
    margin: 0 0 0 -70px !important;
}
/*.selectpicker .ms-drop{
    position: unset !important;
    overflow: unset !important;
}*/
.col-md-1{
    margin: 0 !important;
    width: auto !important; 
}
.selectpicker, #filter_options select{
    width: 140px !important;
}
.jsgrid-header-row>.jsgrid-header-cell{
    color: black !important;
}
td,th{
    text-align: center !important;
}
@-webkit-keyframes yellow-fade {
   0% {background: #2980B9; color: white;}
   100% {background: none; color: auto;}
}
@keyframes yellow-fade {
   0% {background: #2980B9; color: white;}
   100% {background: none; color: auto;}
}
.new_li {
   -webkit-animation: yellow-fade 2s ease-in 1;
   animation: yellow-fade 2s ease-in 1;
}
.hide{
    display: none;
}
td{
    word-wrap: break-word;    
}
</style>




<?php /********************************************* APPLY FILTER ON PAGE SUBMIT *********************************************/ ?>




<?php

if(isset($_POST['submit']) && !empty($_POST['submit'])) {


    $sel_month = $_POST['choose_month'];
    $param_emp_name = $_POST['choose_employee'];
    $param_type = $_POST['choose_project_type'];
    $param_country = $_POST['choose_country'];
    $param_project_num = $_POST['choose_order_number'];

    // SPLIT UP ELEMENTS OF ARRAY
    $all_months = implode(",", $sel_month);
    $all_employees = implode(",", $param_emp_name);
    $all_types = implode(",", $param_type);
    $all_countries = implode(",", $param_country);
    $all_order_number = implode(",", $param_project_num);

    // echo "test";
    // print_r($sel_month);
    // print_r($param_emp_name);

    // INSERT INTO FILTERS TABLE
        
    $sql_insert_in_filters = "INSERT into  `$gd`.`filters` (fk_employee_id,months,employee,project_type,country,order_number,url,added_on) VALUES ('$session_employee_id','$all_months','$all_employees','$all_types','$all_countries','$all_order_number','$complete_url','$india_time')  ";

    mysql_query($sql_insert_in_filters, $connect_db);


} else {

    // $sel_month = "$this_month";

}

?>






            <div class="site-content">
                <!-- Content -->
                <div class="content-area py-1">
                    <div class="container-fluid">
                        <h4> Activity Log </h4>
                        <ol class="breadcrumb no-bg mb-1">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active"> Activity Log </li>
                        </ol>



                        <div id="animated_image"></div>
                        <div id="result_container"></div>


                        <?php if(!empty($_POST)) { ?>

                        <div class="alert alert-success-fill alert-dismissible fade in">

                            <?php 

                                $message = "You have selected: ";


                                if(!empty($sel_month) && $sel_month != "0all"  ) {
                                    // $message .= "Month: " . date("F", mktime(0, 0, 0, $all_months, 10));

                                    $message .= "Month: " ;

                                    foreach ($sel_month as $key => $value) {
                                        $message .= date("F", mktime(0, 0, 0, $sel_month[$key], 10));
        
                                        $count = count($sel_month);
                                        $count = $count - 1;
                                        
                                        if($key < $count) {
                                            $message .= ", ";
                                        }


                                    }

                                    $showComma = true;

                                } 



                                if(!empty($param_project_num)) {

                                    if($showComma === true) {
                                        $message .= ",";
                                    }

                                    $message .= " Order Number: " . $all_order_number;

                                    $showComma = true;

                                }


                                if(!empty($param_emp_name)) {

                                    if($showComma === true) {
                                        $message .= ",";
                                    }

                                    $message .= " Employee: " . $all_employees;

                                    $showComma = true;
                                }


                                if(!empty($param_country)) {

                                    if($showComma === true) {
                                        $message .= ",";
                                    }

                                    if(count($param_country) > 1) {
                                        $message .= " Countries: " . implode(",", $param_country);
                                    } else {
                                        $message .= " Country: " . implode(",", $param_country);
                                    }

                                    $showComma = true;
                                }

                                if(!empty($param_type)) {

                                    if($showComma === true) {
                                        $message .= ",";
                                    }

                                    $message .= " Project Type: " . implode(",", $param_type);

                                    $showComma = true;

                                }


                                echo $message;

                            ?>

                        </div>

                        <?php } ?>



                        <nav class="box box-block bg-white">
                            <h5 class="mb-1"> Filters </h5>

                            <form method="post" id="filter_options" name="filter_options">

                                <div class="row">
                                    <div class="col-md-1">
                                        <select id="choose_month" name="choose_month[]" data-placeholder="Select Month" multiple>

                                            <?php echo Last12Months_Dropdown(); ?>

                                        </select>
                                    </div>



                                    <?php if($member_role == "admin") { ?>


                                    <div class="col-md-1">
                                        <select id="choose_order_number" name="choose_order_number[]" data-placeholder="Select Order Number" multiple >


                                            <!-- <option value="all"> All </option> -->

                                            <option value="Leave"> Leave </option>
                                            <option value="Training"> Training </option>
                                            <?php 
                                                $sql_fetch_order_number = mysql_query("SELECT order_number FROM `$gd`.`projects` where order_number != 'Leave' and order_number != 'Training' order by order_number asc ", $connect_db);
                                                while ($row_fetch_order_number = mysql_fetch_array($sql_fetch_order_number)) {
                                                    extract($row_fetch_order_number);

                                                ?>

                                                <option value="<?php echo $order_number; ?>"  ><?php echo $order_number; ?></option>

                                                <?php }
                                            ?>

                                        </select>
                                    </div>         



                                    <div class="col-md-1">
                                        <select id="choose_employee" name="choose_employee[]" data-placeholder="Select Employee" multiple >

                                            <!-- <option value="">-- Select Name --</option> -->

                                            <?php 
                                                $sql_fetch_all_employees = mysql_query("SELECT * FROM `$gd`.`employees` order by full_name asc ", $connect_db);
                                                while ($row_fetch_all_employees = mysql_fetch_array($sql_fetch_all_employees)) {
                                                    extract($row_fetch_all_employees);

                                                ?>

                                                <option value="<?php echo $full_name; ?>"  ><?php echo $full_name; ?></option>

                                                <?php }
                                            ?>


                                        </select>
                                    </div>                                    


    



                                    <div class="col-md-1">
                                        <select id="choose_employee" name="choose_project_type[]"  data-placeholder="Select Type" multiple >

                                            <!-- <option value="">-- Select Name --</option> -->

                                            <?php $sql_project_type = mysql_query("SELECT * FROM `$gd`.`project_type_dropdown`  ", $connect_db); ?>                            
                                            <?php while($row_project_type = mysql_fetch_array($sql_project_type)){ ?>
                                                <option value="<?php echo $project_type = $row_project_type['project_type']; ?>" >
                                            <?php echo $project_type; ?> 
                                                </option>
                                            <?php } ?>


                                        </select>
                                    </div>    


                                    <div class="col-md-1">
                                        <select id="choose_country" name="choose_country[]"  data-placeholder="Select Location" multiple >


                                            <?php $sql_location = mysql_query("SELECT * FROM `$gd`.`location_dropdown` order by country asc ", $connect_db); ?>                            
                                            <?php while($row_location = mysql_fetch_array($sql_location)){ ?>
                                                <option value="<?php echo $country = $row_location['country']; ?>" >
                                            <?php echo $country; ?> 
                                                </option>
                                            <?php } ?>


                                        </select>
                                    </div>    

                                    <?php } ?>

                                    <div class="col-md-2">
                                        <input type="submit" class="btn btn-primary" name="submit" value="Submit" />
                                    </div>


                                </div>


                            </form>



                        </nav>                        


                        <div class="box box-block bg-white">
                                <div class="dt-buttons btn-group">
                                    <a href="#" class="btn btn-secondary">  </a>
                                </div>

                                <br/><br/>




                            <!-- <p class="mb-1"> Showing results for:  </p> -->
                            <div id="jsGrid-basic">
                            </div>
                        </div><!-- 
                        <div class="box box-block bg-white">
                            <h5 class="mb-1">Static Scenario</h5>
                            <div id="jsGrid-static"></div>
                        </div>
                        <div class="box box-block bg-white">
                            <h5 class="mb-1">Sorting</h5>
                            <select class="custom-select mb-1" id="sortingField">
                                <option>Name</option>
                                <option>Age</option>
                                <option>Address</option>
                                <option>Country</option>
                                <option>Married</option>
                            </select>
                            <div id="jsGrid-sort"></div>
                        </div> -->
                    </div>
                </div>



<?php include $backend_footer_file; ?>


<!-- <script type="text/javascript" src="js/tables-jsgrid.js"></script> -->
<script type="text/javascript" src="vendor/jquery-fullscreen-plugin/jquery.fullscreen-min.js"></script>
<script type="text/javascript" src="vendor/waves/waves.min.js"></script>
<script type="text/javascript" src="vendor/switchery/dist/switchery.min.js"></script>
<script type="text/javascript" src="vendor/jsgrid/dist/jsgrid.min.js"></script>
<script type="text/javascript" src="vendor/jsgrid/demos/db.js"></script>





<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>



<!-- edit modal validation -->
<script>   
$(document).ready(function() {
  $("#filter_options").validate();
});  // END OF FUNCTION 
</script>

<!-- <script type="text/javascript">
function loadReport() {

    var month = $('#choose_month').find(":selected").val();
    var employee_name = $('#selected_emp_name').find(":selected").val();

    $("#animated_image").show();
    $("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo loading_data; ?></span>');

    if(employee_name == '') {

        $.post('activity_loadmore.php', '&month_value='+month , function(response) {
            $("#animated_image").hide();
            $('#jsGrid-basic').html(response);
        });

        
    } else if(month != '' && employee_name != '' ) {

        $.post('activity_loadmore.php', '&month_value='+month + '&employee_name='+employee_name , function(response) {
            $("#animated_image").hide();
            $('#jsGrid-basic').html(response);
        });

    }


}

function filterByName() {

    var employee_name = $('#selected_emp_name').find(":selected").val();

    $("#animated_image").show();
    $("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo loading_data; ?></span>');

    $.post('activity_loadmore.php', '&employee_name='+employee_name , function(response) {
        $("#animated_image").hide();
        $('#jsGrid-basic').html(response);
    });

}

</script> -->


<!-- <?php echo "$session_full_name $member_role"; ?> -->

<script type="text/javascript">
$(document).ready(function() {

    window.db = db;


    <?php


        if($member_role == 'admin') {
            $query_projects =  mysql_query("SELECT order_number FROM `$gd`.`projects`  where order_number != 'Leave' and order_number != 'Training'  and order_number != 'IN00003' ", $connect_db);

            $query_get_all_emp =  mysql_query("SELECT full_name FROM `$gd`.`employees`   ", $connect_db);

        } else {


            $this_month_custom = str_replace("-", "/", $this_month);
            $query_projects =  mysql_query(" SELECT order_number FROM `projects`  where assign_to  like  '%$session_full_name%' and order_number != 'IN00003' and  (status != 'Completed' or actual_completion_date like '%__/$this_month_custom%' )  ", $connect_db);

        }         

        ?>



        <?php

        if(mysql_num_rows($query_projects) < 1) {

                $output_projects = [];


                array_unshift($output_projects, ['OrderNumber'=>'Leave', 'ProjectName'=>'', 'Id' => '']);
                array_unshift($output_projects, ['OrderNumber'=>'Training', 'ProjectName'=>'', 'Id' => '']);
                array_unshift($output_projects, ['OrderNumber'=>'IN00003', 'ProjectName'=>'', 'Id' => '']);

        }

        if(mysql_num_rows($query_projects) > 0) {

            while($rows_projects = mysql_fetch_array($query_projects)) {
                extract($rows_projects);


                $output_projects[] = array(
                    "OrderNumber" => trim($order_number),
                    "ProjectName" => $project_name,
                    "Id" => $project_name
                );


            }


            sort($output_projects);

            array_unshift($output_projects, ['OrderNumber'=>'Leave', 'ProjectName'=>'', 'Id' => '']);
            array_unshift($output_projects, ['OrderNumber'=>'Training', 'ProjectName'=>'', 'Id' => '']);
            array_unshift($output_projects, ['OrderNumber'=>'IN00003', 'ProjectName'=>'', 'Id' => '']);




            while($rows_get_all_emp = mysql_fetch_array($query_get_all_emp)) {
                extract($rows_get_all_emp);

                $emp_names[] = array(
                    "Id" => $full_name,
                    "FullName" => $full_name
                );



            }




        }


    ?>            

    all_project_names = <?php echo json_encode($output_projects); ?>;
    all_emp_names = <?php echo json_encode($emp_names); ?>;


var currentTime = new Date();
// First Date Of the month 
var startDateFrom = new Date(currentTime.getFullYear(),currentTime.getMonth(),1);

// Last Date Of the Month 
var startDateTo = new Date(currentTime.getFullYear(),currentTime.getMonth() +1,0);


    <?php
        /*********************************** JUST FOR DISPLAYING VALUE IN EXISTING ROWS ***********************************/ 

        $query_location =  mysql_query("SELECT * FROM `$gd`.`location_dropdown` ", $connect_db);
        while($row_location = mysql_fetch_array($query_location)) {
            extract($row_location);

            $output_location[] = array(
                "country" => $country,
                "Id" => $country
            );
        }

    ?>            

    country_list = <?php echo json_encode($output_location); ?>;



    jsGrid.setDefaults({
        tableClass: "jsgrid-table table table-striped table-hover jsgrid_body"
    }), jsGrid.setDefaults("text", {
        _createTextBox: function() {
            return $("<input>").attr("type", "text").attr("class", "form-control input-sm")
        }
    }), jsGrid.setDefaults("number", {
        _createTextBox: function() {
            return $("<input>").attr("type", "number").attr("class", "form-control input-sm")
        }
    }), jsGrid.setDefaults("textarea", {
        _createTextBox: function() {
            return $("<input>").attr("type", "textarea").attr("class", "form-control")
        }
    }), jsGrid.setDefaults("control", {
        _createGridButton: function(cls, tooltip, clickHandler) {
            var grid = this._grid;
            return $("<button>").addClass(this.buttonClass).addClass(cls).attr({
                type: "button",
                title: tooltip
            }).on("click", function(e) {
                clickHandler(grid, e)
            })
        }
    }), jsGrid.setDefaults("select", {
        _createSelect: function() {
            var $result = $("<select>").attr("class", "form-control form-control-sm"),
                valueField = this.valueField,
                textField = this.textField,
                selectedIndex = this.selectedIndex;
            return $.each(this.items, function(index, item) {
                var value = valueField ? item[valueField] : index,
                    text = textField ? item[textField] : item,
                    $option = $("<option>").attr("value", value).text(text).appendTo($result);
                $option.prop("selected", selectedIndex === index)
            }), $result
        }
    }),

    function() {

        $("#jsGrid-basic").jsGrid({
            height: "1000px",
            width: "100%",
            filtering: false,
            selecting: true,

            editing: true,
            <?php if($member_role == "admin") { ?>
            inserting: true,
            sorting: true,
            <?php } elseif ($member_role == "user") {
                if(!empty($all_months) && $all_months == $this_month ) { ?>
                  inserting: true,
                  editing: true,
                <?php } else if(!empty($all_months) && $all_months != $this_month ) { ?>
                    editing: false,
                <?php } else if (empty($all_months)) { ?>
                  inserting: true,
                  editing: true,
                <?php } ?>
            <?php } ?> // end of else if
            paging: true,

            autoload: true,
            pageSize: 1000,
            insertRowLocation: "top",
            noDataContent: "No records found",
            pageButtonCount: 5,
            deleteConfirm: "Do you really want to delete this record?",
            controller: db,
            onItemEditing: function(args, item, grid){

                setTimeout(function(){
                    $(".jsgrid-cell .date").val(args.item.RecordDate);
                }, 100);

                // console.log(args.item.RecordDate);
            },
            onItemInserted: function(args) {  // on done of controller.insertItem
                // document.getElementByClassName("jsgrid-row")[0].add("mystyle");
                // $('.jsgrid-row:first').addClass('new_li');
                // $('.jsgrid-row').eq(2).addClass('new_li');
                // jQuery("#" + args.rowId, "#myGrid").addClass("new_li");

            },
            onItemUpdated: function(args) {
                // console.log(args.row);
                $(args.row).animate({backgroundColor: 'green'}, 'slow');
            },
            onOptionChanging: function() {

            },
            controller: {
                // loadData: function(filter) {
                //     <?php if(!empty($sel_month) && empty($param_emp_name) ) { ?>
                //     return $.ajax({
                //         type: "GET",
                //         url : "php/jsgrid_loadmore.php?month=<?php echo implode(",", $sel_month); ?>",
                //         data: filter
                //     });
                //     <?php } else if(empty($sel_month) && !empty($param_emp_name) ) { ?>
                //     return $.ajax({
                //         type: "GET",
                //         url : "php/jsgrid_loadmore.php?employee_name=<?php echo implode(",", $param_emp_name); ?>",
                //         data: filter
                //     });
                //     <?php } else if(!empty($sel_month) && !empty($param_emp_name) ) { ?>
                //     return $.ajax({
                //         type: "GET",
                //         url : "php/jsgrid_loadmore.php?month=<?php echo implode(",", $sel_month); ?>&employee_name=<?php echo implode(",", $param_emp_name); ?>",
                //         data: filter
                //     });
                //     <?php } else { ?>
                //     return $.ajax({
                //         type: "GET",
                //         url : "php/jsgrid_loadmore.php?month=4",
                //         data: filter
                //     });
                //     <?php } ?>
                // },

                loadData: function(filter) {
                    <?php if(empty($sel_month) && empty($param_project_num) && empty($param_emp_name) && empty($param_type) && empty($param_country)  ) { ?>
                    return $.ajax({
                        type: "GET",
                        url : "php/jsgrid_loadmore.php?month=all",
                        data: filter
                    });
                    <?php } else { ?>
                    return $.ajax({
                        type: "GET",
                        url : "php/jsgrid_loadmore.php?month=<?php echo $all_months; ?>&order_number=<?php echo $all_order_number; ?>&employee_name=<?php echo $all_employees; ?>&type=<?php echo $all_types; ?>&country=<?php echo $all_countries; ?>",
                        data: filter
                    });
                    <?php } ?> 
                },

                insertItem: function(item){
                    return $.ajax({
                        type: "POST",
                        url : "php/jsgrid.php",
                        data: item,
                        success: function() {
                            $("#jsGrid-basic").jsGrid("loadData");

                            // location.reload();
                        }
                    });
                },
                updateItem: function(item){
                    return $.ajax({
                        type: "PUT",
                        url : "php/jsgrid.php",
                        data: item,
                        success: function() {
                            $("#jsGrid-basic").jsGrid("loadData");
                            // location.reload();
                        }
                    });
                },
                deleteItem: function(item){
                    return $.ajax({
                        type: "DELETE",
                        url : "php/jsgrid.php",
                        data: item
                    });
                }

            },
            fields: [
                { name: "PkActivityId", visible: false, type: "text", width: 0 },

                { 

                    name: "RecordDate", 
                    editing: true,
                    title: "Date", 
                    type: "text", 
                    width: 60,
                    validate: "required",


                    <?php if($member_role == "admin") { ?>

                        insertTemplate: function(value) {
                            return this._editPicker = $("<input>").datepicker({todayHighlight: true,format: 'dd/mm/yyyy' }).datepicker();
                        },
                        editTemplate: function(value) {
                            return this._editPicker = $("<input class='date'>").datepicker({todayHighlight: true,format: 'dd/mm/yyyy' }).datepicker();
                        },

                    <?php } else { ?>

                        // insertTemplate: function(value) {
                        //     return this._editPicker = $("<input>").datepicker({todayHighlight: true,format: 'dd/mm/yyyy' }).datepicker();
                        // },
                        // editTemplate: function(value) {
                        //     return this._editPicker = $("<input class='date'>").datepicker({todayHighlight: true,format: 'dd/mm/yyyy' }).datepicker();
                        // },


                        insertTemplate: function(value) {
                            return this._editPicker = $("<input>").datepicker({todayHighlight: true,format: 'dd/mm/yyyy', startDate: startDateFrom, endDate: startDateTo}).datepicker();
                        },
                        editTemplate: function(value) {
                            return this._editPicker = $("<input class='date'>").datepicker({todayHighlight: true,format: 'dd/mm/yyyy', startDate: startDateFrom, endDate: startDateTo }).datepicker();
                        },

                    <?php } ?>
                    
                    insertValue: function(value) {
                        return this._editPicker.datepicker({   format: 'dd/mm/yyyy' }).val();
                    },
                    editValue: function(value,item) {
                        return this._editPicker.datepicker({ format: 'dd/mm/yyyy', startDate: '-3d' }).val();
                    }


                },

                { 

                    name: "OrderNumber", 
                    <?php if(!empty($all_months) && $all_months != $this_month && $member_role == "user") { ?>
                    type: "text",
                    <?php } else { ?>
                    type: "select",
                    <?php } ?>
                    width: 100,
                    title: "Order Number", 
                    items: all_project_names,
                    valueField: "OrderNumber",
                    textField: "OrderNumber",


                    <?php if($member_role == "admin") { ?>

                    insertTemplate: function(value, item) {

                        var $select = jsGrid.fields.select.prototype.insertTemplate.call(this);
                        $select.addClass('selectpicker1');
                        
                        setTimeout(function() {
                            $select.multipleSelect({
                                filter: true,
                                single: true
                            })
                        });
                        
                        return $select;

                    },

                    editTemplate: function (value) {

                        // Retrieve the DOM element (select)
                        // Note: prototype.editTemplate
                        var $editControl = jsGrid.fields.select.prototype.editTemplate.call(this, value);
                        $editControl.addClass('selectpicker');


                        setTimeout(function() {
                            $editControl.multipleSelect({
                                filter: true,
                                single: true
                            })
                        });

                        return $editControl;

                    }


                    <?php } ?>

                },


                { 
                    name: "ProjectName", 
                    type: "text1", 
                    width: 100,
                    editing: false,
                    title: "Project Title", 
                    items: all_project_names,
                    valueField: "Id",
                    textField: "ProjectName"
                },

                <?php if($member_role == "admin") { ?>



                { 
                    name: "Location", 
                    type: "select", 
                    width: 50,
                    editing: true,
                    title: "Location", 
                    items: country_list,
                    valueField: "Id",
                    textField: "country"
                },


                <?php } else if($member_role == "user") { ?>

                { 
                    name: "Location", 
                    type: "text1", 
                    width: 50,
                    editing: false,
                    title: "Location", 
                    items: country_list,
                    valueField: "Id",
                    textField: "country"
                },

                <?php } ?>

                { 
                    name: "DesignEngineer", 
                    type: "text", 
                    title: "Design Engineer", 
                    
                    <?php if($member_role == "admin") { ?>

                    type: "select", 
                    title: "Design Engineer", 
                    items: all_emp_names,
                    valueField: "Id",
                    textField: "FullName",
                    width: 50,

                    <?php } else { ?>

                    readonly: true,
                    css: "hide",

                    insertTemplate: function(value) {
                        return "<?php echo $session_full_name; ?>"
                    },
                    editTemplate: function(value) {
                        return "<?php echo $session_full_name; ?>"
                    },
                    insertValue: function(value) {
                        return "<?php echo $session_full_name; ?>"
                    },
                    editValue: function(value,item) {
                        return "<?php echo $session_full_name; ?>"
                    }

                    <?php } ?>





                },


                { name: "ProjectType", type: "text1", title: "Project Type"},

                { 
                    name: "ActivityCarriedOut", 
                    type: "textarea", 
                    title: "Activity Carried Out",
                    validate: "required"

                },

                { 
                    name: "WorkingHours", 
                    type: "text", 
                    title: "Working Hours", 
                    validate: "required",
                    width: 50, 
                    insertTemplate: function(value) { 
                        this.insertControl = $("<input>");
                        insert_on_enter(this.insertControl);
                        return this.insertControl;
                    },

                    editTemplate: function(value) { 
                        this.editControl = $("<input>").val(value);
                        update_on_enter(this.editControl);
                        return this.editControl;
                    }

                },


                { 
                    type: "control", 

                    <?php if($member_role == "user") {
                        if(!empty($all_months) && $all_months != $this_month) { ?>

                        // deleteButton: false,
                        // editButton: false 

                    <?php } } ?> 
                }


            ]
        });


    }()



}); 
</script>


<script type="text/javascript">
$(document).ready(function(){
    $('#reset').click(function(){
        location.href = '<?php echo $file_path[45]; ?>';
    });
});
</script>



<script type="text/javascript" src="vendor/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="vendor/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script type="text/javascript" src="vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="vendor/moment/moment.js"></script>
<script type="text/javascript" src="vendor/bootstrap-daterangepicker/daterangepicker.js"></script>

<script type="text/javascript">

$('.mydatepicker').datepicker({
   autoclose: true
}); 
</script>




<script src="//cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.2/multiple-select.min.js"></script>


<script>
  // Here the mounted function is same as $(function() {})
$(document).ready(function(){
    $('#filter_options select').multipleSelect({
        filter: true
    })
});
</script>


<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $('.multiselect-ui').multiselect({
            numberDisplayed: 1,
            nonSelectedText: 'Select',
            onChange: function(option, checked) {
                // Get selected options.
                var selectedOptions = $('.multiselect-ui option:selected');
 
                // if (selectedOptions.length >= 4) {
                //     // Disable all other checkboxes.
                //     var nonSelectedOptions = $('.multiselect-ui option').filter(function() {
                //         return !$(this).is(':selected');
                //     });
 
                //     nonSelectedOptions.each(function() {
                //         var input = $('input[value="' + $(this).val() + '"]');
                //         input.prop('disabled', true);
                //         input.parent('li').addClass('disabled');
                //     });
                // }
                // else {
                //     // Enable all checkboxes.
                //     $('.multiselect-ui option').each(function() {
                //         var input = $('input[value="' + $(this).val() + '"]');
                //         input.prop('disabled', false);
                //         input.parent('li').addClass('disabled');
                //     });
                // }
            }
        });
    });
</script> -->


<script type="text/javascript">
$(document).ready(function(){
    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel ");  
});


$(document).ready(function(){
    $(".dt-buttons a").click(function() {

        window.open('phpexcel/Examples/download-file.php?month=<?php echo $all_months; ?>&order_number=<?php echo $all_order_number; ?>&employee_name=<?php echo $all_employees; ?>&type=<?php echo $all_types; ?>&country=<?php echo $all_countries; ?>');

    });
});
</script>





<script type="text/javascript">
function insert_on_enter(field) {
  field.on("keydown", function(e) {
    if(e.keyCode === 13) {
      $("#jsGrid-basic").jsGrid("insertItem");
      $("#jsGrid-basic").jsGrid("clearInsert");
      return false;
    }
  });
}
</script>


<script type="text/javascript">
function update_on_enter(field) {
  field.on("keydown", function(e) {
    if(e.keyCode === 13) {
      $("#jsGrid-basic").jsGrid("updateItem");
      return false;
    }
  });
}    
</script>




<?php if(!empty($all_months) && $all_months != $this_month && $member_role == "user") { ?>

<script type="text/javascript">
$(document).ready(function() { // remove the column for editing/deleting/insert icon from right side

    setTimeout(function(){
        $(".jsgrid_body tbody tr th:nth-child(9), .jsgrid_body tbody tr td:nth-child(9)").css('display', 'none');    
    }, 100);


});
</script>

<?php } ?>










