<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>

<?php 

$name = GetLast12MonthsName();

?>				


<link rel="stylesheet" href="vendor/select2/dist/css/select2.min.css">

<link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="vendor/DataTables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.bootstrap4.min.css">

<style type="text/css">
.error{ color: red; }
.dt-buttons{ float: right !important; margin: 0 0 0 20px; }
.buttons-copy{display: none}
/*.buttons-excel::before { Content: 'Download as ' }
.buttons-csv::before { Content: 'Download as ' }
.buttons-pdf::before { Content: 'Download as ' }
*/td,th {
	text-align: center;
}

</style>

<style type="text/css">
.error{ color: red; }
select{height: 40px !important;}
</style>


<?php

$month_names = GetLast12MonthsName();

?>

			<div class="site-content">
				<!-- Content -->
				<div class="content-area py-1">
					<div class="container-fluid">
						<h4> Invoice Summary </h4>
						<ol class="breadcrumb no-bg mb-1">
							<li class="breadcrumb-item"><a href="#">Home </a></li>
							<li class="breadcrumb-item active"> Invoice Summary </li> 


						</ol>


						<div id="animated_image"></div>
						<div id="result_container"></div>






							<div class="box box-block bg-white">

                                <div class="dt-buttons btn-group">

									<button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal" style="float: right;"> <i class="fa fa-plus"></i> &nbsp; Add Invoice </button>

                                </div>



								<table class="table table-striped table-bordered dataTable" id="table-2">
									<thead>

										<tr>

											<th> Month </th>
											<th> NASH-US </th>
											<th> NASH-EUROPE </th>
											<th> MEDICAL </th>
											<th> IPG-US  </th>

											<th class="noExport"> Edit </th>
											<th class="noExport"> Delete </th>

										</tr>


									</thead>

									<tbody>




									<?php 

										$sql_fetch_summary = mysql_query("SELECT *  FROM `$gd`.`invoice_summary` order by STR_TO_DATE(month, '%d-%m-%Y') asc  ", $connect_db);
										while ($row_fetch_summary = (mysql_fetch_array($sql_fetch_summary)) ){
											extract($row_fetch_summary);
												
										?>

										<tr id="<?php echo $pk_invoice_id; ?>">

											<td> <?php echo date("F Y", strtotime($month)); ?> </td> 

											<td> &dollar; <?php echo $row_fetch_summary[2]; ?> </td>
											<td> &dollar; <?php echo $row_fetch_summary[3]; ?> </td>
											<td> &dollar; <?php echo $row_fetch_summary[4]; ?> </td>
											<td> &dollar; <?php echo $row_fetch_summary[5]; ?> </td>

											<td> <a href="#!" onclick="load_invoice(<?php echo $pk_invoice_id; ?>);"> Edit </a> </td>
											<td> <a href="#!" onclick="delete_invoice(<?php echo $pk_invoice_id; ?>);"> Delete </a> </td>

										</tr>

									<?php } ?>

									</tbody>

								</table>	

						</div>


					</div>
				</div>



<?php include $backend_footer_file; ?>

<script type="text/javascript" src="vendor/select2/dist/js/select2.min.js"></script>


<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>



<script type="text/javascript">
$(document).ready(function(){
    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel ");  
});
</script>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">


				<form id="add_new_invoice" name="add_new_invoice" class="form-material material-primary">

	            	<div class="modal-header">

						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="exampleModalLabel"> Add Invoice  </h4>

	            	</div>

	            	<div class="modal-body">



						<div class="form-group">
							<div class="row">
							                                       

								<div class="col-sm-6">
									<label class="control-label"> Month:  </label>
									<select id="month" name="month" class="form-control">

										<option value=""> --Select-- </option>

										<?php echo Last12Months_Dropdown(); ?>


									</select>
								</div>



								                                    
							</div>
						</div>






						<div class="form-group">
							<div class="row">

								<div class="col-sm-6"> 

									<label for="nash_us_total_charge" class="col-form-label"> Nash US Total Charge: </label>
									<div class="">
										<input type="number" class="form-control" id="nash_us_total_charge" name="nash_us_total_charge" placeholder="1,659" required>
									</div>

								</div>


								<div class="col-sm-6">

									<label for="nash_eur_total_charge" class="col-form-label"> Nash EUR Total Charge: </label>
									<div class="">
										<input type="number" class="form-control" id="nash_eur_total_charge" name="nash_eur_total_charge" placeholder="2,631" required>
									</div>

								</div>

							</div>
						</div>



						<div class="form-group">
							<div class="row">

								<div class="col-sm-6"> 

									<label for="medical_total_charge" class="col-form-label"> Medical Total Charge: </label>
									<div class="">
										<input type="number" class="form-control" id="medical_total_charge" name="medical_total_charge" placeholder="463" required>
									</div>

								</div>


								<div class="col-sm-6">

									<label for="nash_eur_total_charge" class="col-form-label"> IPG Total Charge: </label>
									<div class="">
										<input type="number" class="form-control" id="ipg_total_charge" name="ipg_total_charge" placeholder="1,156" required>
									</div>

								</div>

							</div>
						</div>


	            	</div>


				    <div class="modal-footer">
				    	<button type="submit" class="btn btn-primary">Submit</button>
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>

				</form>

    </div>

  </div>
</div>




<!--- new_project form validation --> 
<script>   
$(document).ready(function(){
  $("#add_new_invoice").validate({
    debug: false,
    submitHandler: function(form) {


    	$("#myModal").modal('hide');

    	scrollToTop();

      	$("#animated_image").show();
      	$("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

      	$.post('<?php echo $file_path[30]; ?>', $("#add_new_invoice").serialize() + '&add_new_invoice=add_new_invoice', function(response) {

            $('#add_new_invoice').find("input[type=text],input[type=password],input[type=email],textarea,select").val("");

			$("#animated_image").hide();
			$("#result_container").html(response);



      	}).done(function() {

      		setTimeout(function(){
      			RefreshPage();
      		}, 1000);

      	}); // END OF POST REQUEST 


    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>











<script type="text/javascript">
function load_invoice(pk_id) {

	$("#update_invoice input[type='text'],[type='number'],select").val('');

	$.post('php/get_value.php', '&FetchInvoiceDetails=FetchInvoiceDetails' + '&invoice_id='+pk_id, function(response1){

		// $('#result_container').html(response1);


		var parse_values = JSON.parse(response1);

		$("#month_in_header").html(parse_values.month);

		// $("#update_month").val(parse_values.month);
		$("#update_nash_us_total_charge").val(parse_values.nash_us_total);
		$("#update_nash_eur_total_charge").val(parse_values.nash_eur_total);
		$("#update_medical_total_charge").val(parse_values.medical_total);
		$("#update_ipg_total_charge").val(parse_values.ipg_total);

		$("#invoice_id").val(parse_values.pk_invoice_id);

		$("#EditInvoiceInfo").modal();

		window.pk_invoice_id = pk_id;

		console.log(window.pk_invoice_id);

	});

}
</script>



<!-- load project edit details in modal -->
<div id="EditInvoiceInfo" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

				<form id="update_invoice" name="update_invoice" class="form-material material-primary">





	            	<div class="modal-header">

						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="exampleModalLabel"> You have selected: <span id="month_in_header"></span>  </h4>

	            	</div>

	            	<div class="modal-body">


						<div class="form-group">
							<div class="row">

								<div class="col-sm-6"> 

									<label for="nash_us_total_charge" class="col-form-label"> Nash US Total Charge: </label>
									<div class="">
										<input type="number" class="form-control" id="update_nash_us_total_charge" name="update_nash_us_total_charge" placeholder="1,659" required>
									</div>

								</div>


								<div class="col-sm-6">

									<label for="nash_eur_total_charge" class="col-form-label"> Nash EUR Total Charge: </label>
									<div class="">
										<input type="number" class="form-control" id="update_nash_eur_total_charge" name="update_nash_eur_total_charge" placeholder="2,631" required>
									</div>

								</div>

							</div>
						</div>



						<div class="form-group">
							<div class="row">

								<div class="col-sm-6"> 

									<label for="medical_total_charge" class="col-form-label"> Medical Total Charge: </label>
									<div class="">
										<input type="number" class="form-control" id="update_medical_total_charge" name="update_medical_total_charge" placeholder="463" required>
									</div>

								</div>


								<div class="col-sm-6">

									<label for="nash_eur_total_charge" class="col-form-label"> IPG Total Charge: </label>
									<div class="">
										<input type="number" class="form-control" id="update_ipg_total_charge" name="update_ipg_total_charge" placeholder="1,156" required>
									</div>

								</div>

							</div>
						</div>


	            	</div>


				    <div class="modal-footer">
				    	<button type="submit" class="btn btn-primary">Submit</button>
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>


					<input type="hidden" id="invoice_id" name="invoice_id" value="">

				</form>

            </div>
        </div>
</div>






<!--- new_project form validation --> 
<script>   
$(document).ready(function(){
  $("#update_invoice").validate({
    debug: false,
    submitHandler: function(form) {


    	$("#EditInvoiceInfo").modal('hide');

    	scrollToTop();

      	$("#animated_image").show();
      	$("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

      	$.post('<?php echo $file_path[31]; ?>', $("#update_invoice").serialize() + '&update_invoice=update_invoice', function(response) {

            $('#update_invoice').find("input[type=text],input[type=password],input[type=email],textarea,select").val("");

			$("#animated_image").hide();
			$("#result_container").html(response);

      	}).done(function() {

      		setTimeout(function(){
      			RefreshPage();
      		}, 1000);

      	}); // END OF POST REQUEST 




    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>







<script type="text/javascript">
function delete_invoice(invoice_id) {

 //      $.post('<?php echo $file_path[32]; ?>', '&delete_invoice=delete_invoice'  , function(response) {
	// 	$("#animated_image").hide();
	// 	$("#result_container").html(response);
	// });

    	$("#DeleteInvoiceModal").modal();

    	$("#delete_invoice_form #invoice_id").val(invoice_id);

    	window.invoice_id = invoice_id;


}

</script>





<!-- load project edit details in modal -->
<div id="DeleteInvoiceModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

				<form id="delete_invoice_form" name="delete_invoice_form" class="form-material material-primary">

	            	<div class="modal-header">

						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="exampleModalLabel"> Delete invoice  </h4>

	            	</div>

	            	<div class="modal-body">


				      <div style="padding: 2% 2% 2% 0%;"> Are you sure, you want to delete this invoice? This cannot be undone.</div>


	            	</div>


				    <div class="modal-footer">
				    	<button type="submit" class="btn btn-primary" onclick="RemoveRow();">Submit</button>
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>


					<input type="hidden" id="invoice_id" name="invoice_id" value="">

				</form>

            </div>
        </div>
</div>




<!--- new_project form validation --> 
<script>   
$(document).ready(function(){
  $("#delete_invoice_form").validate({
    debug: false,
    submitHandler: function(form) {

    	$("#DeleteInvoiceModal").modal('hide');

    	scrollToTop();

      	$("#animated_image").show();
      	$("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

      	$.post('<?php echo $file_path[32]; ?>', $("#delete_invoice_form").serialize() + '&delete_invoice=delete_invoice', function(response) {

			$("#animated_image").hide();
			$("#result_container").html(response);

      	}).done(function() {

      		setTimeout(function(){
      			RefreshPage();
      		}, 1000);

      	}); // END OF POST REQUEST 



    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>



<script type="text/javascript">
function RemoveRow() {

	$('#'+window.invoice_id).remove();

}
</script>




<script type="text/javascript">
function RefreshPage(){
	location.reload();
}
</script>