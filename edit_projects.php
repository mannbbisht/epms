 <!-- only admin has access to this page  -->

<link rel="stylesheet" href="vendor/select2/dist/css/select2.min.css">
<!-- MULTI SELECT CSS -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.2/multiple-select.min.css">
<style type="text/css">
.EditModalBody .multiselect-container>li>a>label.checkbox{
	padding: 5px 10px !important;	
}
.EditModalBody .ms-drop{
	margin-left: -10px !important;
}	
.EditModalBody .ms-choice{
	height: 19px !important;
	border: 0px solid #aaa !important;
}
.EditModalBody .ms-parent{
	width: 269px !important;
}

</style>


<?php 
include 'php/global_constants.php'; 
include $dynamic_links;

$project_id = $_POST['project_id'];

// get project details
$sql_get_ProjectInfo = mysql_query("SELECT * from  `$gd`.`projects` where pk_project_id = '$project_id' " , $connect_db);
	while ($row_get_ProjectInfo = (mysql_fetch_array($sql_get_ProjectInfo)) ){
	extract($row_get_ProjectInfo);
}

?>


	<div class="modal-header">

		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel"> You have selected: <?php echo $order_number; ?> </h4>
			      
	</div>


	<div class="modal-body EditModalBody">


			<div class="form-group">
				<div class="row">
				                                       

					<div class="col-sm-6"> 
						<label for="project_division" class="control-label"> Division: </label>
						<select name="division" class="form-control edit_division">

	                        	<option value="<?php echo $division; ?>"><?php echo $division; ?></option>
								<?php $sql_fetch_division = mysql_query("SELECT * FROM `$gd`.`project_division_dropdown` where project_division != '$division'  ", $connect_db); ?>                        	
	                            <?php while($row_fetch_division = mysql_fetch_array($sql_fetch_division)){ ?>
	                            <option value="<?php echo  $project_division = $row_fetch_division['project_division']; ?>" >
	                            <?php echo $project_division; ?> 
	                            </option>
	                            <?php } ?>

						</select>
					</div>

					<div class="col-sm-6">
						<label class="control-label"> Order Number  </label>
						<input name="order_number" class="form-control"  value="<?php echo $order_number; ?>">
					</div>
					                                    
				</div>
			</div>


			<div class="form-group">
				<div class="row">


					<div class="col-sm-6">
						<label for="project_name" class="control-label"> Location: </label>
						<select name="location" class="form-control edit_location" >

	                        	<option value="<?php echo $location; ?>"><?php echo $location; ?></option>
								<?php $sql_fetch_location = mysql_query("SELECT * FROM `$gd`.`location_dropdown` where country != '$location'  ", $connect_db); ?>                        	
	                            <?php while($row_fetch_location = mysql_fetch_array($sql_fetch_location)){ ?>
	                            <option value="<?php echo  $country = $row_fetch_location['country']; ?>" >
	                            <?php echo $country; ?> 
	                            </option>
	                            <?php } ?>


						</select>
					</div>

					<div class="col-sm-6"> 
						<label class="control-label"> Request From </label>
						<input name="request_from"  class="form-control edit_request_from" value="<?php print_value($request_from); ?>">
					</div>

				</div>
			</div>



			<div class="form-group">
				<div class="row">

					<div class="col-sm-6"> 
						<label class="control-label"> Project Title </label>
						<input name="projectname"  class="form-control edit_project_title" value="<?php echo $project_name; ?>">
					</div>


					<div class="col-sm-6">
						<label for="project_type" class="control-label"> Project Type: </label>
						<select name="project_type" class="form-control edit_project_type">

	                        <option value="<?php echo $project_type; ?>"><?php echo $project_type; ?></option>
							<?php $sql_fetch_project_type = mysql_query("SELECT * FROM `$gd`.`project_type_dropdown` where project_type != '$project_type'  ", $connect_db); ?>                        	
	                        <?php while($row_fetch_project_type = mysql_fetch_array($sql_fetch_project_type)){ ?>
	                        <option value="<?php echo  $project_type = $row_fetch_project_type['project_type']; ?>" >
	                        <?php echo $project_type; ?> 
	                        </option>
	                        <?php } ?>

						</select>
					</div>

				</div>
			</div>




			<div class="form-group">
				<div class="row">

					<div class="col-sm-6">
						<label for="project_type" class="control-label"> Description: </label>
						<textarea name="update_description"  class="form-control edit_description" rows="5" required> <?php echo $description; ?> </textarea>
					</div>


					<div class="col-sm-6">
						<label for="assigned_to" class="control-label"> Assigned To </label>

                        <select id="change_assign_to" multiple name="change_assign_to[]" class="form-control" required>


							<?php

							$assign_to_array = explode(", ", $assign_to);
							$assign_to_implode = "'" . implode("', '", $assign_to_array) . "'";

							foreach ($assign_to_array as $key => $value) { ?>

								<option value="<?php echo $value; ?>" selected> <?php echo $value; ?> </option>

							<?php } ?>


							<?php 

								$sql_fetch_names = mysql_query("SELECT * FROM `$gd`.`employees` where full_name not in ($assign_to_implode) order by full_name asc ", $connect_db);
								while ($row_fetch_names = mysql_fetch_array($sql_fetch_names)) {
									extract($row_fetch_names);
							?>

								<option value="<?php echo $full_name; ?>"><?php echo $full_name; ?></option>

							<?php } // END OF WHILE ?>


                        </select>

					</div>

				</div>
			</div>



			<div class="form-group">
				<div class="row">

					<div class="col-sm-6">
						<label for="order_recieved_date" class="control-label"> Order Received Date  </label>
						<input name="order_recieved_date"  class="form-control mydatepicker edit_order_recieved_date" autocomplete="off" value="<?php echo $order_recieved_date; ?>">
					</div>


					<div class="col-sm-6">
						<label for="project_name" class="control-label"> Status: </label>
						<select name="project_status" class="form-control project_status" >

	                        	<option value="<?php echo $status; ?>"><?php echo $status; ?></option>
								<?php $sql_fetch_status = mysql_query("SELECT * FROM `$gd`.`project_status_dropdown` where project_status != '$status'  ", $connect_db); ?>                        	
	                            <?php while($row_fetch_status = mysql_fetch_array($sql_fetch_status)){ ?>
	                            <option value="<?php echo  $project_status = $row_fetch_status['project_status']; ?>" >
	                            <?php echo $project_status; ?> 
	                            </option>
	                            <?php } ?>

	                    </select>

					</div>

				</div>
			</div>




			<div class="form-group">
				<div class="row">

					<div class="col-sm-6"> 
						<label for="customer_want_date" class="control-label"> Comments </label>
						<input name="comments"  class="form-control edit_comments" value="<?php echo $comments; ?>">
					</div>

					<div class="col-sm-6"> 
						<label for="customer_want_date" class="control-label"> Customer Want Date </label>
						<input name="customer_want_date"  class="form-control mydatepicker edit_customer_want_date" autocomplete="off" value="<?php echo $customer_want_date; ?>">
					</div>


				</div>
			</div>	


			<div class="form-group">
				<div class="row">

					<div class="col-sm-6"> 
						<label class="control-label"> Actual Completion Date </label>
						<input name="completed_on"  class="form-control mydatepicker edit_completed_on" autocomplete="off" value="<?php echo $actual_completion_date; ?>">
					</div>




					<div class="col-sm-6"> 
						<label class="control-label"> Complexity Level </label>
						<select id="select2-demo-1" name="complexity_level" class="form-control edit_complexity_level" required>
							<option value="1"> 1 </option>
							<option value="2"> 2 </option>
							<option value="3"> 3 </option>
						</select>
					</div>

				</div>
			</div>	




			<div class="form-group">
				<div class="row">

					<div class="col-sm-6"> 
						<label class="control-label"> Hourly Charge </label>
						<input name="hourly_charge"  class="form-control edit_hourly_charge" autocomplete="off" value="<?php echo number_format($hourly_charge, 2); ?>">
					</div>

				</div>
			</div>	




			<input type="hidden" name="project_id" value="<?php echo $project_id; ?>">


		    <div class="modal-footer">
		    	<button type="submit" class="btn btn-primary" onclick="confirm_edit_project_details();">Submit</button>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>

	</div> <!-- modal body -->




<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>



<script type="text/javascript">
$('.mydatepicker').datepicker({
   	todayHighlight : true,
   	autoclose: true,
	format: 'dd/mm/yyyy'

});	
</script>



<script type="text/javascript" src="vendor/select2/dist/js/select2.min.js"></script>

<script type="text/javascript">
	$('[data-plugin="select2"]').select2($(this).attr('data-options'));
</script>




<script src="//cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.2/multiple-select.min.js"></script>

<script>
  // Here the mounted function is same as $(function() {})
$(document).ready(function(){
    $('#change_assign_to').multipleSelect({
    	filter: true,
		minimumCountSelected: 1
    })
});
</script>


