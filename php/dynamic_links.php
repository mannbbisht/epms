<?php 
// SESSION VARIABLES
session_start();
$session_employee_id = $_SESSION['Pk_Employee_Id'];
$session_full_name = $_SESSION['FullName'];
$session_email_id = $_SESSION['Email'];

$filename =  array(
                0 => 'vendor/bootstrap4/css/bootstrap.min.css',
                1 => 'vendor/themify-icons/themify-icons.css',
                2 => 'vendor/font-awesome/css/font-awesome.min.css',
		        3 => 'vendor/animate.css/animate.min.css',
				4 => 'vendor/jscrollpane/jquery.jscrollpane.css',
				5 => 'vendor/waves/waves.min.css',
				6 => 'vendor/switchery/dist/switchery.min.css',
				7 => 'vendor/morris/morris.css',
				8 => 'vendor/jvectormap/jquery-jvectormap-2.0.3.css',
				9 => 'css/core.css',

				/* js file */
				10 => 'vendor/tether/js/tether.min.js',
				11 => 'vendor/bootstrap4/js/bootstrap.min.js',
				12 => 'vendor/detectmobilebrowser/detectmobilebrowser.js',
				13 => 'vendor/jscrollpane/jquery.mousewheel.js',
				14 => 'vendor/jscrollpane/mwheelIntent.js',
				15 => 'vendor/jscrollpane/jquery.jscrollpane.min.js',
				16 => 'vendor/jquery-fullscreen-plugin/jquery.fullscreen-min.js',
				17 => 'vendor/waves/waves.min.js',
				18 => 'vendor/switchery/dist/switchery.min.js',
				19 => 'vendor/flot/jquery.flot.min.js',
				20 => 'vendor/flot/jquery.flot.resize.min.js',
				21 => 'vendor/flot.tooltip/js/jquery.flot.tooltip.min.js',
				22 => 'vendor/CurvedLines/curvedLines.js',
				23 => 'vendor/TinyColor/tinycolor.js',
				24 => 'vendor/sparkline/jquery.sparkline.min.js',
				25 => 'vendor/raphael/raphael.min.js',
				26 => 'vendor/morris/morris.min.js',
				27 => 'vendor/jvectormap/jquery-jvectormap-2.0.3.min.js',
				28 => 'vendor/jvectormap/jquery-jvectormap-world-mill.js',
				29 => 'vendor/peity/jquery.peity.js',





 				/* php files */

 				30 => 'php/insert.php',
 				31 => 'php/update.php',
 				32 => 'php/delete.php',
 				33 => 'add-project.php',
 				34 => 'add-activity.php',
 				35 => 'logout.php',
 				36 => 'activity-report.php',
 				37 => 'img/spinner.gif',
 				38 => 'proposed-vs-actual.php',
 				39 => 'type-of-work.php',
 				40 => 'cfr-vs-site.php',
 				41 => 'projects.php',
 				42 => 'report-out.php',
 				43 => 'project-status.php',
 				44 => 'project-consolidated.php',
 				45 => 'activity-log.php',
 				46 => 'dashboard.php',
 				47 => 'project-tracking.php',
 				48 => 'add-fields.php',
 				49 => 'user-creation.php',
 				50 => 'status-vs-dates.php',
 				51 => 'assigned-projects.php',
 				52 => 'index.php',
 				53 => 'project-status.php',
 				54 => 'project-type-report.php',
 				55 => 'resource-utilization-report.php',
 				56 => 'region-loading-report.php',
 				57 => 'offsite-charges.php',
 				58 => 'onsite-cross-charges.php',
 				59 => 'invoice-summary.php',
				60 => 'archived-projects.php',
 				61 => 'upload_holidays.php'
 

);




     // ||  (strpos($file,'js') == true)  || strpos($file,'css') == true) 


foreach ($filename as $file)
{


	if (file_exists($file)) {
		$file =  $file;
	}elseif (file_exists('../'.$file)) {
		$file = '../'.$file;
	}elseif (file_exists('../../'.$file)) {
		$file = '../../'.$file;
	}elseif (file_exists('../../../'.$file)) {
		$file = '../../../'.$file;
	}
	
	$file_path[] = $file;
	
	       
}





// GET ALL USER PERMISSIONS

/*

$sql_getInfo_UserPermissions = mysql_query("select * from `$xb_accounts`.`members_permissions`   where Fk_Member_Id = '$Pk_Members_Id' " , $connect_accounts);
while ($row_getInfo_UserPermissions = (mysql_fetch_array($sql_getInfo_UserPermissions)) ){
	extract($row_getInfo_UserPermissions);
}
*/
?>