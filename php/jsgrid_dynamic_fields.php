<?php

date_default_timezone_set('Asia/Kolkata');
$india_time = date('l jS \of F Y h:i:s A');  // WILL GIVE TIME IN THIS FORMAT Sunday 8th of September 2013 09:56:31 PM 


$year_to_date =  date("Y/m/d");
$date_to_year =  date("d/m/Y");
$month_date_year =  date("m/d/Y");

$this_month = date("m", strtotime($month_date_year));



$connect = new PDO("mysql:host=localhost;dbname=gardner_denver", "root", "");

	
session_start();
$session_employee_id = $_SESSION['Pk_Employee_Id'];



$query_fetch_role = " SELECT * FROM  employees  where pk_employee_id = '$session_employee_id' ";
$pdo_statement = $connect->prepare($query_fetch_role);
$pdo_statement->execute();
$rows_employee = $pdo_statement->fetchAll();

foreach ($rows_employee as $value) {
	$role = $value['role'];
}


$method = $_SERVER['REQUEST_METHOD'];





	







if( $method == "PUT" ) { // if pk_employee_id_1 is set and method is put, then admin is updating

	parse_str(file_get_contents("php://input"), $_PUT);

	$type = $_PUT['FieldType'];

	if($type == "user-creation") {

		$data = array(
			'id'   => $_PUT['PkEmployeeId'],
			'full_name'   => $_PUT['FullName'],
			'email_id' => $_PUT['EmailId'],
			'role' => $_PUT['Role']
		);

		$id = $data['id'];
		$full_name = $data['full_name'];
		$email_id = $data['email_id'];
		$role = $data['role'];


		$query = " UPDATE employees SET full_name = '$full_name', email_id = '$email_id', role = '$role' WHERE pk_employee_id = '$id' ";

		
	 	$statement = $connect->prepare($query);

	 	$statement->execute();

	}




	if($type == "location") {

		$data = array(
			'id'   => $_PUT['PkLocationId'],
			'country'   => $_PUT['Country']
		);

		$id = $data['id'];
		$country = $data['country'];

		$query = " UPDATE location_dropdown SET country = '$country' WHERE pk_location_id = '$id' ";

		
	 	$statement = $connect->prepare($query);

	 	$statement->execute();

	}




	if($type == "project-type") {

		$data = array(
			'id'   => $_PUT['PkDropdownId'],
			'project_type'   => $_PUT['ProjectType']
		);

		$id = $data['id'];
		$project_type = $data['project_type'];

		$query = " UPDATE project_type_dropdown SET project_type = '$project_type' WHERE pk_dropdown_id = '$id' ";

		
	 	$statement = $connect->prepare($query);

	 	$statement->execute();

	}





	if($type == "project-status") {

		$data = array(
			'id'   => $_PUT['PkStatusId'],
			'status'   => $_PUT['ProjectStatus']
		);

		$id = $data['id'];
		$status = $data['status'];

		$query = " UPDATE project_status_dropdown SET project_status = '$status' WHERE pk_status_id = '$id' ";

		
	 	$statement = $connect->prepare($query);

	 	$statement->execute();

	}	






	if($type == "project-division") {

		$data = array(
			'id'   => $_PUT['PkDivisionId'],
			'project_division'   => $_PUT['ProjectDivision']
		);

		$id = $data['id'];
		$project_division = $data['project_division'];

		$query = " UPDATE project_division_dropdown SET project_division = '$project_division' WHERE pk_division_id = '$id' ";

		
	 	$statement = $connect->prepare($query);

	 	$statement->execute();

	}	

} 




if( $method == "POST") { // // if full_name_1 is set and method is post, then admin is updating


	parse_str(file_get_contents("php://input"), $_POST);

	$new_email_id =	$_POST['EmailId']; // employees table
	$new_country = $_POST['Country']; // location_dropdown table 
	$new_project_type = $_POST['ProjectType']; // project_type_dropdown table 
	$new_project_status = $_POST['ProjectStatus']; // project_type_dropdown table 
	$new_project_division = $_POST['ProjectDivision']; // project_type_dropdown table 

	if(!empty($new_email_id)) {

		$data = array(
			'full_name'   => $_POST['FullName'],
			'email_id' => $_POST['EmailId'],
			'role' => $_POST['Role']
		);

		$full_name = ucwords($data['full_name']);
		$email_id = $data['email_id'];
		$role = $data['role'];


		$query = "INSERT INTO employees (full_name,email_id,password,role,added_on,simple_date,added_by) VALUES ('$full_name','$email_id','gardner@365','$role','$india_time','$date_to_year','$session_employee_id')";

		

	} else if(!empty($new_country)) {

		$query = "INSERT INTO location_dropdown (country,added_on,added_by) VALUES ('$new_country','$india_time','$session_employee_id')";


	} else if(!empty($new_project_type)) {

		$query = "INSERT INTO project_type_dropdown (project_type,added_on,added_by) VALUES ('$new_project_type','$india_time','$session_employee_id')";


	} else if(!empty($new_project_status)) {

		$query = "INSERT INTO project_status_dropdown (project_status,added_on,added_by) VALUES ('$new_project_status','$india_time','$session_employee_id')";

	} else if(!empty($new_project_division)) {

		$query = "INSERT INTO project_division_dropdown (project_division,added_on,added_by) VALUES ('$new_project_division','$india_time','$session_employee_id')";

	}

		
	$statement = $connect->prepare($query);

	$statement->execute();






}










/************************************************************* DELETE DATA : START *************************************************************/ 

if($method == "DELETE") {

	parse_str(file_get_contents("php://input"), $_DELETE);


	if(!empty($_DELETE['PkEmployeeId'])){
 	
	 	$query = "DELETE FROM employees WHERE pk_employee_id = '".$_DELETE["PkEmployeeId"]."'";

	} else if(!empty($_DELETE['PkLocationId'])) {

	 	$query = "DELETE FROM location_dropdown WHERE pk_location_id = '".$_DELETE["PkLocationId"]."'";

	} else if(!empty($_DELETE['PkDropdownId'])) {

	 	$query = "DELETE FROM project_type_dropdown WHERE pk_dropdown_id = '".$_DELETE["PkDropdownId"]."'";

	} else if(!empty($_DELETE['PkStatusId'])) {

	 	$query = "DELETE FROM project_status_dropdown WHERE pk_status_id = '".$_DELETE["PkStatusId"]."'";

	} else if(!empty($_DELETE['PkDivisionId'])) {

	 	$query = "DELETE FROM project_division_dropdown WHERE pk_division_id = '".$_DELETE["PkDivisionId"]."'";

	}


 	$statement = $connect->prepare($query);

	$statement->execute();

}

/************************************************************* DELETE DATA : END *************************************************************/ 


	
?>
	

