
				<!-- Footer -->
				<footer class="footer">
					<div class="container-fluid">
						<div class="row text-xs-center">
							<div class="col-sm-4 text-sm-left mb-0-5 mb-sm-0">
								<?php echo date("Y"); ?> © <?php echo $company_name; ?>
							</div>
<!-- 							<div class="col-sm-8 text-sm-right">
								<ul class="nav nav-inline l-h-2">
									<li class="nav-item"><a class="nav-link text-black" href="#">Privacy</a></li>
									<li class="nav-item"><a class="nav-link text-black" href="#">Terms</a></li>
									<li class="nav-item"><a class="nav-link text-black" href="#">Help</a></li>
								</ul>
							</div> -->
						</div>
					</div>
				</footer>
			</div>
		</div>

	</body>
</html>


<!-- Vendor JS -->

<!-- <script type="text/javascript" src="jquery/jquery.min.js"></script> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script type="text/javascript" src="vendor/tether/js/tether.min.js"></script>
<script type="text/javascript" src="vendor/bootstrap4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="vendor/detectmobilebrowser/detectmobilebrowser.js"></script>
<!-- <script type="text/javascript" src="vendor/jscrollpane/jquery.mousewheel.js"></script> -->
<script type="text/javascript" src="vendor/jscrollpane/mwheelIntent.js"></script>
<script type="text/javascript" src="vendor/jscrollpane/jquery.jscrollpane.min.js"></script>
<!-- <script type="text/javascript" src="vendor/jquery-fullscreen-plugin/jquery.fullscreen-min.js"></script> -->
<script type="text/javascript" src="vendor/waves/waves.min.js"></script>
<script type="text/javascript" src="vendor/switchery/dist/switchery.min.js"></script>
<!-- 		<script type="text/javascript" src="vendor/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="vendor/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="vendor/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script type="text/javascript" src="vendor/CurvedLines/curvedLines.js"></script> -->
<script type="text/javascript" src="vendor/TinyColor/tinycolor.js"></script>
<script type="text/javascript" src="vendor/sparkline/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="vendor/raphael/raphael.min.js"></script>
<script type="text/javascript" src="vendor/morris/morris.min.js"></script>
<!-- 		<script type="text/javascript" src="vendor/jvectormap/jquery-jvectormap-2.0.3.min.js"></script>
<script type="text/javascript" src="vendor/jvectormap/jquery-jvectormap-world-mill.js"></script>
 --><script type="text/javascript" src="vendor/peity/jquery.peity.js"></script>

<!-- Neptune JS -->
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="js/demo.js"></script>
<!-- <script type="text/javascript" src="js/index.js"></script> -->

<?php if ((strpos($complete_url, '/add-activity.php') == true)) { ?>

<!-- <script type="text/javascript" src="js/forms-plugins.js"></script> -->
<?php } ?>


<?php 

if ((strpos($complete_url, '/projects.php')) || (strpos($complete_url, '/dashboard.php')) || (strpos($complete_url, '/status-vs-dates.php')) || (strpos($complete_url, '/resource-utilization.php')) || (strpos($complete_url, '/offsite-charges.php'))  || (strpos($complete_url, '/onsite-cross-charges.php')) || (strpos($complete_url, '/upload_holidays.php')) || (strpos($complete_url, '/invoice-summary.php'))  == true) { 

?>

<script type="text/javascript" src="vendor/DataTables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Responsive/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Responsive/js/responsive.bootstrap4.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.bootstrap4.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/JSZip/jszip.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/pdfmake/build/pdfmake.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/pdfmake/build/vfs_fonts.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.print.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.colVis.min.js"></script>


<!-- <script type="text/javascript" src="js/tables-datatable.js"></script> -->
	<?php

		if ( (strpos($complete_url, '/resource-utilization.php') ) || (strpos($complete_url, '/upload_holidays.php')) || (strpos($complete_url, '/offsite-charges.php') )  || (strpos($complete_url, '/invoice-summary.php')   )  == true) { 

	?>
	<script type="text/javascript">
	    $('#table-2').DataTable( {
	        dom: 'Bfrtip',

	        "scrollY": "5000px",
	        "scrollCollapse": true,
		    paging: false,
            'autoWidth':true,
   "sScrollX": "100%",
   "scrollX": true,
	        "aaSorting": [],



			buttons: 
			[
			  {
			    extend: 'excel',
			    // footer: true,
			      exportOptions: {
					columns: "thead th:not(.noExport)"
			      }
			  },
			  {
			    extend: '',
			    footer: false
			  },
			  {
			    extend: '',
			    footer: false
			  }         
			]  

	    } );

		// var table = $('#table-2').DataTable();
		// table.order.listener( '#sorter', 15 );

		// table.order([15, "asc"]).draw();


	</script>


	<?php } else if ((strpos($complete_url, '/onsite-cross-charges.php')) == true) {  ?>



	<script type="text/javascript">

	    $('#table-2').DataTable( {
	        dom: 'Bfrtip',
	        sorting: false,
	        "searching": false,
	        "scrollX": "2000px",
	        "scrollY": "2000px",
	        "scrollCollapse": true,
		    paging: false,


			buttons: 
			[
			  {
			    extend: 'excel',
			    exportOptions: {
					columns: "thead th:not(.noExport)"
			    }

			  },
			  {
			    extend: '',
			    footer: false
			  },
			  {
			    extend: '',
			    footer: false
			  }         
			]  

	    } );

	</script>


	<?php } else { ?>


	<script type="text/javascript">
	    $('#table-2').DataTable( {
	        dom: 'Bfrtip',
	 		// pageLength: 50

	    } );
	</script>

	<?php } ?>

<?php } ?>





<script type="text/javascript">
function scrollToTop(){
	$('html, body').animate({scrollTop:0}, 'slow');
}	
</script>




<?php


	$geouserip = getenv('REMOTE_ADDR');
	$geodatainformation = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$geouserip"));

	$continent_code = $geodatainformation["geoplugin_continentCode"];
	$country_name = $geodatainformation["geoplugin_countryName"];
	$country_code = $geodatainformation["geoplugin_countryCode"];
	$city = $geodatainformation["geoplugin_city"];
	$region_code = $geodatainformation["geoplugin_regionCode"];
	$region_name = $geodatainformation["geoplugin_regionName"];
	$latitude = $geodatainformation["geoplugin_latitude"];
	$longitude = $geodatainformation["geoplugin_longitude"];
	$currency_code = $geodatainformation["geoplugin_currencyCode"];

	$location = "$country_name $city";



	$sql_insert_visitors_info = "INSERT into `$gd`.`visitors` (fk_member_id,ipaddress,dateandtime,browser,pageviewed,location)   VALUES ('$session_employee_id','$geouserip','$india_time','$browser','$complete_url','$location') ";  
	mysql_query($sql_insert_visitors_info, $connect_db) or die("Couldn't select gd database");


?>


<script type="text/javascript">
// same link to home href 
$(document).ready(function(){
	$('.breadcrumb-item a').attr('href', '<?php echo $domain; ?>');	
	$(".no_class").removeClass('active');

	// $(".dt-buttons a:first").html('Download as Excel');
});
</script>



<script type="text/javascript">
// notification read function
$('.notification_btn').click(function() {
	$.post('<?php echo $file_path[31]; ?>', '&mark_notification_as_read=mark_notification_as_read');
});

</script>

<script type="text/javascript">
function RefreshPage(){
	location.reload();
}
</script>

<script type="text/javascript">
$(".DownloadReport").click(function(e) {

	e.preventDefault();
	window.open('phpexcel/Examples/export.php?file=status-vs-dates');	

});

$(".SampleReport").click(function(e) {

	e.preventDefault();
	window.open('phpexcel/Examples/export.php?file=public-holidays');	

});

</script>