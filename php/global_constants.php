<?php 
$common_php_functions = 'php/common_php_functions.php';
$dbc = 'php/database_connection.php';


if (file_exists($common_php_functions)) {
	$common_php_functions =  $common_php_functions;
}elseif (file_exists('../'.$common_php_functions)) {
	$common_php_functions = '../'.$common_php_functions;
}elseif (file_exists('../../'.$common_php_functions)) {
	$common_php_functions = '../../'.$common_php_functions;
}elseif (file_exists('../../../'.$common_php_functions)) {
	$common_php_functions = '../../../'.$common_php_functions;
}



if (file_exists($dbc)) {
	$dbc =  $dbc;
}elseif (file_exists('../'.$dbc)) {
	$dbc = '../'.$dbc;
}elseif (file_exists('../../'.$dbc)) {
	$dbc = '../../'.$dbc;
}elseif (file_exists('../../../'.$dbc)) {
	$dbc = '../../../'.$dbc;
}

 


include $dbc;
include $common_php_functions;







define ("company_name", "Ingersoll Rand");
$company_name = company_name;
 









$header = "From:manjitsingh.bisht@gardnerdenver.com \r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";



//DYNAMIC LINKS


$dynamic_links = "php/dynamic_links.php";
if (file_exists($dynamic_links)) { $dynamic_links =  $dynamic_links; }elseif (file_exists('../'.$dynamic_links)) { $dynamic_links = '../'.$dynamic_links; }elseif (file_exists('../../'.$dynamic_links)) { $dynamic_links = '../../'.$dynamic_links; }



/* *************************  FRONTEND  ****************************** */





/* *************************  BACKEND  ****************************** */


//HEADER FILE
$backend_header_file = "php/backend_header.php";
if (file_exists($backend_header_file)) { $backend_header_file =  $backend_header_file; }elseif (file_exists('../'.$backend_header_file)) { $backend_header_file = '../'.$backend_header_file; }elseif (file_exists('../../'.$backend_header_file)) { $backend_header_file = '../../'.$backend_header_file; }



//FOOTER Custom File
$backend_footer_file = "php/backend_footer.php";
if (file_exists($backend_footer_file)) { $backend_footer_file =  $backend_footer_file; }elseif (file_exists('../'.$backend_footer_file)) { $backend_footer_file = '../'.$backend_footer_file; }elseif (file_exists('../../'.$backend_footer_file)) { $backend_footer_file = '../../'.$backend_footer_file; }


//LEFT HAND MENU LINKS
$backend_sidebar = "php/backend_sidebar.php";
if (file_exists($backend_sidebar)) { $backend_sidebar =  $backend_sidebar; } elseif (file_exists('../'.$backend_sidebar)) { $backend_sidebar = '../'.$backend_sidebar; }elseif (file_exists('../../'.$backend_sidebar)) { $backend_sidebar = '../../'.$backend_sidebar; }




//BREADCRUMBS LINKS
$backend_breadcrumbs = "php/backend/backend_breadcrumbs.php";
if (file_exists($backend_breadcrumbs)) { $backend_breadcrumbs =  $backend_breadcrumbs; }elseif (file_exists('../'.$backend_breadcrumbs)) { $backend_breadcrumbs = '../'.$backend_breadcrumbs; }elseif (file_exists('../../'.$backend_breadcrumbs)) { $backend_breadcrumbs = '../../'.$backend_breadcrumbs; }



// BUTTON NAMES

define ("save_button", "Save");
define ("search_button", "Search");
define ("close_button", "Close");
define ("reset_button", "Reset");
define ("login_button", "Login");
define ("register_button", "Register");
define ("back_button", "Back");
define ("create_button", "Create");
define ("delete_button", "Delete");
define ("add_button", "Add");
define ("sent_button", "Sent");
define ("edit_button", "Edit");
define ("read_only_button", "Readonly");


define ("enabledPerms_view_button", "Edit");
define ("disabledPerms_view_button", "Read Only");


// NO IMAGE AVAILABLE FOR PRODUCT - IMAGE PATH 
define ("noImageForProduct", "image/products/no_image.png");



// Loading Data
define ("loading_data", "Loading Data...");




// drop down list styling
define ("dropDownList_styling", "background-repeat:no-repeat;padding-left:6%;");


$scrollToTop = "<a href='#' style='padding-top:3px;margin-bottom:55px;' class='totop animation' data-toggle='waypoints totop' data-showanim='bounceIn' data-hideanim='bounceOut' data-offset='50%'><i class='fa fa-angle-double-up fa-2x'></i></a>";

$scollshoppingBasket = "<a href='#' style='float:left;margin-right:45px;'  class='totop animation' data-toggle='waypoints totop' data-showanim='bounceIn' data-hideanim='bounceOut' data-offset='70%'><i class='fa fa-shopping-cart fa-2x'></i></a>";


// NO RECORDS FOUND

define ("noRecordsFound", "<div style='padding:2% 2% 2% 0%;color:#f86741;font-size:18px;'> No Records Found. </div>");

define ("noImageUploaded", "<div style='padding:2% 2% 2% 0%;color:#f86741;font-size:18px;'> No Image Uploaded. </div>");

define ("noMessagesFound", "<div style='padding:5% 2% 2% 1%;color:#f86741;font-size:18px;'> You do not have any messages. </div>");

define ("noOrdersPlacedYet", "<div style='padding:5% 2% 2% 1%;color:#f86741;font-size:18px;'> You have not placed any new order yet. </div>");

define ("noOrdersCancelledYet", "<div style='padding:5% 2% 2% 1%;color:#f86741;font-size:18px;'> There are no cancelled Orders. </div>");

define ("noRecordsFoundStockSystem", "<div class='row' style='margin-top:30px;'> <div class='container'> <div class='col-md-4'></div> <div class='col-md-4'> <div class='alert alert-dismissable alert-danger' > No Records Found. </div> </div>  <div class='col-md-4'></div> </div> </div>");


 

// BOOTSTRAP table ATTRIBUTES


//define ("bt_table_attributes", 'data-height="499"  data-pagination="true"  data-toggle="table" minimumCountColumns="1"  data-show-refresh="true"   data-show-columns="true" data-search="true"  data-show-export="true"  data-striped="true" data-mobile-responsive="true"  style="overflow-x:hidden;"     ');
 

define ("bt_table_attributes", '   data-toggle="table" minimumCountColumns="1"  data-show-refresh="true"   data-show-columns="true"   data-search="true"  data-show-export="true"     data-striped="true" data-mobile-responsive="true"  data-height="700"     style="overflow-x:hidden;"      ');
 


// SAVING DATA

define ("saving_data", "Saving Data...");
define ("processing_request", "Processing request...");


$domain = "http://192.168.43.153:8080";

?>