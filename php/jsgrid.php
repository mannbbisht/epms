<?php

date_default_timezone_set('Asia/Kolkata');
$india_time = date('l jS \of F Y h:i:s A');  // WILL GIVE TIME IN THIS FORMAT Sunday 8th of September 2013 09:56:31 PM 


$year_to_date =  date("Y/m/d");
$date_to_year =  date("d/m/Y");
$month_date_year =  date("m/d/Y");

$this_month = date("m", strtotime($month_date_year));



$connect = new PDO("mysql:host=localhost;dbname=gardner_denver", "root", "");

	
session_start();
$session_employee_id = $_SESSION['Pk_Employee_Id'];



$query_fetch_role = " SELECT * FROM  employees  where pk_employee_id = '$session_employee_id' ";
$pdo_statement = $connect->prepare($query_fetch_role);
$pdo_statement->execute();
$rows_employee = $pdo_statement->fetchAll();

foreach ($rows_employee as $value) {
	$role = $value['role'];
}


$method = $_SERVER['REQUEST_METHOD'];



/************************************************************* SELECT ALL RECORDS : START *************************************************************/ 

















/************************************************************* INSERT NEW DATA : START *************************************************************/ 


if($method == "POST") {

	
	$data = array(

		'record_date'  => $_POST['RecordDate'],
		'order_number'  => $_POST['OrderNumber'],
		'project_name'  => $_POST['ProjectName'],
		'location'  => $_POST['Location'],
		'working_hours' => $_POST['WorkingHours'],
		'activity_carried_out' => $_POST['ActivityCarriedOut'],
		'design_engineer' => $_POST['DesignEngineer']

	);

	$record_date = $data['record_date'];
	$order_number = $data['order_number'];
	$project_name = $data['project_name'];
	$location = $data['location'];
	$working_hours = $data['working_hours'];
	$activity_carried_out = $data['activity_carried_out'];
	$design_engineer = $data['design_engineer'];
	

	/********************* GET PROJECT INFO *********************/

	$query_fetch_projects = " SELECT * FROM projects where order_number = '$order_number' ";
	$statement_1 = $connect->prepare($query_fetch_projects);
	$statement_1->execute($data);
	$rows_fetch_projects = $statement_1->fetchAll();

	$project_name = $rows_fetch_projects[0]['project_name'];
	$location = $rows_fetch_projects[0]['location'];
	$project_type = $rows_fetch_projects[0]['project_type'];

	$project_name = mysql_real_escape_string($project_name);
	$activity_carried_out = mysql_real_escape_string($activity_carried_out);



	/********************* GET EMPLOYEE INFO *********************/

	$query_fetch_emp_id = " SELECT * FROM employees where full_name = '$design_engineer' ";
	$statement_2 = $connect->prepare($query_fetch_emp_id);
	$statement_2->execute($data);
	$rows_fetch_emp_id = $statement_2->fetchAll();

	$design_engineer_id = $rows_fetch_emp_id[0]['pk_employee_id'];
    $date_acronym = substr($record_date, 0, 2); 
    $month_acronym = substr($record_date, 3, 2); 
    $year_acronym = substr($record_date, 6, 4); 


    $date_year = "$month_acronym-$year_acronym";


	
	$insert_new_activity = "INSERT INTO activity (fk_employee_id,record_date,order_number,project_title,project_type,country,activity_carried_out,working_hours,date_acronym,month_acronym,date_year,added_on,simple_date) VALUES ('$design_engineer_id','$record_date','$order_number','$project_name','$project_type','$location','$activity_carried_out','$working_hours','$date_acronym','$month_acronym','$date_year','$india_time','$month_date_year')";

	$statement_2 = $connect->prepare($insert_new_activity);

	
	$statement_2->execute($data);

	
}


/************************************************************* INSERT NEW DATA : END *************************************************************/ 

























/************************************************************* UPDATE DATA : START *************************************************************/ 


if($method == 'PUT') {

	parse_str(file_get_contents("php://input"), $_PUT);

	$data = array(
		'id'   => $_PUT['PkActivityId'],
		'record_date'   => $_PUT['RecordDate'],
		'order_n' => $_PUT['OrderNumber'],
		'location' => $_PUT['Location'],
		'project_name'   => $_PUT['ProjectName'],
		'working_hours'   => $_PUT['WorkingHours'],
		'design_engineer'   => $_PUT['DesignEngineer'],
		'activity_carried_out'  => $_PUT['ActivityCarriedOut']
	);

	$id = $data['id'];
	$record_date = $data['record_date'];
	$order_number = $data['order_n'];
	$selected_location = $data['location'];
	$project_name = $data['project_name'];
	$working_hours = $data['working_hours'];
	$design_engineer = $data['design_engineer'];
	$activity_carried_out = $data['activity_carried_out'];



	$query_1 = " SELECT * FROM projects where order_number = '$order_number' ";
	$statement_1 = $connect->prepare($query_1);
	$statement_1->execute($data);
	$result_1 = $statement_1->fetchAll();


	if(!empty($design_engineer)) {
		$query_2 = " SELECT * FROM employees where full_name = '$design_engineer' ";
	} else {
		$query_2 = " SELECT * FROM employees where pk_employee_id = '$session_employee_id' ";
	}
	$statement_2 = $connect->prepare($query_2);
	$statement_2->execute($data);
	$result_2 = $statement_2->fetchAll();


	$new_project_name = $result_1[0]['project_name'];
	$new_location = $result_1[0]['location'];
	$new_project_type = $result_1[0]['project_type'];

	$new_project_name = mysql_real_escape_string($new_project_name);


	// $activity_carried_out = mysql_real_escape_string($activity_carried_out);
	// $activity_carried_out = htmlspecialchars_decode($activity_carried_out);


	$pk_id = $result_2[0]['pk_employee_id'];

    $month_acronym = substr($record_date, 3, 2); 
    $date_acronym = substr($record_date, 0, 2); 
    $year_acronym = substr($record_date, 6, 4); 


    $date_year = "$month_acronym-$year_acronym";

    if($order_number == "Training") {
    	if(empty($selected_location)) {
	    	$new_location = "India";
    	} else {
	    	$new_location = "$selected_location";
    	}
    }

	$query = " UPDATE activity SET order_number = '$order_number', fk_employee_id = '$pk_id',  project_title = '$new_project_name', project_type = '$new_project_type' , country = '$new_location', record_date = '$record_date', working_hours = '$working_hours', activity_carried_out = '$activity_carried_out', date_acronym = '$date_acronym', month_acronym = '$month_acronym', date_year = '$date_year', updated_on = '$india_time' WHERE pk_activity_id = '$id' ";

	
 	$statement = $connect->prepare($query);

 	$statement->execute($data);

	
}

/************************************************************* UPDATE DATA : END *************************************************************/ 
	

	


















/************************************************************* DELETE DATA : START *************************************************************/ 

if($method == "DELETE") {

	parse_str(file_get_contents("php://input"), $_DELETE);

 	$query = "DELETE FROM activity WHERE pk_activity_id = '".$_DELETE["PkActivityId"]."'";

 	$statement = $connect->prepare($query);

	$statement->execute();

}

/************************************************************* DELETE DATA : END *************************************************************/ 

	

	
?>