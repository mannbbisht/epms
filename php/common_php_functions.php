<?php 
// DATE & TIME FUNCTIONS

// IF YOU ARE IN LONDON : date_default_timezone_set('Europe/London');
// IF YOU ARE IN INDIA : date_default_timezone_set('Asia/Kolkata'); 
// FOR A LIST OF TIMEZONES VISIT : http://www.php.net/manual/en/timezones.php

date_default_timezone_set('Asia/Kolkata');
$india_time = date('l jS \of F Y h:i:s A');  // WILL GIVE TIME IN THIS FORMAT Sunday 8th of September 2013 09:56:31 PM 



// Y will display year in four digit 

$simple_date =  date("Y/m/d");

$year_to_date =  date("Y/m/d");
$date_to_year =  date("d/m/Y");

$simple_date_yearfirst = date("Y-m-d");  //yyyy-mm-dd//
$only_year =  date("Y");

date_default_timezone_set('Asia/Kolkata');
$date_time = date("m/d/Y H:i");


date('d/m/Y',strtotime("-1 days"));
$day_name =  date('l'); // dayname


$this_month = date("m-Y", strtotime($simple_date));

//get future date
$date = new DateTime();
$date->modify("+10 day");
$ten_days_later =  $date->format("Y-m-d");


$yesterday_date =  date('d/m/Y',strtotime("-1 days"));


$month = date("F");



//$user_ip = $_SERVER['REMOTE_ADDR'];

$user_ip = $_SERVER['REMOTE_ADDR'];



//$browser = $_SERVER['HTTP_USER_AGENT'];

$browser = $_SERVER['HTTP_USER_AGENT'];


// Automatically collects the hostname or domain  like example.com)
$host  = $_SERVER['HTTP_HOST'];
$host_upper = strtoupper($host);
$path   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');



// account number 
$digits = 7;
$account_number =  rand(pow(10, $digits-1), pow(10, $digits)-1);


// Generates a random code simple 4 digit number : syntax mt_rand(min,max);
$four_digit_random_number = mt_rand(1000,9999);
$seven_digit_random_number = mt_rand(1000000, 9999999);







function print_value($val) {
    if(!empty($val)) {
        echo $val;
    } else {
        echo "-";
    }
}


function allMonths(){

	$current_month = date("m");
	// $current_month += 1;

	for ($i=1; $i <= $current_month; $i++) { 

		$custom_date = "22-".$i."-".date("Y");
		$get_month_from_date = strtotime($custom_date);
		$month = date("F", $get_month_from_date);

	?>
		<option value="<?php echo $i; ?>" selected> <?php echo $month . ' ' . date("Y"); ?> </option>
	<?php } 
}



function allMonths_Unchecked(){

	$current_month = date("m");
	// $current_month += 1;

	for ($i=1; $i <= $current_month; $i++) { 

		$custom_date = "22-".$i."-".date("Y");
		$get_month_from_date = strtotime($custom_date);
		$month = date("F", $get_month_from_date);

	?>
		<option value="<?php echo $i; ?>" > <?php echo $month . ' ' . date("Y"); ?> </option>
	<?php } 
	
}




$hourly_charges = array(
	'Analysis' => 16.39, 
	'Programming' => 15.17, 
	'Design' => 10.68, 
	'SAP/Documentation' => 10.68, 
	'Modeling / Drawing' => 10.68, 
	'i2V' => 10.68
);





function AllMonths_YTD_Dropdown() {


	$months = array();

	for ($i=date("m", strtotime("now")); $i > 0; $i--) { 

		$month_format = "01-$i-".date("Y");

	?>

	<option value="<?php echo date("m-Y", strtotime("$month_format")); ?>"> <?php echo date("F Y", strtotime("$month_format")); ?> </option>

	<?php }


}




function Last12Months_Dropdown() {
	$months = array();

	/*
	for ($i=date("m", strtotime("now")); $i > 0; $i--) { 
		$month_format = "01-$i-".date("Y");
	?>
	*/

	for ($i=0; $i < 12; $i++) { 
		$month_format = date("d-m-Y", strtotime("-$i months"));
	?>


	<option value="<?php echo date("m-Y", strtotime("$month_format")); ?>"> <?php echo date("F Y", strtotime("$month_format")); ?> </option>

	<?php }
}



function GetLast12MonthsName(){

	$month_names = array();

	for ($i=0; $i <12 ; $i++) { 
		$month_names[] = date("F Y", strtotime("-$i month"));
	}

	// for ($i=date("m", strtotime("now")); $i >= 1; $i--) { 

	// 	$month_names[] = date("F Y", strtotime("01-$i-"."2019"));

	// }

	// for ($i=12; $i >= date("m", strtotime("+1 month")); $i--) { 

	// 	$month_names[] = date("F Y", strtotime("01-$i-"."2018"));

	// }

	return $month_names;

}





function GetLast12MonthsNumber(){

	$month_number = array();

	for ($i=0; $i <12 ; $i++) { 
		$month_number[] = date("m-Y", strtotime("-$i month"));
	}

	// for ($i=date("m", strtotime("now")); $i >= 1; $i--) { 

	// 	$month_number[] = date("m", strtotime("01-$i-"."2019"));

	// }

	// for ($i=12; $i >= date("m", strtotime("now")); $i--) { 

	// 	$month_number[] = date("m", strtotime("01-$i-"."2018"));

	// }

	return $month_number;

}




function AllMonthsName_YTD() {

	$months = array();

	for ($i=01; $i <= date("m", strtotime("now")); $i++) { 

		if($i<10) { $i = "0$i"; }

		$month_format = "01-$i-".date("Y");

		$months[] = date("F Y", strtotime("$month_format"));

	}

	return $months;

}




function AllMonthsNumber_YTD() {

	$months_number = array();

	for ($i=01; $i <= date("m", strtotime("now")); $i++) { 

		if($i<10) { $i = "0$i"; }

		$month_format = "01-$i-".date("Y");

		$months_number[] = date("m-Y", strtotime("$month_format"));

	}

	return $months_number;

}


function AllAdminUsers() {

	return ['Pournima Wakekar','Priyank Shah'];

}


/******** skip these dates for email *******/
// $skip_dates_for_email = ['26-01-'.date("Y"), '15-08-'.date("Y"), '02-09-'.date("Y"), '02-10-'.date("Y")];

$skip_dates_for_email = [

		// for every year
		'14-01-'.date("Y"),
		'15-01-'.date("Y"),
		'26-01-'.date("Y"), 
		'15-08-'.date("Y"), 
		'25-12-'.date("Y"),

		// for 2019

		'02-09-'.date("Y"),
		'08-10-'.date("Y"),
		'28-10-'.date("Y"),
		'29-10-'.date("Y"),
		'12-11-'.date("Y")

	];

 
// GET CURRENT PAGE 
$current_page = basename($_SERVER['SCRIPT_FILENAME']);
$complete_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";


$global_team_email = "esgglobal@gardnerdenver.com";
?>