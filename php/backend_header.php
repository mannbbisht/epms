<?php include $dynamic_links; ?>

<?php

$sql_fetch_user_details = mysql_query("SELECT * FROM `$gd`.`employees` where pk_employee_id = '$session_employee_id' ", $connect_db);
while ($row_fetch_user_details = mysql_fetch_array($sql_fetch_user_details)) {
	extract($row_fetch_user_details);

	$member_role = $role;

}
?>

<!DOCTYPE html>
<html lang="en">
	
<head>
		<!-- Meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Title -->
		<title>

		<?php if ((strpos($complete_url, 'index.php') == true)) { ?> Home <?php } ?>
		<?php if ((strpos($complete_url, $file_path[46]) == true)) { ?> Dashboard <?php } ?>
		<?php if ((strpos($complete_url, $file_path[33]) == true)) { ?> Create a Project <?php } ?>
		<?php if ((strpos($complete_url, $file_path[34]) == true)) { ?> Add your activity <?php } ?>
		<?php if ((strpos($complete_url, $file_path[36]) == true)) { ?> Activity Report <?php } ?>
		<?php if ((strpos($complete_url, '/projects.php') == true)) { ?> Projects <?php } ?>
		<?php if ((strpos($complete_url, $file_path[48]) == true)) { ?> Add Fields <?php } ?>
		<?php if ((strpos($complete_url, $file_path[45]) == true)) { ?> Activity Log <?php } ?>
		<?php if ((strpos($complete_url, $file_path[50]) == true)) { ?> Status VS Dates <?php } ?>
		<?php if ((strpos($complete_url, 'assigned-') == true)) { ?> Assigned Projects <?php } ?>
		<?php if ((strpos($complete_url, $file_path[52]) == true)) { ?> Resource Utilization <?php } ?>
		<?php if ((strpos($complete_url, $file_path[49]) == true)) { ?> User Creation <?php } ?>
		<?php if ((strpos($complete_url, $file_path[53]) == true)) { ?> Project Status Report <?php } ?>
		<?php if ((strpos($complete_url, 'resource-utilization.php') == true)) { ?> Resource Utilization <?php } ?>
		<?php if ((strpos($complete_url, 'reset-password.php') == true)) { ?> Reset Password <?php } ?>
		<?php if ((strpos($complete_url, 'notifications.php') == true)) { ?> View all Notifications <?php } ?>
		<?php if ((strpos($complete_url, $file_path[54]) == true)) { ?> Project Type Report <?php } ?>
		<?php if ((strpos($complete_url, $file_path[55]) == true)) { ?> ETC Resource Utilization <?php } ?>
		<?php if ((strpos($complete_url, $file_path[56]) == true)) { ?> Region Wise Loading <?php } ?>
		<?php if ((strpos($complete_url, $file_path[57]) == true)) { ?> Offsite Charges <?php } ?>
		<?php if ((strpos($complete_url, $file_path[58]) == true)) { ?> Onsite Cross Charges <?php } ?>
		<?php if ((strpos($complete_url, $file_path[59]) == true)) { ?> Invoice Summary <?php } ?>
		<?php if ((strpos($complete_url, $file_path[60]) == true)) { ?> Archived Projects <?php } ?>
		<?php if ((strpos($complete_url, $file_path[61]) == true)) { ?> Public Holidays <?php } ?>

		</title>

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo $file_path[0]; ?>">
		<link rel="stylesheet" href="<?php echo $file_path[1]; ?>">
		<!-- <link rel="stylesheet" href="<?php echo $file_path[2]; ?>"> -->
		<!-- font awesome cdn link -->

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

		<link rel="stylesheet" href="<?php echo $file_path[3]; ?>">
		<link rel="stylesheet" href="<?php echo $file_path[4]; ?>">
		<link rel="stylesheet" href="<?php echo $file_path[5]; ?>">
		<link rel="stylesheet" href="<?php echo $file_path[6]; ?>">
		<link rel="stylesheet" href="<?php echo $file_path[7]; ?>">
		<link rel="stylesheet" href="<?php echo $file_path[8]; ?>">


		<link rel="stylesheet" href="css/custom.css">

		<!-- Neptune CSS -->
		<link rel="stylesheet" href="<?php echo $file_path[9]; ?>">


		<style type="text/css">
			.navbar-brand:hover{
				cursor: default;
			}
			.esg_div_logo{
				height: 40px;
				margin: 10px;
			}
		</style>



		<?php if($member_role == "global") { ?>

		<style type="text/css">
		@media (min-width: 768px) {
			.site-header .navbar-left {
			    float: left;
			    width: 0px !important;
			}			
		}
		@media (min-width: 768px) {
			.site-content {
				margin-left: 0px !important; 
			}		
		}
		</style>

		<?php } ?>


		<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="fixed-sidebar fixed-header skin-default content-appear">
		<div class="wrapper">

			<!-- Preloader -->
			<!-- <div class="preloader"></div> -->


			<?php 

			if($member_role != "global") {

				include $backend_sidebar; 

			} ?>


			
			<!-- Sidebar second -->


<!-- 			<div class="site-sidebar-second custom-scroll custom-scroll-dark">
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#tab-1" role="tab">Chat</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#tab-2" role="tab">Activity</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#tab-3" role="tab">Todo</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#tab-4" role="tab">Settings</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab-1" role="tabpanel">
						<div class="sidebar-chat animated fadeIn">
							<div class="sidebar-group">
								<h6>Favorites</h6>
								<a class="text-black" href="#">
									<span class="sc-status bg-success"></span>
									<span class="sc-name">John Doe</span>
									<span class="sc-writing"> typing...<i class="ti-pencil"></i></span>
								</a>
								<a class="text-black" href="#">
									<span class="sc-status bg-success"></span>
									<span class="sc-name">Vance Osborn</span>
								</a>
								<a class="text-black" href="#">
									<span class="sc-status bg-danger"></span>
									<span class="sc-name">Wolfe Stevie</span>
									<span class="tag tag-primary">7</span>
								</a>
								<a class="text-black" href="#">
									<span class="sc-status bg-warning"></span>
									<span class="sc-name">Jonathan Mel</span>
								</a>
								<a class="text-black" href="#">
									<span class="sc-status bg-secondary"></span>
									<span class="sc-name">Carleton Josiah</span>
								</a>
							</div>
							<div class="sidebar-group">
								<h6>Work</h6>
								<a class="text-black" href="#">
									<span class="sc-status bg-secondary"></span>
									<span class="sc-name">Larry Hal</span>
								</a>
								<a class="text-black" href="#">
									<span class="sc-status bg-success"></span>
									<span class="sc-name">Ron Carran</span>
									<span class="sc-writing"> typing...<i class="ti-pencil"></i></span>
								</a>
							</div>
							<div class="sidebar-group">
								<h6>Social</h6>
								<a class="text-black" href="#">
									<span class="sc-status bg-success"></span>
									<span class="sc-name">Lucius Skyler</span>
									<span class="tag tag-primary">3</span>
								</a>
								<a class="text-black" href="#">
									<span class="sc-status bg-danger"></span>
									<span class="sc-name">Landon Graham</span>
								</a>
							</div>
						</div>
						<div class="sidebar-chat-window animated fadeIn">
							<div class="scw-header clearfix">
								<a class="text-grey float-xs-left" href="#"><i class="ti-angle-left"></i> Back</a>
								<div class="float-xs-right">
									<strong>Jonathan Mel</strong>
									<div class="avatar box-32">
										<img src="img/avatars/5.jpg" alt="">
										<span class="status bg-success top right"></span>
									</div>
								</div>
							</div>
							<div class="scw-item">
								<span>Hello!</span>
							</div>
							<div class="scw-item self">
								<span>Meeting at 16:00 in the conference room. Remember?</span>
							</div>
							<div class="scw-item">
								<span>No problem. Thank's for reminder!</span>
							</div>
							<div class="scw-item self">
								<span>Prepare to be amazed.</span>
							</div>
							<div class="scw-item">
								<span>Good. Can't wait!</span>
							</div>
							<div class="scw-form">
								<form>
									<input class="form-control" type="text" placeholder="Type...">
									<button class="btn btn-secondary" type="button"><i class="ti-angle-right"></i></button>
								</form>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tab-2" role="tabpanel">
						<div class="sidebar-activity animated fadeIn">
							<div class="notifications">
								<div class="n-item">
									<div class="media">
										<div class="media-left">
											<div class="avatar box-48">
												<img class="b-a-radius-circle" src="img/avatars/1.jpg" alt="">
												<span class="n-icon bg-danger"><i class="ti-pin-alt"></i></span>
											</div>
										</div>
										<div class="media-body">
											<div class="n-text"><a class="text-black" href="#">John Doe</a> <span class="text-muted">uploaded <a class="text-black" href="#">monthly report</a></div>
											<div class="text-muted font-90">12 minutes ago</div>
										</div>
									</div>
								</div>
								<div class="n-item">
									<div class="media">
										<div class="media-left">
											<div class="avatar box-48">
												<img class="b-a-radius-circle" src="img/avatars/3.jpg" alt="">
												<span class="n-icon bg-success"><i class="ti-comment"></i></span>
											</div>
										</div>
										<div class="media-body">
											<div class="n-text"><a class="text-black" href="#">Vance Osborn</a> <span class="text-muted">commented </span> <a class="text-black" href="#">Project</a></div>
											<div class="n-comment my-0-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</div>
											<div class="text-muted font-90">3 hours ago</div>
										</div>
									</div>
								</div>
								<div class="n-item">
									<div class="media">
										<div class="media-left">
											<div class="avatar box-48">
												<img class="b-a-radius-circle" src="img/avatars/2.jpg" alt="">
												<span class="n-icon bg-danger"><i class="ti-email"></i></span>
											</div>
										</div>
										<div class="media-body">
											<div class="n-text"><a class="text-black" href="#">Ron Carran</a> <span class="text-muted">send you files</span></div>
											<div class="row row-sm my-0-5">
												<div class="col-xs-4">
													<img class="img-fluid" src="img/photos-1/1.jpg" alt="">
												</div>
												<div class="col-xs-4">
													<img class="img-fluid" src="img/photos-1/2.jpg" alt="">
												</div>
												<div class="col-xs-4">
													<img class="img-fluid" src="img/photos-1/3.jpg" alt="">
												</div>
											</div>
											<div class="text-muted font-90">30 minutes ago</div>
										</div>
									</div>
								</div>
								<div class="n-item">
									<div class="media">
										<div class="media-left">
											<div class="avatar box-48">
												<img class="b-a-radius-circle" src="img/avatars/4.jpg" alt="">
												<span class="n-icon bg-primary"><i class="ti-plus"></i></span>
											</div>
										</div>
										<div class="media-body">
											<div class="n-text"><a class="text-black" href="#">Larry Hal</a> <span class="text-muted">invited you to </span><a class="text-black" href="#">Project</a></div>
											<div class="text-muted font-90">10:58</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tab-3" role="tabpanel">
						<div class="sidebar-todo animated fadeIn">
							<div class="sidebar-group">
								<h6>Important</h6>
								<div class="st-item">
									<label class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input">
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">Mock up</span>
									</label>
								</div>
								<div class="st-item">
									<label class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" checked>
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">Prototype iPhone</span>
									</label>
								</div>
								<div class="st-item">
									<label class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input">
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">Build final version</span>
									</label>
								</div>
								<div class="st-item">
									<label class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input">
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">Approval docs</span>
									</label>
								</div>
							</div>
							<div class="sidebar-group">
								<h6>Secondary</h6>
								<div class="st-item">
									<label class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" checked>
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">Website redesign</span>
									</label>
								</div>
								<div class="st-item">
									<label class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" checked>
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">Skype call</span>
									</label>
								</div>
								<div class="st-item">
									<label class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input">
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">Blog post</span>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tab-4" role="tabpanel">
						<div class="sidebar-settings animated fadeIn">
							<div class="sidebar-group">
								<h6>Main</h6>
								<div class="ss-item">
									<div class="text-truncate">Anyone can register</div>
									<div class="ss-checkbox"><input type="checkbox" class="js-switch" data-size="small" data-color="#3e70c9" checked></div>
								</div>
								<div class="ss-item">
									<div class="text-truncate">Allow commenting</div>
									<div class="ss-checkbox"><input type="checkbox" class="js-switch" data-size="small" data-color="#3e70c9"></div>
								</div>
								<div class="ss-item">
									<div class="text-truncate">Allow deleting</div>
									<div class="ss-checkbox"><input type="checkbox" class="js-switch" data-size="small" data-color="#3e70c9"></div>
								</div>
							</div>
							<div class="sidebar-group">
								<h6>Notificatiоns</h6>
								<div class="ss-item">
									<div class="text-truncate">Commits</div>
									<div class="ss-checkbox"><input type="checkbox" class="js-switch" data-size="small" data-color="#3e70c9"></div>
								</div>
								<div class="ss-item">
									<div class="text-truncate">Messages</div>
									<div class="ss-checkbox"><input type="checkbox" class="js-switch" data-size="small" data-color="#3e70c9" checked></div>
								</div>
							</div>
							<div class="sidebar-group">
								<h6>Security</h6>
								<div class="ss-item">
									<div class="text-truncate">Daily backup</div>
									<div class="ss-checkbox"><input type="checkbox" class="js-switch" data-size="small" data-color="#3e70c9" checked></div>
								</div>
								<div class="ss-item">
									<div class="text-truncate">API Access</div>
									<div class="ss-checkbox"><input type="checkbox" class="js-switch" data-size="small" data-color="#3e70c9" checked></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> -->

			<!-- Template options -->
<!-- 			<div class="template-options">
				<div class="to-toggle"><i class="ti-settings"></i></div>
				<div class="custom-scroll custom-scroll-dark">
					<div class="to-content">
						<a class="btn btn-info btn-block waves-effect waves-light mb-2" href="https://themeforest.net/item/neptune-dashboard-ui-kit-for-web-application-development/18519415?ref=demo" target="_blank">BUY NOW $24</a>
						<h6>Layouts</h6>
						<div class="row mb-2 text-xs-center">
							<div class="col-xs-6 mb-2">
								<div class="to-item">
									<a href="index-2.html">
										<img src="img/layouts/default.png" class="img-fluid">
									</a>
									<div class="text-muted">Default</div>
								</div>
							</div>
							<div class="col-xs-6 mb-2">
								<div class="to-item">
									<label>
										<input name="compact-sidebar" type="checkbox">
										<div class="to-icon"><i class="ti-check"></i></div>
										<img src="img/layouts/compact-sidebar.png" class="img-fluid">
									</label>
									<div class="text-muted">Compact Sidebar</div>
								</div>
							</div>
							<div class="col-xs-6 mb-2">
								<div class="to-item">
									<label>
										<input name="fixed-header" type="checkbox" checked>
										<div class="to-icon"><i class="ti-check"></i></div>
										<img src="img/layouts/fixed-header.png" class="img-fluid">
									</label>
									<div class="text-muted">Fixed Header</div>
								</div>
							</div>
							<div class="col-xs-6 mb-2">
								<div class="to-item">
									<label>
										<input name="fixed-sidebar" type="checkbox" checked>
										<div class="to-icon"><i class="ti-check"></i></div>
										<img src="img/layouts/sticky-sidebar.png" class="img-fluid">
									</label>
									<div class="text-muted">Sticky Sidebar</div>
								</div>
							</div>
							<div class="col-xs-6 mb-2">
								<div class="to-item">
									<label>
										<input name="boxed-wrapper" type="checkbox">
										<div class="to-icon"><i class="ti-check"></i></div>
										<img src="img/layouts/boxed-wrapper.png" class="img-fluid">
									</label>
									<div class="text-muted">Boxed Wrapper</div>
								</div>
							</div>
							<div class="col-xs-6 mb-2">
								<div class="to-item">
									<label>
										<input name="static" type="checkbox">
										<div class="to-icon"><i class="ti-check"></i></div>
										<img src="img/layouts/static.png" class="img-fluid">
									</label>
									<div class="text-muted">Static</div>
								</div>
							</div>
						</div>
						<h6>Skins</h6>
						<div class="row">
							<div class="col-xs-3 mb-2">
								<label class="skin-label">
									<input name="skin" value="skin-default" type="radio" checked>
									<div class="to-icon"><i class="ti-check"></i></div>
									<div class="to-skin">
										<span class="skin-dark-blue"></span>
										<span class="skin-white"></span>
										<span class="skin-dark-blue"></span>
									</div>
								</label>
							</div>
							<div class="col-xs-3 mb-2">
								<label class="skin-label">
									<input name="skin" value="skin-1" type="radio">
									<div class="to-icon"><i class="ti-check"></i></div>
									<div class="to-skin">
										<span class="skin-dark-blue-2"></span>
										<span class="skin-dark-blue-2"></span>
										<span class="bg-white"></span>
									</div>
								</label>
							</div>
							<div class="col-xs-3 mb-2">
								<label class="skin-label">
									<input name="skin" value="skin-2" type="radio">
									<div class="to-icon"><i class="ti-check"></i></div>
									<div class="to-skin">
										<span class="bg-danger"></span>
										<span class="bg-white"></span>
										<span class="bg-black"></span>
									</div>
								</label>
							</div>
							<div class="col-xs-3 mb-2">
								<label class="skin-label">
									<input name="skin" value="skin-3" type="radio">
									<div class="to-icon"><i class="ti-check"></i></div>
									<div class="to-skin">
										<span class="bg-white"></span>
										<span class="bg-white"></span>
										<span class="bg-white"></span>
									</div>
								</label>
							</div>
							<div class="col-xs-3 mb-2">
								<label class="skin-label">
									<input name="skin" value="skin-4" type="radio">
									<div class="to-icon"><i class="ti-check"></i></div>
									<div class="to-skin">
										<span class="bg-white"></span>
										<span class="skin-dark-blue-2"></span>
										<span class="bg-white"></span>
									</div>
								</label>
							</div>
							<div class="col-xs-3 mb-2">
								<label class="skin-label">
									<input name="skin" value="skin-5" type="radio">
									<div class="to-icon"><i class="ti-check"></i></div>
									<div class="to-skin">
										<span class="bg-primary"></span>
										<span class="bg-primary"></span>
										<span class="bg-white"></span>
									</div>
								</label>
							</div>
							<div class="col-xs-3 mb-2">
								<label class="skin-label">
									<input name="skin" value="skin-6" type="radio">
									<div class="to-icon"><i class="ti-check"></i></div>
									<div class="to-skin">
										<span class="bg-black"></span>
										<span class="bg-info"></span>
										<span class="bg-black"></span>
									</div>
								</label>
							</div>
						</div>
						<div class="to-material">
							<div class="tom-checkbox"><input name="material-design" type="checkbox" class="js-switch" data-size="small" data-color="#20b9ae"></div>
							<div class="text-truncate">Material design</div>
						</div>
					</div>
				</div>
			</div> -->

			<!-- Header -->
			<div class="site-header">
				<nav class="navbar navbar-light">

					<?php if($member_role == "global") { ?>
						<img src="<?php echo $domain; ?>/img/gdlogo.jpg" alt="<?php echo $company_name; ?>"  style="float: left !important;height: 55px; margin: 0px;" />
					<?php } ?>


					<div class="navbar-left">
						<a class="navbar-brand" href="#">
							<div class="logo1" style="color: white;"><?php echo $company_name; ?></div>
						</a>
						<div class="toggle-button dark sidebar-toggle-first float-xs-left hidden-md-up">
							<span class="hamburger"></span>
						</div>
						<div class="toggle-button-second dark float-xs-right hidden-md-up">
							<i class="ti-arrow-left"></i>
						</div>
						<div class="toggle-button dark float-xs-right hidden-md-up" data-toggle="collapse" data-target="#collapse-1">
							<span class="more"></span>
						</div>
					</div>
					<div class="navbar-right navbar-toggleable-sm collapse" id="collapse-1">
						<div class="toggle-button light sidebar-toggle-second float-xs-left hidden-sm-down">

							<?php if($member_role != "global") { ?>

								<span class="hamburger"></span> 

							<?php } ?>

						</div>
<!-- 						<div class="toggle-button-second light float-xs-right hidden-sm-down">
							<i class="ti-arrow-left"></i>
						</div> -->
						<ul class="nav navbar-nav float-md-right">
<!-- 							<li class="nav-item dropdown">
								<a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">
									<i class="ti-flag-alt"></i>
									<span class="hidden-md-up ml-1">Tasks</span>
									<span class="tag tag-success top">3</span>
								</a>
								<div class="dropdown-tasks dropdown-menu dropdown-menu-right animated fadeInUp">
									<div class="t-item">
										<div class="mb-0-5">
											<a class="text-black" href="#">First Task</a>
											<span class="float-xs-right text-muted">75%</span>
										</div>
										<progress class="progress progress-danger progress-sm" value="75" max="100">100%</progress>
										<span class="avatar box-32">
											<img src="img/avatars/2.jpg" alt="">
										</span>
										<a class="text-black" href="#">John Doe</a>, <span class="text-muted">5 min ago</span>
									</div>
									<div class="t-item">
										<div class="mb-0-5">
											<a class="text-black" href="#">Second Task</a>
											<span class="float-xs-right text-muted">40%</span>
										</div>
										<progress class="progress progress-purple progress-sm" value="40" max="100">100%</progress>
										<span class="avatar box-32">
											<img src="img/avatars/3.jpg" alt="">
										</span>
										<a class="text-black" href="#">John Doe</a>, <span class="text-muted">15:07</span>
									</div>
									<div class="t-item">
										<div class="mb-0-5">
											<a class="text-black" href="#">Third Task</a>
											<span class="float-xs-right text-muted">100%</span>
										</div>
										<progress class="progress progress-warning progress-sm" value="100" max="100">100%</progress>
										<span class="avatar box-32">
											<img src="img/avatars/4.jpg" alt="">
										</span>
										<a class="text-black" href="#">John Doe</a>, <span class="text-muted">yesterday</span>
									</div>
									<div class="t-item">
										<div class="mb-0-5">
											<a class="text-black" href="#">Fourth Task</a>
											<span class="float-xs-right text-muted">60%</span>
										</div>
										<progress class="progress progress-success progress-sm" value="60" max="100">100%</progress>
										<span class="avatar box-32">
											<img src="img/avatars/5.jpg" alt="">
										</span>
										<a class="text-black" href="#">John Doe</a>, <span class="text-muted">3 days ago</span>
									</div>
									<a class="dropdown-more" href="#">
										<strong>View all tasks</strong>
									</a>
								</div>
							</li>
-->


							<li class="nav-item dropdown">
<!-- 								<a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">
									<i class="ti-flag-alt"></i>
									<span class="hidden-md-up ml-1">Tasks</span>
									<span class="tag tag-success top">3</span>
								</a>
-->


								<!-- <img src="<?php echo $domain; ?>/img/esg.jpg" alt="ESG Division" class="esg_div_logo" /> -->




							</li>

							<?php

							if($member_role == "admin") {
								$subject = "Project status updated";

								$sql_fetch_notifs = mysql_query("SELECT * FROM `$gd`.`notifications` where subject = '$subject' and status = 'Unread' order by pk_notification_id desc  ", $connect_db);
								$notif_count = mysql_num_rows($sql_fetch_notifs);


							} else if($member_role == "user") {
								$subject = "New project assigned";

								$sql_fetch_notifs = mysql_query("SELECT * FROM `$gd`.`notifications` where subject = '$subject' and status = 'Unread'  and fk_employee_id = '$session_employee_id' order by pk_notification_id desc ", $connect_db);
								$notif_count = mysql_num_rows($sql_fetch_notifs);


							}

							?>


							<?php if($member_role != 'global') { ?>

							<li class="nav-item dropdown notification_btn">
								<a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">
									<i class="ti-bell"></i>
									<span class="hidden-md-up ml-1"> Notifications </span>
									<span class="tag <?php if($notif_count > 0) { ?> tag-danger <?php } ?> top"> 
										<?php echo $notif_count; ?>  
									</span>
								</a>

								<div class="dropdown-messages dropdown-tasks dropdown-menu dropdown-menu-right animated fadeInUp" <?php if($notif_count > 5) { ?> style="height: 400px;overflow-y: scroll; overflow-x: hidden;" <?php } ?>>

									<?php
									if($notif_count > 0) {

										while ($row_fetch_notifs = (mysql_fetch_array($sql_fetch_notifs)) ){
										extract($row_fetch_notifs);


											$sql_fetch_name = mysql_query("SELECT * FROM `$gd`.`employees` where pk_employee_id = '$added_by'   ", $connect_db);
												while ($row_fetch_name = (mysql_fetch_array($sql_fetch_name)) ){
												extract($row_fetch_name);



											$sql_fetch_project_id = mysql_query("SELECT * FROM `$gd`.`projects` where pk_project_id = '$fk_project_id'   ", $connect_db);
												while ($row_fetch_project_id = (mysql_fetch_array($sql_fetch_project_id)) ){
												extract($row_fetch_project_id);

									?>

									<div class="m-item">
										<div class="mi-icon bg-info"><i class="ti-comment"></i></div>

										<?php if($member_role == "admin") { ?>
										<div class="mi-text"><a class="text-black" href="#"> <?php echo "$full_name "; ?> </a> <span class="text-muted"> updated a project status for <?php echo $order_number; ?>.  </span> <a class="text-black" href="#">  </a></div>

										<?php } else { ?>

										<div class="mi-text"><a class="text-black" href="#"> <?php echo " $full_name "; ?> </a> <span class="text-muted"> assigned a new project to you. Project Number: <?php echo $order_number; ?>  </span> <a class="text-black" href="#">  </a></div>

										<?php } ?>

										<div class="mi-time"> 
											
											<?php 

												$yesterday = date('d/m/Y',strtotime("-1 days"));

												if($date_to_year === $row_fetch_notifs['simple_date']) { 
													echo " Today "; 
												} else if($row_fetch_notifs['simple_date'] == $yesterday) { 
													echo " Yesterday"; 
												} else {
													echo $row_fetch_notifs['simple_date'];
												}

											?>


										</div>
									</div>

									<?php } } } ?>

									<a class="dropdown-more" href="notifications.php">
										<strong> Read all notifications </strong>
									</a>

<!-- 									<a class="dropdown-more" href="#">
										<strong>View all notifications</strong>
									</a> -->

									<?php } else { ?>

									<a class="dropdown-more" href="notifications.php">
										<strong> No new notifications </strong>
									</a>

									<?php } ?>

								</div>
							</li> 

							<?php } ?>


							<li class="nav-item dropdown hidden-sm-down">

<!-- 								<a href="#" data-toggle="dropdown" aria-expanded="false">

									<?php if(!empty($session_employee_id)) { ?> 

										<span class="avatar box-32">

										<?php 

											$file_name_1 = str_replace(" ", "-", $session_full_name);
											$file_name_1 = strtolower($file_name_1);

											$avatar1 = "img/avatars/$file_name_1.png";
											$avatar2 = "img/avatars/$file_name_1.jpg";

							                if(file_exists($avatar1)) { 
							                	$avatar_img = $avatar1; 
							                } else if(file_exists($avatar2)) { 
							                	$avatar_img = $avatar2; 
							                } else {  
							                	$avatar_img = "img/avatars/avatar.png"; 
							                }

										?>

											<img src="<?php echo $avatar_img; ?>" alt="<?php echo $session_full_name; ?>">
										</span>

									<?php } ?>

								</a>

 -->


								<a href="#" data-toggle="dropdown" aria-expanded="false">

									<?php if(!empty($session_employee_id)) { ?> 

										<span class="avatar box-32">

<?php
$sql_fetch_avatar = mysql_query("SELECT avatar FROM `$gd`.`employees` where pk_employee_id = '$session_employee_id' ", $connect_db);
while ($row_fetch_avatar = mysql_fetch_array($sql_fetch_avatar)) {
	extract($row_fetch_avatar);

	// echo "$avatar";

	if(!empty($avatar)) {
		$avatar_1 = "img/avatars/".$avatar;
	} else {
		$avatar_2 = "img/avatars/avatar.png";
	}


	if(file_exists($avatar_1)) {
		$current_avatar = $avatar_1;
	} else {
		$current_avatar = $avatar_2;
	}


}
?>


											<img style="height: 100% !important;" src="<?php echo $current_avatar; ?>" alt="<?php echo "$session_full_name"; ?>">
										</span>

									<?php } ?>

								</a>



 								<div class="dropdown-menu dropdown-menu-right animated fadeInUp">

									<?php if(!empty($session_employee_id)) { ?> 

										<a class="dropdown-item" href="<?php echo $domain; ?>">
											<i class="fa fa-user"></i> <?php echo $session_full_name; ?>
										</a>

									<?php } ?>

									<hr/>


									<?php if($member_role != 'global') { ?>

 									<a class="dropdown-item logout" href="reset-password.php">
										<i class="fa fa-lock"></i> Change Password 
									</a> 

									<?php } ?>


									<a class="dropdown-item" href="<?php echo $file_path[35]; ?>">
										<i class="fa fa-sign-out"></i> Logout 
									</a>

<!--
									<a class="dropdown-item" href="#">
										<i class="ti-user mr-0-5"></i> Profile
									</a>
									<a class="dropdown-item" href="#">
										<i class="ti-settings mr-0-5"></i> Settings
									</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="#"><i class="ti-help mr-0-5"></i> Help</a>
									<a class="dropdown-item" href="#"><i class="ti-power-off mr-0-5"></i> Sign out</a>
-->
								</div> 
							</li>
						</ul>

						<ul class="nav navbar-nav">
<!-- 							<li class="nav-item hidden-sm-down">
								<a class="nav-link toggle-fullscreen" href="#">
									<i class="ti-fullscreen"></i>
								</a>
							</li>
 --><!-- 							<li class="nav-item dropdown hidden-sm-down">
								<a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">
									<i class="ti-layout-grid3"></i>
								</a>
								<div class="dropdown-apps dropdown-menu animated fadeInUp">
									<div class="a-grid">
										<div class="row row-sm">
											<div class="col-xs-4">
												<div class="a-item">
													<a href="#">
														<div class="ai-icon"><img class="img-fluid" src="img/brands/dropbox.png" alt=""></div>
														<div class="ai-title">Dropbox</div>
													</a>
												</div>
											</div>
											<div class="col-xs-4">
												<div class="a-item">
													<a href="#">
														<div class="ai-icon"><img class="img-fluid" src="img/brands/github.png" alt=""></div>
														<div class="ai-title">Github</div>
													</a>
												</div>
											</div>
											<div class="col-xs-4">
												<div class="a-item">
													<a href="#">
														<div class="ai-icon"><img class="img-fluid" src="img/brands/wordpress.png" alt=""></div>
														<div class="ai-title">Wordpress</div>
													</a>
												</div>
											</div>
											<div class="col-xs-4">
												<div class="a-item">
													<a href="#">
														<div class="ai-icon"><img class="img-fluid" src="img/brands/gmail.png" alt=""></div>
														<div class="ai-title">Gmail</div>
													</a>
												</div>
											</div>
											<div class="col-xs-4">
												<div class="a-item">
													<a href="#">
														<div class="ai-icon"><img class="img-fluid" src="img/brands/drive.png" alt=""></div>
														<div class="ai-title">Drive</div>
													</a>
												</div>
											</div>
											<div class="col-xs-4">
												<div class="a-item">
													<a href="#">
														<div class="ai-icon"><img class="img-fluid" src="img/brands/dribbble.png" alt=""></div>
														<div class="ai-title">Dribbble</div>
													</a>
												</div>
											</div>
										</div>
									</div>
									<a class="dropdown-more" href="#">
										<strong>View all apps</strong>
									</a>
								</div>
							</li> -->
						</ul>

<!-- 						<div class="header-form float-md-left ml-md-2">
							<form>
								<input type="text" class="form-control b-a" placeholder="Search for...">
								<button type="submit" class="btn bg-white b-a-0">
									<i class="ti-search"></i>
								</button>
							</form>
						</div> -->



						<div class="header-form float-md-left ml-md-2">
<!-- 							<?php

								echo "Welcome $session_full_name";

							?>
 -->						</div>


					</div>
				</nav>
			</div>
