<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>

<style type="text/css">
.error{ color: red; }
</style>


			<div class="site-content">
				<!-- Content -->
				<div class="content-area py-1">
					<div class="container-fluid">
						<h4> Add your activity </h4>
						<ol class="breadcrumb no-bg mb-1">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<!-- <li class="breadcrumb-item"><a href="#">Forms</a></li> -->
							<li class="breadcrumb-item active">Activity Tracker</li>
						</ol>

						<div id="animated_image"></div>
						<div id="result_container"></div>

						<div class="box box-block bg-white">
							<!-- <h5>Create new project</h5> -->
							<!-- <p class="font-90 text-muted mb-1">To use, add <code>.material-*</code> to the form.</p> -->
							<form id="new_activity" name="new_activity">
								<div class="form-group row">
									<label for="select2-demo-1" class="col-sm-2 form-control-label">Choose a Project:</label>
									<div class="col-sm-4">
										<select id="select2-demo-1" name="project_title" class="form-control" data-plugin="select2" >
<!-- 											<option value="option1">HTML</option>
											<option value="option2">CSS</option>
											<option value="option3">Javascript</option>
											<option value="option4">PHP</option>
											<option value="option5">Bootstrap</option>
 -->
											<?php 

												$sql_fetch_projects = mysql_query("SELECT * FROM `$gd`.`projects`  ", $connect_db);

												if(mysql_num_rows($sql_fetch_projects) > 0 ) {

													while ($row_fetch_projects = mysql_fetch_array($sql_fetch_projects)) {
												   	extract($row_fetch_projects);
											?>

													<option value="<?php echo $projectname; ?>"><?php echo $projectname; ?></option>

												<?php } // END OF WHILE ?>

											<?php } else { ?>

													<option value=""> Please add a Project </option>

											<?php } ?>

										</select>
									</div>
								</div>

								<div class="form-group row">
									<label for="input_2" class="col-sm-2 col-form-label">Start Time:</label>
<!-- 									<div class="col-sm-2">
										<div class="">
											<div class="input-group">
			                   					<input type="text" id="datepicker-autoclose" name="date" class="form-control" placeholder="mm/dd/yyyy" required>
			                    				<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
			                    			</div>
		                    			</div>
									</div>
 -->

							        <div class='col-sm-2'>
										<input class="form-control" id="datetimepicker" name="date_and_time" type="text" placeholder="Click to select date and time" required autocomplete="off">
							        </div>

								</div>


<!-- 
								<div class="form-group row">
									<label for="input_3" class="col-sm-2 col-form-label">Success</label>
									<div class="col-sm-4">
										<input type="email" class="form-control" name="input_3" placeholder="Success">
									</div>
								</div>

								<div class="form-group row">
									<label for="input_4" class="col-sm-2 col-form-label">Warning</label>
									<div class="col-sm-4">
										<input type="email" class="form-control" id="input_4" placeholder="Warning">
									</div>
								</div>

								<div class="form-group row">
									<label for="input_5" class="col-sm-2 col-form-label">Danger</label>
									<div class="col-sm-4">
										<input type="email" class="form-control" id="input_5" placeholder="Danger">
									</div>
								</div>
 -->
								<button type="submit" class="btn btn-primary w-min-sm mb-0-25 waves-effect waves-light">Submit</button>

							</form>
						</div>
					</div>
				</div>

<?php include $backend_footer_file; ?>



<!-- validation libs -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>


<!--- new_project form validation --> 
<script>   
$(document).ready(function(){
  $("#new_activity").validate({
    debug: false,
    submitHandler: function(form) {

      	$("#animated_image").show();
      	$("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[76]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

      	$.post('<?php echo $file_path[30]; ?>', $("#new_activity").serialize() + '&addNewActivity=addNewActivity', function(response) {

			$("#animated_image").hide();
			$("#result_container").html(response);

      	}); // END OF POST REQUEST 


    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>
