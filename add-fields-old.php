<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>

<style type="text/css">
.error{ color: red; }
</style>


			<div class="site-content">
				<!-- Content -->
				<div class="content-area py-1">
					<div class="container-fluid">
						<h4> Add dropdown field </h4>
						<ol class="breadcrumb no-bg mb-1">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<!-- <li class="breadcrumb-item"><a href="#">Forms</a></li> -->
							<li class="breadcrumb-item active">Add Fields</li>
						</ol>

						<nav class="box box-block bg-white">
							<!-- <h5 class="mb-1"> Consolidated </h5> -->

							<div class="form-group">
								<div class="row">
									<div class="col-md-3">
										<label>Select:</label>
										<select id="select2-demo-1" name="field_type" class="form-control field_type" data-plugin="select2" onchange="loadForm()">

											<option value="location" selected> Location </option>
											<!-- <option value="type_of_work"> Type of Work </option> -->
											<option value="project_status"> Project Status </option>
											<option value="project_type"> Project Type </option>
											<option value="project_division"> Project Division </option>

										</select>
									</div>

									<div class="col-md-9">
										<button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#NewFieldPopUp" style="float: right;"> <i class="fa fa-plus"></i> &nbsp; Add Field </button>
									</div>

								</div>
							</div>
							


							<!-- <button type="button" class="btn btn-primary btn-md" onclick="location.href='<?php echo $file_path[33]; ?>' " style="float: right;"> <i class="fa fa-plus"></i> &nbsp; Add project</button> -->

						</nav>

						<div id="animated_image"></div>
						<div id="result_container"></div>

						
						<div class="box box-block bg-white">
							<!-- <h5>Create new project</h5> -->
							<!-- <p class="font-90 text-muted mb-1">To use, add <code>.material-*</code> to the form.</p> -->
							<!-- <form id="new_activity" name="new_activity"> -->


								<div id="load_form"></div>


						</div>
					</div>
				</div>



<?php include $backend_footer_file; ?>



<!-- validation libs -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>




<script type="text/javascript">
$(window).on("load", function () {
	$('.type_of_work, .project_division, .project_type, .project_status').hide();
	loadForm();
});

function loadForm() {

	var name = $('.field_type').find(":selected").text();

	$.post('load_fields_form.php',  '&name='+name, function(response){
		$('#load_form').html(response);
	});

}	

</script>





<!-- Add FIeld Modal -->
<div id="NewFieldPopUp" class="modal fade" role="dialog">
 	<div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">

	    	<form id="dropdown_field_form" name="dropdown_field_form">

		    	<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title"> Add new field </h4>
		      	</div>

		      	<div class="modal-body">


					<div class="form-group">
						<div class="row">
						                                       
							<div class="col-md-6">
								<label>Select:</label>
								<select id="select2-demo-1" name="field_type_in_modal" class="form-control field_type_in_modal" data-plugin="select2" onchange="shuffleFields();">

									<option value="location" selected> Location </option>
									<!-- <option value="type_of_work"> Type of Work </option> -->
									<option value="project_status"> Project Status </option>
									<option value="project_type"> Project Type </option>
									<option value="project_division"> Project Division </option>

								</select>
							</div>
							                                    
							<div class="col-sm-6"> 


							</div>

						</div>
					</div>



					<div class="form-group">
						<div class="row">

							<div class="col-sm-6 location"> 
								<label class="control-label dynamic_label"> Country: </label>
								<input type="text" name="new_field_value"  class="form-control"  required>
							</div>

						</div>

					</div>

		      	</div> <!-- modal body -->

		      	<div class="modal-footer">
			    	<button type="submit" class="btn btn-primary">Save</button>
			    	<button type="button" class="btn" data-dismiss="modal">Close</button>
		      	</div>

			</form>
	    </div>

  	</div>
</div>








<!-- Delete Field Modal -->
<div id="DeleteModal" class="modal fade" role="dialog">
 	<div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	    	<div id="load_info"></div>
	    </div>

  	</div>
</div>






<script type="text/javascript">
$(document).ready(function(){
    $("#dropdown_field_form").validate({
      debug: false,
       submitHandler: function(form) {
        // do other stuff for a valid form

	    $('#NewFieldPopUp').modal('hide');

        $("#animated_image").show();
        $("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');
    
        $.post('<?php echo $file_path[30]; ?>', $("#dropdown_field_form").serialize() + '&newDropdownField=newDropdownField' , function(response) {


            $('#dropdown_field_form').find("input[type=text]").val("");


	        $("#animated_image").hide();
	        $('#result_container').empty();
	        $('#result_container').html(response);

	        // loadReport();

		}); // END OF POST REQUEST 

		} // END OF SUBMIT HANDLER

    });  // END OF VALIDATION

}); // END OF DOCUMENT READY FUNCTION 

</script>
  




<script type="text/javascript">
function shuffleFields() {

	var name = $('.field_type_in_modal').find(":selected").text();
	var name = name.trim();

	if(name == 'Location') {
		$('.dynamic_label').html('Location:');
		// $('.location').show();
		// $('.type_of_work, .project_division, .project_type, .project_status').hide();
	} else if(name == 'Type of Work') {
		$('.dynamic_label').html('Type of Work:');
		// $('.type_of_work').show();
		// $('.location, .project_division, .project_type, .project_status').hide();
	} else if(name == 'Project Status') {
		$('.dynamic_label').html('Project Status:');
		// $('.project_status').show();
		// $('.location, .project_division, .project_type, .type_of_work').hide();
	} else if(name == 'Project Type') {
		$('.dynamic_label').html('Project Type:');
		// $('.project_type').show();
		// $('.location, .project_division, .type_of_work, .project_status').hide();
	} else if(name == 'Project Division') {
		$('.dynamic_label').html('Project Division:');
		// $('.project_division').show();
		// $('.location, .type_of_work, .project_type, .project_status').hide();
	} else {

	}


}	
</script>



<script type="text/javascript">
function confirm_delete(id,type){

		$('#DeleteModal').modal('hide');

        $("#animated_image").show();
        $("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');
    
        $.post('<?php echo $file_path[32]; ?>', '&deleteDropdownField=deleteDropdownField' + '&pk_id='+id + '&type='+type , function(response) {

	        $("#animated_image").hide();
	        $('#result_container').empty();
	        $('#result_container').html(response);

	        // loadReport();

		}); // END OF POST REQUEST 

}
</script>