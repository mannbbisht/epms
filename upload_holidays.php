<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>

<link rel="stylesheet" href="vendor/DataTables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.bootstrap4.min.css">


<style type="text/css">
.error{ color: red; }
.dt-buttons{ float: right !important; margin: 0 0 0 20px; }
.buttons-copy{display: none}
/*.buttons-excel::before { Content: 'Download as ' }
.buttons-csv::before { Content: 'Download as ' }
.buttons-pdf::before { Content: 'Download as ' }
*/td,th {
	text-align: center;
}
#table-2{
	width: auto !important;
}
</style>

<?php

$isError = $_GET['error'];
$isSuccess = $_GET['success'];

$ip = "http://$_SERVER[HTTP_HOST]"; 

if(isset($_POST['submit'])){


    // Allowed mime types
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');

    
    // Validate whether selected file is a CSV file
    if(!empty($_FILES['file']['name'])){
        
		$name = $_FILES["file"]["name"];
		$ext = end((explode(".", $name))); # extra () to prevent notice

		if($ext != "csv") {
			echo("<script>location.href = '$ip/upload_holidays.php?error=true';</script>");
			exit;
		}

        // If the file is uploaded
        if(is_uploaded_file($_FILES['file']['tmp_name'])){

            
			$row = 1;
			if (($handle = fopen($_FILES['file']['tmp_name'], 'r')) !== FALSE) {

				$sql_delete_holidays = "DELETE FROM `public_holidays` WHERE `holiday_on` LIKE '%".date("Y")."%'";
				mysql_query($sql_delete_holidays,$connect_db);

			    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			        $num = count($data);
			        $row++;
			        for ($c=0; $c < $num; $c++) {
			        	$holiday_name = mysql_real_escape_string($data[0]);
			        	$date = date("Y-m-d", strtotime($data[1]));
			        }

					$sql_insert_holidays = "INSERT into  `$gd`.`public_holidays` (festival,holiday_on,added_by) VALUES ('$holiday_name','$date','$session_employee_id')  ";

					mysql_query($sql_insert_holidays, $connect_db) or die("Mysql error" . mysql_error());



			    }
			    fclose($handle);
			}

            $qstring = '?status=succ';
        }else{
            $qstring = '?status=err';
        }
    }else{
        $qstring = '?status=invalid_file';
    }

	echo("<script>location.href = '$ip/upload_holidays.php?success=true';</script>");
	exit;

}

?>

			<div class="site-content">
				<!-- Content -->
				<div class="content-area py-1">
					<div class="container-fluid">
						<h4> Public Holidays </h4>
						<ol class="breadcrumb no-bg mb-1">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<li class="breadcrumb-item active">Public Holidays</li>
						</ol>

						<?php

						if($isSuccess == true) {
							echo '
								<div class="alert alert-success-fill alert-dismissible fade in">
									File uploaded sucessfully.
								</div>
							';
						}

						if($isError == true) {
							echo '
								<div class="alert alert-danger-fill alert-dismissible fade in">
									Please upload a csv file.
								</div>
							';
						}

						?>


						<nav class="box box-block bg-white">
							<h5 class="mb-1"> CSV Upload </h5>

							<form id="filter_option_form" name="filter_option_form" method="post" enctype="multipart/form-data">
	                            <div class="row">
	                                <div class="col-md-3">
	                                	<input type="file" name="file" />
									</div>

									<div class="col-sm-1">
										<input type="submit" name="submit" class="btn btn-primary" value="Submit">
									</div>
								</div>
							</form>
						</nav>


						<div id="animated_image"></div>
						<div id="result_container"></div>


						<div class="box-block bg-white">

							<div class="dt-buttons btn-group">
								<a class="btn btn-secondary buttons-excel buttons-html5 SampleReport">
									Sample
								</a>
							</div>
							<table class="table table-striped table-bordered dataTable" id="table-2">
								<thead>
									<tr>
										<th> Festival </th>
										<th> Date </th>
									</tr>
								</thead>
								<tbody>

									<?php 

										$sql_fetch_holidays = mysql_query("SELECT * from public_holidays where holiday_on like '%".date("Y")."%'  ", $connect_db);
										while ($row_fetch_holidays = (mysql_fetch_array($sql_fetch_holidays)) ){
										extract($row_fetch_holidays);
									?>			

										<tr>
											<td>  <?php echo $festival; ?> </td>
											<td>  <?php echo $holiday_on; ?> </td>
										</tr>

									<?php }  ?>

								</tbody>
							</table>

						</div> <!-- box-block -->

					</div>
				</div>

			</div>


<?php include $backend_footer_file; ?>

	
<script type="text/javascript">
$(document).ready(function(){
    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel ");  
    $(".dt-buttons .SampleReport").html("<i class='fa fa-download'></i> Sample File ");  
});
</script>