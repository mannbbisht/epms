<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>


<?php include $backend_header_file; ?>

<link rel="stylesheet" href="vendor/DataTables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.bootstrap4.min.css">


<!-- MULTI SELECT CSS -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.2/multiple-select.min.css">


<style type="text/css">
#filter_option_form select{
	width: 130px !important;
}
#table-2{
	width: 100% !important;
}	

</style>




<?php /********************************************* APPLY FILTER ON PAGE SUBMIT *********************************************/ ?>

<?php

$sql_fetch_projects = mysql_query("SELECT * FROM `$gd`.`projects`  where assign_to like '%$session_full_name%' order by STR_TO_DATE(order_recieved_date, '%d/%m/%Y') ", $connect_db);

?>	

			<div class="site-content">
				<!-- Content -->
				<div class="content-area py-1">
					<div class="container-fluid">
						<h4> Assigned Projects </h4>
						<ol class="breadcrumb no-bg mb-1">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<li class="breadcrumb-item active">Assigned Projects</li>
						</ol>


						<?php if(!empty($_POST)) { ?>

						<div class="alert alert-success-fill alert-dismissible fade in">

							<?php 

								$message = "You have selected: ";

								if(!empty($sel_month) && $sel_month != "0all"  ) {
									// $message .= "Month: " . date("F", mktime(0, 0, 0, $all_months, 10));

									$message .= "Month: " ;

									foreach ($sel_month as $key => $value) {
										$message .= date("F", mktime(0, 0, 0, $sel_month[$key], 10));
		
										$count = count($sel_month);
										$count = $count - 1;
										
										if($key < $count) {
											$message .= ", ";
										}


									}

									$showComma = 1;

								} 



								echo $message;

							?>

						</div>

						<?php } ?>

						<div id="animated_image"></div>
						<div id="result_container"></div>


						<div class="box box-block bg-white">
							<!-- <h5>Create new project</h5> -->
							<!-- <p class="font-90 text-muted mb-1">To use, add <code>.material-*</code> to the form.</p> -->

							<div id="report_container">

								<table class="table table-striped table-bordered dataTable" id="table-2">
									<thead>
										<tr>


											<th> Division </th>
											<th> Order Number </th>
											<th> Location </th>
											<th> Request From </th>
											<th> Project Title </th>
											<th> Description </th>
											<th> Assign To </th>
											<th> Project Type </th>
											<th id="order_recieved_date"> Order Received Date </th>
											<th id="customer_want_date"> Customer Want Date </th>
											<th id="engineer_commitment_date"> Engineer Commitment Date </th>
											<th id="actual_completion_date"> Actual Completion </th>
											<th> Status </th>
											<th> Complexity Level </th>

											<th class="noExport" style="display: none;"> Order Date Sort Column </th>
											<th class="noExport" style="display: none;"> Customer Date Sort Column </th>
											<th class="noExport" style="display: none;"> Commitment Date Sort Column </th>
											<th class="noExport" style="display: none;"> Completion Date Sort Column </th>


										</tr>
									</thead>
									<tbody>

										<?php 

										while ($row_fetch_projects = (mysql_fetch_array($sql_fetch_projects)) ){
										extract($row_fetch_projects);	


											$order_date = str_replace("/", "-", $order_recieved_date);
											$order_date_miliseconds = strtotime($order_date);

											$customer_date = str_replace("/", "-", $customer_want_date);
											$customer_date_miliseconds = strtotime($customer_date);

											$completion_date = str_replace("/", "-", $actual_completion_date);
											$completion_date_miliseconds = strtotime($completion_date);

											$self_commitment_date_1 = str_replace("/", "-", $self_commitment_date);
											$self_commitment_date_miliseconds = strtotime($self_commitment_date_1);


										    $month1 = substr($actual_completion_date, 3, 2); 
										    $year1 = substr($actual_completion_date, 6, 4); 

											$description = utf8_encode($description);
											$location = utf8_encode($location);

										?>


										<?php if($status == "Completed" && $month1 == 08 && $year1 == 2019) { ?>

										<tr>

											<td> <?php print_value($division); ?> </td>
											<td> <?php print_value($order_number); ?> </td>
											<td> <?php print_value($location); ?> </td>
											<td> <?php print_value($request_from); ?> </td>
											<td> <?php print_value($project_name); ?> </td>
											<td> <?php print_value($description); ?> </td>
											<td> <?php print_value($assign_to); ?> </td>
											<td> <?php print_value($project_type); ?> </td>
											<td> <?php print_value($order_recieved_date); ?> </td>
											<td> <?php print_value($customer_want_date); ?> </td>
											<td> <?php print_value($self_commitment_date); ?> </td>

											<td> <?php print_value($actual_completion_date); ?> </td>
											<td> <?php print_value($status); ?> </td>
											<td> <?php print_value($complexity_level); ?> </td>

											<td style="display: none;"> <?php echo $order_date_miliseconds; ?> </td>
											<td style="display: none;"> <?php echo $customer_date_miliseconds; ?> </td>
											<td style="display: none;"> <?php echo $self_commitment_date_miliseconds; ?> </td>
											<td style="display: none;"> <?php echo $completion_date_miliseconds; ?> </td>

										</tr>

										<?php } else if ($status != "Completed") { ?> 

										<tr>

											<td> <?php print_value($division); ?> </td>
											<td> <?php print_value($order_number); ?> </td>
											<td> <?php print_value($location); ?> </td>
											<td> <?php print_value($request_from); ?> </td>
											<td> <?php print_value($project_name); ?> </td>
											<td> <?php print_value($description); ?> </td>
											<td> <?php print_value($assign_to); ?> </td>
											<td> <?php print_value($project_type); ?> </td>
											<td> <?php print_value($order_recieved_date); ?> </td>
											<td> <?php print_value($customer_want_date); ?> </td>
											<td> <?php print_value($self_commitment_date); ?> </td>
											<td> <?php print_value($actual_completion_date); ?> </td>
											<td> <?php print_value($status); ?> </td>
											<td> <?php print_value($complexity_level); ?> </td>

											<td style="display: none;"> <?php echo $order_date_miliseconds; ?> </td>
											<td style="display: none;"> <?php echo $customer_date_miliseconds; ?> </td>
											<td style="display: none;"> <?php echo $self_commitment_date_miliseconds; ?> </td>
											<td style="display: none;"> <?php echo $completion_date_miliseconds; ?> </td>

										</tr>


										<?php } }  ?>

									</tbody>
								</table>

							</div>


						</div> <!-- box-block -->
					</div>
				</div>

			</div>


<?php include $backend_footer_file; ?>








<script type="text/javascript">
$(document).ready(function(){
    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel ");  

	$(document).prop('title', 'Assigned Projects');
});
</script>


<script src="//cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.2/multiple-select.min.js"></script>

<!-- 
<script>
  init({
    title: 'selected/disabled options',
    desc: 'Multiple Select can take from selected and disabled options',
    links: ['multiple-select.css'],
    scripts: ['multiple-select.js']
  })
</script>

 -->

<script>
  // Here the mounted function is same as $(function() {})
$(document).ready(function(){
    $('select').multipleSelect({
    	filter: true
    })
});
</script>



<script type="text/javascript" src="vendor/DataTables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Responsive/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Responsive/js/responsive.bootstrap4.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.bootstrap4.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/JSZip/jszip.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/pdfmake/build/pdfmake.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/pdfmake/build/vfs_fonts.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.print.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.colVis.min.js"></script>


	<script type="text/javascript">

	    $('#table-2').DataTable( {
	        dom: 'Bfrtip',

	        "scrollX": "500px",
	        "scrollY": "2000px",
	        "scrollCollapse": true,
		    paging: false,

	        "aaSorting": [],


	        destroy: true,
			buttons: 
			[
			  {
			    extend: 'excel',
			    // footer: true,
			      exportOptions: {
					columns: "thead th:not(.noExport)"
			      }
			  },
			  {
			    extend: '',
			    footer: false
			  },
			  {
			    extend: '',
			    footer: false
			  }         
			]  

	    } );


	</script>






<script type="text/javascript">

var counter = 1;
    
$(document).ready(function() {
    	

	$("#order_recieved_date").click(function(){

	counter++;
	    
		// $("#result_container").text(counter);

		if(counter % 2 === 0) {

			var table = $('#table-2').DataTable();
			table.order.listener( '#order_recieved_date', 14 );

			table.order([14, "desc"]).draw();


		} else {

			var table = $('#table-2').DataTable();
			table.order.listener( '#order_recieved_date', 14 );

			table.order([14, "asc"]).draw();

		} 

	    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel  ");  

	}); // end of $("#sorter").click(function(){

}); // end of doc ready
</script>


<script type="text/javascript">

var counter = 1;
    
$(document).ready(function() {
    	

	$("#customer_want_date").click(function(){

	counter++;
	    
		// $("#result_container").text(counter);

		if(counter % 2 === 0) {

			var table = $('#table-2').DataTable();
			table.order.listener( '#customer_want_date', 15 );

			table.order([15, "desc"]).draw();


		} else {

			var table = $('#table-2').DataTable();
			table.order.listener( '#customer_want_date', 15 );

			table.order([15, "asc"]).draw();

		} 

	    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel  ");  

	}); // end of $("#sorter").click(function(){

}); // end of doc ready
</script>




<script type="text/javascript">

var counter = 1;
    
$(document).ready(function() {

	$("#engineer_commitment_date").click(function(){

	counter++;
	    
		// $("#result_container").text(counter);

		if(counter % 2 === 0) {

			var table = $('#table-2').DataTable();
			table.order.listener( '#engineer_commitment_date', 16 );

			table.order([16, "desc"]).draw();


		} else {

			var table = $('#table-2').DataTable();
			table.order.listener( '#engineer_commitment_date', 16 );

			table.order([16, "asc"]).draw();

		} 

	    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel  ");  

	}); // end of $("#sorter").click(function(){

}); // end of doc ready
</script>





<script type="text/javascript">

var counter = 1;
    
$(document).ready(function() {

	$("#actual_completion_date").click(function(){

	counter++;
	    
		// $("#result_container").text(counter);

		if(counter % 2 === 0) {

			var table = $('#table-2').DataTable();
			table.order.listener( '#actual_completion_date', 17 );

			table.order([17, "desc"]).draw();


		} else {

			var table = $('#table-2').DataTable();
			table.order.listener( '#actual_completion_date', 17 );

			table.order([17, "asc"]).draw();

		} 

	    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel  ");  

	}); // end of $("#sorter").click(function(){

}); // end of doc ready
</script>


