<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>

<link rel="stylesheet" href="vendor/DataTables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.bootstrap4.min.css">

<style type="text/css">
.no-bgcolor{
	background-color: white !important;
}
.no-border{
	border: 1px solid white !important;
}
#report_container{
	width: 100%;
	overflow-x: scroll;
}
</style>


			<div class="site-content">
				<!-- Content -->
				<div class="content-area py-1">
					<div class="container-fluid">
<!-- 						<h4> Track project activity </h4>
						<ol class="breadcrumb no-bg mb-1">
							<lli class="breadcrumb-item"><a href="#">Home</a></li>
							<li class="breadcrumb-item active">Activity Tracker</li>
						</ol>
 -->


						<div id="animated_image"></div>
						<div id="result_container"></div>


						<div class="box box-block bg-white">
							<!-- <h5>Create new project</h5> -->
							<!-- <p class="font-90 text-muted mb-1">To use, add <code>.material-*</code> to the form.</p> -->

							<div id="report_container">

								<table class="table table-striped table-bordered dataTable" id="table-2">
									<thead>
										<tr>
											<th> Order Number </th>
											<th> Revision </th>
											<th> Latest Status </th>
											<th>  </th>
											<th>  </th>

											<?php 

											// $sql_fetch_project = mysql_query(" SELECT distinct fk_project_id  FROM `project_revision` WHERE  inserted_date like '%".$month."%'   ", $connect_db);
											// while ($row_fetch_project = (mysql_fetch_array($sql_fetch_project)) ){
											// 	extract($row_fetch_project);



												// $sql_fetch_revision = mysql_query(" SELECT fk_project_id  FROM `project_revision` WHERE fk_project_id = '$fk_project_id' and inserted_date like '%".$month."%'   ", $connect_db);
												// $count = mysql_num_rows($sql_fetch_revision);
											// }


											$sql_fetch_revision = mysql_query(" SELECT fk_project_id, COUNT(*) AS total FROM project_revision where scope_cleared_date != '' GROUP BY fk_project_id ORDER BY total DESC LIMIT 1  ", $connect_db);
											$row_fetch_revision = mysql_fetch_array($sql_fetch_revision);
											$total = $row_fetch_revision['total'];


											$i=1;
											while ($i <= $total) {

											?>

											<th colspan="2"> Rev <?php echo $i; ?> </th>

											<?php $i++; } ?>

										</tr>


									</thead>


									<tbody>



								<!-- 		<tr>

											<td rowspan="2"> AB10001 </td>
											<td> Status </td>

											<td></td>
											<td> To be started </td>
											<td> Scope Clarity </td>

											<td>In Progress</td>
											<td>Under Customer Review</td>

											<td>In Progress</td>
											<td>Under Customer Review</td>

										</tr>

										<tr>
											<td> Date </td>
											<td>  </td>
											<td>  </td>
											<td>  </td>

											<td> 03/03/2019 </td>
											<td> 25/04/2019 </td>

											<td> 26/04/2019 </td>
											<td> 01/05/2019 </td>

										</tr> -->	


										<?php 

											// $sql_fetch_count = mysql_query("SELECT * FROM `project_revision` WHERE inserted_date like '%".$month."%'  ", $connect_db);
											// $count = mysql_num_rows($sql_fetch_count);

											$sql_fetch_project_info_1 = mysql_query("SELECT distinct fk_project_id FROM `project_revision` where  inserted_date like '__/05/____'  ", $connect_db);

											while ($row_fetch_project_info_1 = (mysql_fetch_array($sql_fetch_project_info_1)) ){
												extract($row_fetch_project_info_1);

												$sql_fetch_order_number = mysql_query("SELECT * FROM `projects` WHERE pk_project_id = '$fk_project_id' ", $connect_db);
												while ($row_fetch_order_number = (mysql_fetch_array($sql_fetch_order_number)) ){
													extract($row_fetch_order_number);


													$actual_completion_date = $actual_completion_date;

										?>


										<tr>


											<td rowspan="3"> <?php echo "$order_number"; ?> </td>

											<td> Status </td>


											<td> <?php echo $status; ?> </td>
											<td> To be started </td>
											<td> Scope Clarity </td>

											<?php

											$i=1;
											while ($i <= $total) {

											?>

											<td>In Progress</td>
											<td>Under Customer Review</td>

											<?php $i++; } ?>

											<td>Completed</td>


										</tr>

										<tr>
											<td> Date </td>
											<td>  </td>
											<td>  </td>
											<td>  </td>

											<?php 

											$sql_fetch_project_info = mysql_query("SELECT * FROM `project_revision` where scope_cleared_date != '' and fk_project_id = '$pk_project_id' and inserted_date like '__/05/____'  ", $connect_db);
											while ($row_fetch_project_info = (mysql_fetch_array($sql_fetch_project_info)) ){
												extract($row_fetch_project_info);


											?>

											<td>
												<table>
												    <tr class="no-bgcolor">
												        <td class="no-border"> Commitment Date </td>
												    	<td class="no-border"> Completion Date </td>
												    </tr>

												</table>
											</td>
											
											<td> 
 											</td>


											<?php } ?>

											<!-- <td> <span style="color:red;"> <?php echo $actual_completion_date; ?> </span> </td> -->

											<!-- <td colspan="10"> <p style="float: right;color: red;"><?php echo $actual_completion_date; ?></p> </td> -->

										</tr>	


										<tr>
											<td> Input </td>
											<td>  </td>

											<?php

												$sql_fetch_project_log = mysql_query("SELECT * FROM `project_revision` WHERE project_start_date != ''  and fk_project_id = '$fk_project_id'    ", $connect_db);
												$row_fetch_project_log = mysql_fetch_array($sql_fetch_project_log);
												$project_start_date = $row_fetch_project_log['project_start_date'];
											?>

											<td>  <?php echo $project_start_date; ?>  </td>


											<?php

												$sql_fetch_project_log = mysql_query("SELECT * FROM `project_revision` WHERE scope_cleared_date != ''  and fk_project_id = '$fk_project_id'    ", $connect_db);
												$row_fetch_project_log = mysql_fetch_array($sql_fetch_project_log);
												$scope_cleared_date = $row_fetch_project_log['scope_cleared_date'];
											?>

											<td>  <?php echo $scope_cleared_date; ?>  </td>

											<?php 

											$sql_fetch_project_info = mysql_query("SELECT * FROM `project_revision` WHERE   fk_project_id = '$pk_project_id' and inserted_date like '__/05/____'  ", $connect_db);
											while ($row_fetch_project_info = (mysql_fetch_array($sql_fetch_project_info)) ){
												extract($row_fetch_project_info);

												if(!empty($self_commitment_date) ) {

											?>

											<td>
												<table>
												    <tr>
												        <td> <?php if(!empty($self_commitment_date)) { echo $self_commitment_date; } else { echo "not updated yet"; } ?>  </td>
												    	<td> <?php if(!empty($submission_date)) { echo $submission_date; } else { echo "not updated yet"; } ?>  </td>
												    </tr>

												</table>
											</td>
 
											
											<td> 
												<?php echo $customer_feedback_date;  ?>
											</td>


											<?php } } ?>

											<!--  <td> <span style="color:red;"> <?php echo $actual_completion_date; ?> </span> </td> --> 

											<td colspan="10"> <p style="float: right;color: red;"><?php echo $actual_completion_date; ?></p> </td>

										</tr>	


										<?php } } ?>

									</tbody>

								</table>
								
							</div>


						</div> <!-- box-block -->
					</div>
				</div>

			</div>



<?php include $backend_footer_file; ?>


<script type="text/javascript">
function loadReport() {

	var month = $('#choose_month').find(":selected").val();

    $("#animated_image").show();
    $("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo loading_data; ?></span>');

	$.post('load_status_dates_report.php', '&month_value='+month , function(response) {
		$("#animated_image").hide();
		$('#report_container').html(response);
	});
}	
</script>																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																						