<?php

/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt    LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
// error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once '../Classes/PHPExcel.php';


// Create new PHPExcel object
// echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();

// SET FONT SIZE

$objPHPExcel->getActiveSheet()->getStyle("A1:Z1")->getFont()->setSize(10);


// Set document properties
// echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                             ->setLastModifiedBy("Maarten Balliauw")
                             ->setTitle("PHPExcel Test Document")
                             ->setSubject("PHPExcel Test Document")
                             ->setDescription("Test document for PHPExcel, generated using PHP classes.")
                             ->setKeywords("office PHPExcel php")
                             ->setCategory("Test result file");


// Add some data
// echo date('H:i:s') , " Add some data" , EOL;



$styleArray = array(
    'font'  => array(
        // 'bold'  => true,
        'color' => array('rgb' => 'FF0000')
        // 'size'  => 15,
        // 'name'  => 'Verdana'
    )
);

// Add some data
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A1", "Order Number")
    ->setCellValue("B1", "Revision")
    ->setCellValue("C1", "Latest Status")
    ->setCellValue("D1", "")
    ->setCellValue("E1", "");


$sql_fetch_revision = mysql_query(" SELECT fk_project_id, COUNT(*) AS total FROM project_revision where project_start_date != '' GROUP BY fk_project_id ORDER BY total DESC LIMIT 1  ", $connect_db);
$row_fetch_revision = mysql_fetch_array($sql_fetch_revision);
$total = $row_fetch_revision['total'];

$i=1;
$rev_label = 6;
$head_label = "F";
$ipg_label_header = "G";
$ipg_label_header2 = "H";

// completed header on line 1
$completed_label_header_1 = ord($head_label);
$completed_label_header_1 += $total * 4;
$completed_label_header_1 = chr($completed_label_header_1);

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("$completed_label_header_1"."1", "Completed Date");


while ($i <= $total) {

    $head_label++;

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("$head_label"."1", "Rev $i");

        $ucr_label_header = ord($ipg_label_header);
        $ucr_label_header += 2;
        $ucr_label_header = chr($ucr_label_header);

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("$head_label"."2", "In Progress")

            ->setCellValue("$ipg_label_header"."3", "Project Start Date")
            // ->setCellValue("$ipg_label_header"."3", "Commitment Date")
            ->setCellValue("$ipg_label_header2"."3", "Engineer Commitment Date");

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("$ucr_label_header"."2", "Under Customer Review")
            ->setCellValue("$ucr_label_header"."3", "Actual Submission Date");




        $head_label++;
        $head_label++;

        $ipg_label_header++;
        $ipg_label_header++;
        $ipg_label_header++;

        $ipg_label_header2++;
        $ipg_label_header2++;
        $ipg_label_header2++;
        $rev_label++;
        $i++;



}

    // $head_label;
    // $objPHPExcel->setActiveSheetIndex(0)
    //     ->setCellValue("$head_label"."1", "Completed");


$body_alphabet = "A";
$body_row_count = 2;

$status_row_count = 2;
$date_row_count = 4;
$date_label_count = 3;

$input_row_count = 4;

$ipd_start_alphabet = "G";
$ucr_start_alphabet = "H";


$sql_fetch_project_info_1 = mysql_query("SELECT distinct fk_project_id FROM `project_revision`  order by fk_project_id asc  ", $connect_db);
while ($row_fetch_project_info_1 = (mysql_fetch_array($sql_fetch_project_info_1)) ){
extract($row_fetch_project_info_1);

    $sql_fetch_order_number = mysql_query("SELECT * FROM `projects` WHERE pk_project_id = '$fk_project_id' ", $connect_db);
    while ($row_fetch_order_number = (mysql_fetch_array($sql_fetch_order_number)) ){
    extract($row_fetch_order_number);



        // --------------------

        $in_progress_date_start = "G";
        $commitment_date_start = "H";
        $customer_feedback_date_start = "I";

        $sql_fetch_project_log = mysql_query("SELECT * FROM `project_revision` WHERE  fk_project_id = '$fk_project_id'    ", $connect_db);
        while ($row_fetch_project_log = (mysql_fetch_array($sql_fetch_project_log)) ){
            extract($row_fetch_project_log);


            if(!empty($project_start_date)) {

                if($input_row_count > 4) {

                    $head_1 = $input_row_count - 2;

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("$in_progress_date_start"."$head_1", "In Progress")
                        ->setCellValue("$customer_feedback_date_start"."$head_1", "Under Customer Review");


                    $head_2 = $input_row_count - 1;

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("$in_progress_date_start"."$head_2", "Project Start Date")
                        ->setCellValue("$commitment_date_start"."$head_2", "Engineer Commitment Date")
                        ->setCellValue("$customer_feedback_date_start"."$head_2", "Actual Submission Date");

                }

                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("$in_progress_date_start"."$input_row_count", "$project_start_date");


                $objPHPExcel->getActiveSheet()->getStyle("$in_progress_date_start"."$input_row_count")->applyFromArray($styleArray);

                $in_progress_date_start++;
                $in_progress_date_start++;
                $in_progress_date_start++;


            }


            if(!empty($self_commitment_date)) {

                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("$commitment_date_start"."$input_row_count", "$self_commitment_date");

                $objPHPExcel->getActiveSheet()->getStyle("$commitment_date_start"."$input_row_count")->applyFromArray($styleArray);

                $commitment_date_start++;
                $commitment_date_start++;
                $commitment_date_start++;

            }



            if(!empty($customer_feedback_date)) {

                if(empty($project_start_date)) {


                // if actual submission date is not empty then just show header on each row for that cell only
                // only for old records inserted by excel sheet

                // for new records no need of below code

                if($input_row_count > 4) {

                    $head_1 = $input_row_count - 2;

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("$customer_feedback_date_start"."$head_1", "Under Customer Review");


                    $head_2 = $input_row_count - 1;

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("$customer_feedback_date_start"."$head_2", "Actual Submission Date");

                }


                }


                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("$customer_feedback_date_start"."$input_row_count", "$customer_feedback_date");


                $objPHPExcel->getActiveSheet()->getStyle("$customer_feedback_date_start"."$input_row_count")->applyFromArray($styleArray);

                $customer_feedback_date++;
                $customer_feedback_date++;
                $customer_feedback_date++;

            }



            if(!empty($actual_completion_date)) {

                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("$completed_label_header_1"."$input_row_count", "$actual_completion_date");

                $objPHPExcel->getActiveSheet()->getStyle("$completed_label_header_1"."$input_row_count")->applyFromArray($styleArray);

            }



        }



        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("D"."$date_row_count", "$project_start_date")
            ->setCellValue("E"."$date_row_count", "$scope_cleared_date");


        $objPHPExcel->getActiveSheet()->getStyle("D"."$date_row_count")->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle("E"."$date_row_count")->applyFromArray($styleArray);


        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("$body_alphabet"."$body_row_count", "$order_number")

            ->setCellValue("B"."$status_row_count", "Status")
            ->setCellValue("B"."$date_label_count", "Date")
            ->setCellValue("B"."$input_row_count", "Input")
            ->setCellValue("C"."$body_row_count", "$status")
            ->setCellValue("D"."$body_row_count", "To be started")
            ->setCellValue("E"."$body_row_count", "Scope Clarity");



        // $body_alphabet++;
        $body_row_count += 2;

        $status_row_count += 2;
        $date_row_count += 2;
        $date_label_count += 3;

        $input_row_count += 2;

        $body_row_count++;

        $status_row_count++;
        $date_row_count++;
        $input_row_count++;
        $ipd_start_alphabet++;

        unset($project_start_date);


    }


}






// $sql_fetch_project_log = mysql_query("SELECT * FROM `project_revision` WHERE project_start_date != '' and fk_project_id = '$fk_project_id'    ", $connect_db);
// while ($row_fetch_project_log = (mysql_fetch_array($sql_fetch_project_log)) ){
//     extract($row_fetch_project_log);

//     $objPHPExcel->setActiveSheetIndex(0)
//         ->setCellValue("$ipd_start_alphabet"."$date_row_count", "$project_start_date");

// }


$objPHPExcel->getActiveSheet()->setTitle('Simple');



$objPHPExcel->setActiveSheetIndex(0);



$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$path = 'Status Dates Report.xls';
$objWriter->save($path);


$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;


?>

<script type="text/javascript">
location.href = '<?php echo $path; ?>';

setTimeout(function() {
    window.close();
}, 1000);

</script>