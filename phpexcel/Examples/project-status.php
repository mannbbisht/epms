<?php

/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt    LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
// error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once '../Classes/PHPExcel.php';


// Create new PHPExcel object
// echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();

// SET FONT SIZE

$objPHPExcel->getActiveSheet()->getStyle("A1:Z1")->getFont()->setSize(10);


// Set document properties
// echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                             ->setLastModifiedBy("Maarten Balliauw")
                             ->setTitle("PHPExcel Test Document")
                             ->setSubject("PHPExcel Test Document")
                             ->setDescription("Test document for PHPExcel, generated using PHP classes.")
                             ->setKeywords("office PHPExcel php")
                             ->setCategory("Test result file");


// Add some data
// echo date('H:i:s') , " Add some data" , EOL;


// LAST 7 MONTHS
for ($i=6; $i >= 0; $i--) { 

    $last_7_months_numeric[] = date("m", strtotime("-".$i."months"));
    $last_7_months[] = date("F Y", strtotime("-".$i."months"));

}


$sql_fetch_location = mysql_query("SELECT * FROM `$gd`.`location_dropdown` where country  != 'India' and country != 'Nuremberg' order by field(country, 'Quincy', 'UK', 'Finland', 'Parma', 'Bad Neustadt', 'Simmern', 'Schopfheim', 'Nash-Nuremberg', 'Nash-Schopfheim', 'Bentleyville', 'Sheboygan', 'Furstenfeldbruck', 'Frankfurt') ", $connect_db);
// $loc_count = 1;
// $total_countries = mysql_num_rows($sql_fetch_location);
while ($row_fetch_location = (mysql_fetch_array($sql_fetch_location)) ){
extract($row_fetch_location);

    $all_countries[] = $country;

}


$sql_fetch_status = mysql_query("SELECT * FROM `$gd`.`project_status_dropdown` where project_status != 'Scope Clarity' order by field(project_status, 'Completed', 'In Progress', 'To be started', 'New Requirement', 'Under Customer Review', 'Proposal Under Devlpt', 'Hold', 'Opportunity Lost')   ", $connect_db);
while ($row_fetch_status = (mysql_fetch_array($sql_fetch_status)) ){
extract($row_fetch_status);

    $all_status[] = $project_status;

}


// $objPHPExcel->getActiveSheet()->mergeCells('B1:F1');



// Add some data


$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A1", "Status");

$head = "B";
$row = 2;

foreach ($all_countries as $key => $value) {


    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("$head"."1", "$value");
            
    $head++;

}


foreach ($all_status as $status_name) {

    foreach ($all_countries as $key1 => $country_name) {

        $sql_fetch_project_count = mysql_query(" SELECT *  FROM `$gd`.`projects` where  location = '$country_name' and status = '$status_name'  ", $connect_db);

        $count1 = count($all_countries);
        for ($a=0; $a <= $count1 ; $a++) { 

            if($a == $key1) {
                $array[$a] = mysql_num_rows($sql_fetch_project_count);
            }

        }

    }                                            



    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A$row", "$status_name")
        ->setCellValue("B$row", "$array[0]")
        ->setCellValue("C$row", "$array[1]")
        ->setCellValue("D$row", "$array[2]")
        ->setCellValue("E$row", "$array[3]")
        ->setCellValue("F$row", "$array[4]")
        ->setCellValue("G$row", "$array[5]")
        ->setCellValue("H$row", "$array[6]")
        ->setCellValue("I$row", "$array[7]")
        ->setCellValue("J$row", "$array[8]")
        ->setCellValue("K$row", "$array[9]")
        ->setCellValue("L$row", "$array[10]")
        ->setCellValue("M$row", "$array[11]")
        ->setCellValue("N$row", "$array[12]")
        ->setCellValue("O$row", "$array[13]")
        ->setCellValue("P$row", "$array[14]");



    $row++;


}



$objPHPExcel->getActiveSheet()->setTitle('Simple');



$objPHPExcel->setActiveSheetIndex(0);



$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$path = 'Project Status Report.xls';
$objWriter->save($path);


$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;


?>

<script type="text/javascript">
location.href = '<?php echo $path; ?>';

setTimeout(function() {
    window.close();
}, 1000);

</script>