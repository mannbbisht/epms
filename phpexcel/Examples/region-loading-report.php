<?php

/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt    LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
// error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once '../Classes/PHPExcel.php';


// Create new PHPExcel object
// echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();

// SET FONT SIZE

$objPHPExcel->getActiveSheet()->getStyle("A1:Z1")->getFont()->setSize(10);


// Set document properties
// echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                             ->setLastModifiedBy("Maarten Balliauw")
                             ->setTitle("PHPExcel Test Document")
                             ->setSubject("PHPExcel Test Document")
                             ->setDescription("Test document for PHPExcel, generated using PHP classes.")
                             ->setKeywords("office PHPExcel php")
                             ->setCategory("Test result file");


// Add some data
// echo date('H:i:s') , " Add some data" , EOL;


// LAST 7 MONTHS
for ($i=5; $i >= 0; $i--) { 

    $last_7_months_numeric[] = date("m-Y", strtotime("-".$i."months"));
    $last_7_months[] = date("F Y", strtotime("-".$i."months"));

}


$sql_fetch_location = mysql_query("SELECT * FROM `$gd`.`location_dropdown` where country != 'India'  and country != 'Nuremberg' order by field(country, 'Quincy', 'UK', 'Finland', 'Parma', 'Bad Neustadt', 'Simmern', 'Schopfheim', 'Nash-Nuremberg', 'Nash-Schopfheim', 'Bentleyville', 'Sheboygan', 'Furstenfeldbruck', 'Frankfurt', 'Training') ", $connect_db);
// $loc_count = 1;
// $total_countries = mysql_num_rows($sql_fetch_location);
while ($row_fetch_location = (mysql_fetch_array($sql_fetch_location)) ){
extract($row_fetch_location);

    $all_countries[] = $country;

}

array_push($all_countries, "Training");
// $objPHPExcel->getActiveSheet()->mergeCells('B1:F1');



// Add some data


$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A"."1", "Country");

$head = "B";

foreach ($last_7_months as $key => $value) {


    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("$head"."1", "$value");
            
    $head++;

}





$body = "A";
$a = 2;

                                        foreach ($all_countries as $country_id => $country_name) {

                                            foreach ($last_7_months_numeric as $key1 => $month_name) {

     
                                                if($country_name != 'Training') {
                                                    $sql_fetch_project_count = mysql_query(" SELECT  sum(working_hours) as total_working_hours FROM `$gd`.`activity` where  country = '$country_name' and date_year = '$month_name'    ", $connect_db);
                                                } else {
                                                    $sql_fetch_project_count = mysql_query(" SELECT  sum(working_hours) as total_working_hours FROM `$gd`.`activity` where  order_number = '$country_name' and month_acronym = '$month_name'   ", $connect_db);
                                                }

                                                $row_fetch_project_count = mysql_fetch_array($sql_fetch_project_count);
                                                $working_hours = $row_fetch_project_count['total_working_hours'];



                                                $sql_fetch_actual_hours = mysql_query(" SELECT actual_hours,training  FROM `$gd`.`resource_utilization_data` where   month = '$month_name'  ", $connect_db);

                                                $row_fetch_actual_hours = mysql_fetch_array($sql_fetch_actual_hours);
                                                $actual_hours = $row_fetch_actual_hours['actual_hours'];
                                                $training_hours = $row_fetch_actual_hours['training'];

                                                if($country_name != "Training") {
                                                    $count_percent = round(number_format($working_hours * 100 / $actual_hours, 2)) . " %";
                                                } else {
                                                    $count_percent = round(number_format($training_hours * 100 / $actual_hours, 2)) . " %";
                                                }


                                                if($key1 == 0) {
                                                    $country_wise_count_1 = $count_percent;
                                                } else if ($key1 == 1) {
                                                    $country_wise_count_2 = $count_percent;
                                                } else if ($key1 == 2) {
                                                    $country_wise_count_3 = $count_percent;
                                                } else if ($key1 == 3) {
                                                    $country_wise_count_4 = $count_percent;
                                                } else if ($key1 == 4) {
                                                    $country_wise_count_5 = $count_percent;
                                                } else if ($key1 == 5) {
                                                    $country_wise_count_6 = $count_percent;
                                                } else if ($key1 == 6) {
                                                    $country_wise_count_7 = $count_percent;
                                                }


                                            }




                                            $objPHPExcel->setActiveSheetIndex(0)
                                                ->setCellValue("A"."$a", "$country_name") // countries

                                                ->setCellValue("B"."$a", "$country_wise_count_1")
                                                ->setCellValue("C"."$a", "$country_wise_count_2")
                                                ->setCellValue("D"."$a", "$country_wise_count_3")
                                                ->setCellValue("E"."$a", "$country_wise_count_4")
                                                ->setCellValue("F"."$a", "$country_wise_count_5")
                                                ->setCellValue("G"."$a", "$country_wise_count_6")
                                                ->setCellValue("H"."$a", "$country_wise_count_7");


                                            $body++;                                                    
                                            $a++;



                                    }




$objPHPExcel->getActiveSheet()->setTitle('Simple');



$objPHPExcel->setActiveSheetIndex(0);



$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$path = 'Region Loading Report.xls';
$objWriter->save($path);


$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;


?>

<script type="text/javascript">
location.href = '<?php echo $path; ?>';

setTimeout(function() {
    window.close();
}, 1000);

</script>