<?php

/** Error reporting */
// error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once '../Classes/PHPExcel.php';


// Create new PHPExcel object
// echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();

// SET FONT SIZE

$objPHPExcel->getActiveSheet()->getStyle("A1:Z1")->getFont()->setSize(10);


// Set document properties
// echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                             ->setLastModifiedBy("Maarten Balliauw")
                             ->setTitle("PHPExcel Test Document")
                             ->setSubject("PHPExcel Test Document")
                             ->setDescription("Test document for PHPExcel, generated using PHP classes.")
                             ->setKeywords("office PHPExcel php")
                             ->setCategory("Test result file");


// Add some data
// echo date('H:i:s') , " Add some data" , EOL;



// Add some data
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A1", "Public Holiday")
    ->setCellValue("B1", "Date")
    ->setCellValue("A2", "Kite Festival")
    ->setCellValue("B2", "14-01-2020")
    ->setCellValue("A3", "Holi")
    ->setCellValue("B3", "14-01-2020");







$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$path = 'Public Holidays Sample.xls';
$objWriter->save($path);


$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;


?>

<script type="text/javascript">
location.href = '<?php echo $path; ?>';

setTimeout(function() {
    window.close();
}, 1000);

</script>