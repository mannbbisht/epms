<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>

<style type="text/css">
.error{ color: red; }
textarea{height: 35px;resize: none;}
</style>

<!-- MULTI SELECT CSS -->
<link rel="stylesheet" href="vendor/select2/dist/css/select2.min.css">

<?php

$last12months = GetLast12MonthsName();


        // $training_hours = 0;

$sql_fetch_project_hours = mysql_query("SELECT * FROM `$gd`.`activity`  where date_year = '$this_month'  ", $connect_db);
while ($row_fetch_project_hours = (mysql_fetch_array($sql_fetch_project_hours)) ){
    extract($row_fetch_project_hours);

    if($order_number != 'Leave' and $order_number != 'Training') {

        if($country != 'India') {
            $project_hours += $working_hours;
        }

    }


    if($order_number == "Training"){

        $training_hours += $working_hours;

    }   


}

$sql_insert_new_month_data = mysql_query("SELECT * FROM `$gd`.`resource_utilization_data`  where month = '$this_month' ", $connect_db);

if(mysql_num_rows($sql_insert_new_month_data) < 1) {

    $sql_insert_data = "INSERT into  `$gd`.`resource_utilization_data` (month,project_hours,training,simple_date) VALUES ('$this_month','$project_hours','$training_hours','$date_to_year')  ";
    mysql_query($sql_insert_data, $connect_db) ;

} else {

    $sql_update_record = "UPDATE `$gd`.`resource_utilization_data` set  project_hours = '$project_hours',   updated_on = '$india_time' where month = '$this_month'  ";
    mysql_query($sql_update_record, $connect_db) or die(mysql_error());

}



?> 
            <div class="site-content">
                <!-- Content -->
                <div class="content-area py-1">
                    <div class="container-fluid">
                        <h4> ESG Resource Utilization Report  </h4>
                        <ol class="breadcrumb no-bg mb-1">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <!-- <li class="breadcrumb-item"><a href="#">Forms</a></li> -->
                            <li class="breadcrumb-item active"> ESG Resource Utilization Report </li>

                        </ol>


                        <div id="animated_image"></div>
                        <div id="result_container"></div>


                        <nav class="box box-block bg-white" style="height: 60px;">
                            <div class="col-sm-1 float-right">
                                <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#ModifyReport" style="float: right;"> <i class="fa fa-plus"></i> &nbsp; Modify Report</button>
                            </div>
                        </nav>                            


                        <div class="box box-block bg-white">


                        <?php

                            $sql_fetch_all_data = mysql_query("SELECT * FROM `$gd`.`resource_utilization_data` order by STR_TO_DATE(simple_date, '%d/%m/%Y')  ", $connect_db);
                            while ($row_fetch_all_data = (mysql_fetch_array($sql_fetch_all_data)) ){
                                extract($row_fetch_all_data);

                                $simple_date = str_replace("/", "-", $simple_date);


                                $month_names[] = date("F Y", strtotime($simple_date)); //May

                                $project_and_training = $project_hours + $training;
                                $count[] = number_format($project_and_training * 100 / $actual_hours, 2);

                            }

                            $all_month_names = implode("', '", $month_names);
                            $project_count = implode(",", $count);


                        ?>


                        <br/><br/>

                        <div id="container"></div>

                        </div>
                    </div>
                </div>


<?php include $backend_footer_file; ?>

<!-- validation library -->    
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>


<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="http://code.highcharts.com/modules/offline-exporting.js"></script>
<script src="http://highcharts.github.io/export-csv/export-csv.js"></script>


<!-- multiple select js -->
<script type="text/javascript" src="vendor/select2/dist/js/select2.min.js"></script>


<script type="text/javascript">
    $('[data-plugin="select2"]').select2($(this).attr('data-options'));
</script>


<script type="text/javascript">
var chart = Highcharts.chart('container', {

 
    title: {
        text: 'ESG Resource Utilization'
    },

    
    // exporting: {
    //     url: "212.227.255.63"
    // },

    subtitle: {
        text: ''
    },

    xAxis: {
        categories: ['<?php echo $all_month_names; ?>'],
    },


    yAxis: {
        min: 10,
        max: 150,
        tickInterval: 20,
        title: {
            text: "% Loaded"
        }

    },
    
    credits: {
        enabled: false
    },

    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b>%<br/>',
        shared: true
    },

    exporting: {
        csv: {
            columnHeaderFormatter: function(item, key) {
                if (item instanceof Highcharts.Series) {
                    return 'Count'
                }

                return 'Month'
            }
        }
    },


    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                inside: false
            }
        }
    },    

    series: [{
        type: 'column',
        // colorByPoint: true,
        name: 'Count',
        data: [<?php echo $project_count; ?>],
        showInLegend: false,
        dataLabels: [{
            align: 'left',
            format: '{point.y} %'
        }]

    }]

});
</script>




<?php
$sql_fetch_values = mysql_query("SELECT * FROM `$gd`.`resource_utilization_data`  where month = '$this_month' ", $connect_db);
while ($row_fetch_values = (mysql_fetch_array($sql_fetch_values)) ){
extract($row_fetch_values);   
    $training_hours = $training;
}
?>


<!-- Modal -->
<div id="ModifyReport" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

        <div id="animated_image_modal"></div>

                <form id="esg_report_form" name="esg_report_form" class="form-material material-primary">

                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel"> Modify Report  </h4>

                    </div>

                    <div class="modal-body">


                        <div class="form-group">
                            <div class="row">
                                                                   
                                <div class="col-sm-6">
                                    <label class="control-label"> Month:  </label>
                                        <select name="report_month" class="form-control"   id="media" style="height: 40px;">

                                            <?php AllMonths_YTD_Dropdown(); ?>

                                        </select>
                                </div>

                                <div class="col-sm-6"> </div>
                                                                    
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="row">
                                                                   
                                <div class="col-sm-6">

                                    <label for="" class="col-form-label"> Working Days: </label>
                                    <div class="">
                                        <input type="number" class="form-control" id="working_days" name="working_days" value="<?php echo $working_days; ?>" required>
                                    </div>

                                </div>

                                <div class="col-sm-6"> 

                                                <label for="" class="col-form-label"> Comments: </label>
                                                <div class="">
                                                    <textarea rows="1"  class="form-control" id="update_comments_working_days" name="update_comments_working_days" placeholder="Comments for working days"> <?php echo $working_days_comments; ?> </textarea>
                                                </div>

                                </div>
                                                                    
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="row">
                                                                   
                                <div class="col-sm-6">

                                    <label for="" class="col-form-label"> Hours per day: </label>
                                    <div class="">
                                        <input type="number" class="form-control" id="hours_per_day" name="hours_per_day" value="<?php echo $total_hours_eachday; ?>" required>
                                    </div>

                                </div>

                                <div class="col-sm-6"> 

                                                <label for="" class="col-form-label"> Comments: </label>
                                                <div class="">
                                                    <textarea rows="1"  class="form-control" id="update_comments_hours_per_day" name="update_comments_hours_per_day" placeholder="Comments for hours per day"> <?php echo $total_hours_eachday_comments; ?> </textarea>
                                                </div>

                                </div>
                                                                    
                            </div>
                        </div>








                        <div class="form-group">
                            <div class="row">
                                                                   
                                <div class="col-sm-6">

                                            <label for="" class="col-form-label"> Head Count: </label>
                                            <div class="">
                                                <input type="number" class="form-control" id="head_count" name="head_count" value="<?php echo $head_count; ?>" >
                                            </div>


                                </div>

                                <div class="col-sm-6"> 
                                                <label for="" class="col-form-label"> Comments: </label>
                                                <div class="">
                                                    <textarea rows="1"  class="form-control" id="update_comments_headcount" name="update_comments_headcount" placeholder="Comments for head count"><?php echo $head_count_comments; ?></textarea>
                                                </div>

                                </div>
                                                                    
                            </div>
                        </div>






                        <div class="form-group">
                            <div class="row">
                                                                   
                                <div class="col-sm-6">

                                    <label for="" class="col-form-label"> Hours available: </label>
                                    <div class="">
                                        <input type="number" class="form-control" id="hours_available" name="hours_available" value="<?php echo $hours_available; ?>" required>
                                    </div>

                                </div>

                                <div class="col-sm-6"> 

                                                <label for="" class="col-form-label"> Comments: </label>
                                                <div class="">
                                                    <textarea rows="1"  class="form-control" id="update_comments_hours_available" name="update_comments_hours_available" placeholder="Comments for hours available"> <?php echo $hours_available; ?> </textarea>
                                                </div>

                                </div>
                                                                    
                            </div>
                        </div>






                        <div class="form-group">
                            <div class="row">
                                                                   
                                <div class="col-sm-6">

                                            <label for="" class="col-form-label"> Leave Hours: </label>
                                            <div class="">
                                                <input type="number" class="form-control" id="leave_hours" value="<?php echo $leave_hours; ?>" name="leave_hours">
                                            </div>

                                </div>

                                <div class="col-sm-6"> 

                                                <label for="" class="col-form-label"> Comments: </label>
                                                <div class="">
                                                    <textarea rows="1" class="form-control" id="update_comments_leave_hours" name="update_comments_leave_hours"  placeholder="Comments for leave Hours"><?php echo $leave_hours_comments; ?></textarea>
                                                </div>

                                </div>
                                                                    
                            </div>
                        </div>






                        <div class="form-group">
                            <div class="row">
                                                                   
                                <div class="col-sm-6">

                                            <label for="" class="col-form-label"> Add Hours: </label>
                                            <div class="">
                                                <input type="number" class="form-control" id="add_hours" value="<?php echo $add_hours; ?>" name="add_hours">
                                            </div>

                                </div>

                                <div class="col-sm-6"> 

                                                <label for="" class="col-form-label"> Comments: </label>
                                                <div class="">
                                                    <textarea rows="1" class="form-control" id="update_comments_add_hours" name="update_comments_add_hours"  placeholder="Comments for Add Hours"><?php echo $add_hours_comments; ?></textarea>
                                                </div>

                                </div>
                                                                    
                            </div>
                        </div>










                        <div class="form-group">
                            <div class="row">
                                                                   
                                <div class="col-sm-6">

                                            <label for="" class="col-form-label"> Subtract Hours: </label>
                                            <div class="">
                                                <input type="number" class="form-control" id="subtract_hours" value="<?php echo $subtract_hours; ?>" name="subtract_hours">
                                            </div>

                                </div>

                                <div class="col-sm-6"> 

                                                <label for="" class="col-form-label"> Comments: </label>
                                                <div class="">
                                                    <textarea rows="1" class="form-control" id="update_comments_subtract_hours" name="update_comments_subtract_hours"  placeholder="Comments for Subtract Hours"><?php echo $subtract_hours_comments; ?></textarea>
                                                </div>

                                </div>
                                                                    
                            </div>
                        </div>                        






                        <div class="form-group">
                            <div class="row">
                                                                   
                                <div class="col-sm-6">


                                            <label for="" class="col-form-label"> Actual Hours: </label>
                                            <div class="">
                                                <input type="number" class="form-control" id="actual_hours" name="actual_hours" value="<?php echo $actual_hours; ?>" >
                                            </div>

                                </div>

                                <div class="col-sm-6"> 
                                                <label for="" class="col-form-label"> Comments: </label>
                                                <div class="">
                                                    <textarea rows="1" class="form-control" id="update_comments_actual_hours" name="update_comments_actual_hours"  placeholder="Comments for actual hours"><?php echo $actual_hours_comments; ?></textarea>
                                                </div>

                                </div>
                                                                    
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="row">
                                                                   
                                <div class="col-sm-6">

                                            <label for="" class="col-form-label"> Project Hours: </label>
                                            <div class="">
                                                <input type="number" class="form-control" id="project_hours" name="project_hours" value="<?php echo $project_hours; ?>" >
                                            </div>

                                </div>

                                <div class="col-sm-6">

                                                <label for="" class="col-form-label"> Comments: </label>
                                                <div class="">
                                                    <textarea rows="1" class="form-control" id="update_comments_project_hours" name="update_comments_project_hours" placeholder="Comments for project hours"><?php echo $project_hours_comments; ?></textarea>
                                                </div>

                                </div>
                                                                    
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="row">
                                                                   
                                <div class="col-sm-6">

                                            <label for="" class="col-form-label"> Training Hours: </label>
                                            <div class="">
                                                <input type="number" class="form-control" id="training_hours" name="training_hours" value="<?php echo $training_hours; ?>" >
                                            </div>


                                </div>

                                <div class="col-sm-6"> 

                                                <label for="" class="col-form-label"> Comments: </label>
                                                <div class="">
                                                    <textarea rows="1" class="form-control" id="update_comments_training_hours" name="update_comments_training_hours" placeholder="Comments for training hours"><?php echo $training_hours_comments; ?></textarea>
                                                </div>

                                </div>
                                                                    
                            </div>
                        </div>
                    </div>




                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </form>




    </div>

  </div>
</div>




<script>   
$(document).ready(function(){
  $("#esg_report_form").validate({
    debug: false,
    submitHandler: function(form) {

        $("#ModifyReport").modal('hide');

        scrollToTop();

        $("#animated_image").show();
        $("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

        $.post('<?php echo $file_path[31]; ?>', $("#esg_report_form").serialize() + '&updateESGFormValues=updateESGFormValues', function(response) {

            $("#animated_image").hide();
            $("#result_container").html(response);

        }).done(function(){

            setTimeout(function(){
                RefreshPage();
            },1000);

        }); // END OF POST REQUEST 


    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>




<script type="text/javascript">
$('#media').change(function(){

    var sel_month = $(this).val();

    if(sel_month == "07-2019") {
        var sel_date = "07/01/2019";
    } else if(sel_month == "06-2019") {
        var sel_date = "06/01/2019";
    } else if(sel_month == "05-2019") {
        var sel_date = "05/01/2019";
    } else if (sel_month == "04-2019") {
        var sel_date = "04/01/2019";
    } else if (sel_month == "03-2019") {
        var sel_date = "03/01/2019";
    } else if (sel_month == "02-2019") {
        var sel_date = "02/01/2019";
    } else if (sel_month == "01-2019") {
        var sel_date = "01/01/2019";
    }
    var sel_date = new Date(sel_date);
    // console.log(sel_date);

    // GET CURRENT DATE IN SPECIFIC FORMAT    
    var today_date = new Date();
    var month1 = today_date.getMonth();
    var date1 = "0"+today_date.getDate();
    var year1 = today_date.getFullYear();
    var today_date = month1 + "/" + date1 + "/" + year1;   
    var today_date = new Date(today_date);
    // console.log(today_date);


    const diffTime = Math.abs(today_date.getTime() - sel_date.getTime());
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
    // console.log(diffDays);    

    if(diffDays >= 15) {
        $("#esg_report_form input, #esg_report_form textarea").attr("readonly", true);
    } else {
        $("#esg_report_form input, #esg_report_form textarea").attr("readonly", false);
    }

    $.post("php/get_value.php", '&esg_values=esg_values' + '&sel_month='+sel_month , function(response1){

        // $('#esg_report_form').find("input[type=text]").val("");

        var parse_values = JSON.parse(response1);

        $("#working_days").val(parse_values.working_days);
        $("#head_count").val(parse_values.head_count);
        $("#actual_hours").val(parse_values.actual_hours);
        $("#leave_hours").val(parse_values.leave_hours);
        $("#project_hours").val(parse_values.project_hours);
        $("#training_hours").val(parse_values.training);
        $("#hours_per_day").val(parse_values.total_hours_eachday);
        $("#hours_available").val(parse_values.hours_available);
        $("#add_hours").val(parse_values.add_hours);
        $("#subtract_hours").val(parse_values.subtract_hours);

        $("#update_comments_working_days").val(parse_values.working_days_comments);
        $("#update_comments_headcount").val(parse_values.head_count_comments);
        $("#update_comments_actual_hours").val(parse_values.actual_hours_comments);
        $("#update_comments_leave_hours").val(parse_values.leave_hours_comments);
        $("#update_comments_project_hours").val(parse_values.project_hours_comments);
        $("#update_comments_training_hours").val(parse_values.training_hours_comments);
        $("#update_comments_hours_per_day").val(parse_values.hours_per_day_comments);
        $("#update_comments_hours_available").val(parse_values.hours_available_comments);
        $("#update_comments_add_hours").val(parse_values.add_hours_comments);
        $("#update_comments_subtract_hours").val(parse_values.subtract_hours_comments);


    });

});
</script>







