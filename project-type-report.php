<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>

<style type="text/css">
.error{ color: red; }
</style>

<!-- MULTI SELECT CSS -->
<link rel="stylesheet" href="vendor/select2/dist/css/select2.min.css">


<?php /********************************************* APPLY FILTER ON PAGE SUBMIT *********************************************/ ?>

<?php

if(isset($_POST['submit'])) {

    $sel_month = $_POST['filter_month'];

} else {

    $sel_month[] = "$this_month";

}

?>

<?php

$arr_month = implode(",", $sel_month);
  
$sql_fetch_project_type = mysql_query("SELECT * FROM `$gd`.`project_type_dropdown`  ", $connect_db);

?>

            <div class="site-content">
                <!-- Content -->
                <div class="content-area py-1">
                    <div class="container-fluid">
                        <h4> Project Type Report </h4>
                        <ol class="breadcrumb no-bg mb-1">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <!-- <li class="breadcrumb-item"><a href="#">Forms</a></li> -->
                            <li class="breadcrumb-item active"> Project Type Report  </li>
                        </ol>

                        <?php if(!empty($_POST)) { ?>

                        <div class="alert alert-success-fill alert-dismissible fade in">

                            <?php 

                                $message = "You have selected: ";

                                if(!empty($sel_month) && $sel_month != "0all"  ) {
                                    // $message .= "Month: " . date("F", mktime(0, 0, 0, $all_months, 10));

                                    $message .= "Month: " ;

                                    foreach ($sel_month as $key => $value) {
                                        $message .= date("F", mktime(0, 0, 0, $sel_month[$key], 10));
        
                                        $count = count($sel_month);
                                        $count = $count - 1;
                                        
                                        if($key < $count) {
                                            $message .= ", ";
                                        }


                                    }

                                    $showComma = 1;

                                } 



                                echo $message;

                            ?>

                        </div>

                        <?php } ?>

                        <nav class="box box-block bg-white">
                            <h5 class="mb-1"> Filters </h5>

                            <form id="filter_option_form" name="filter_option_form" method="post">
                                <div class="row">

                                    <div class="col-md-2">
                                        <select id="select2-demo-1" name="filter_month[]" class="form-control project_division" data-plugin="select2" data-placeholder="Select Month" required>

                                            <?php echo Last12Months_Dropdown(); ?>

                                        </select>
                                    </div>

                                    <div class="col-sm-1">
                                        <input type="submit" name="submit" class="btn btn-primary" value="Submit">

                                        <!-- <input type="submit" class="btn btn-primary" id="reset_filters" name="reset_filters" value="Reset" />  -->

                                    </div>


                                </div>
                            </form>
                        </nav>


                        <div id="animated_image"></div>
                        <div id="result_container"></div>

                        <div class="box box-block bg-white">


                        <?php

                            $count = array();
                            $sql_fetch_project_type = mysql_query("SELECT * FROM `$gd`.`project_type_dropdown` ", $connect_db);
                                $i=0;
                            while ($row_fetch_project_type = (mysql_fetch_array($sql_fetch_project_type)) ){
                                extract($row_fetch_project_type);

                                $sql_fetch_working_hours = mysql_query("SELECT sum(working_hours) as total1 
                                 FROM `$gd`.`activity` where  project_type = '$project_type' and date_year IN ('$arr_month')  ", $connect_db);
                                while ($row_fetch_working_hours = (mysql_fetch_array($sql_fetch_working_hours)) ){
                                    extract($row_fetch_working_hours);

                                    $type[] = $project_type;
                                    $project_hours1 = $total1;
                                }

                                // echo " $type[$i] $project_hours1 <br/>";


                                $sql_fetch_actual_hours = mysql_query("SELECT * FROM `$gd`.`resource_utilization_data` where month  in ('$arr_month')  ", $connect_db);
                                while ($row_fetch_actual_hours = (mysql_fetch_array($sql_fetch_actual_hours)) ){
                                    extract($row_fetch_actual_hours);
                                    $actual_hours = $actual_hours;
                                }

                                $number = $project_hours1*100/$actual_hours;
                                $count[] = $number;

                                    $i++;

                            }


                            $a = array_combine($type, $count);

                            // echo print_r($a);


                        ?>


                        <!-- <button class="btn btn-primary downloadBtn" style="float: right;">  </button> -->

                        <br/><br/>

                        <?php if(empty($actual_hours)) { ?>
                            <p> Please update actual hours for selected month. </p>
                        <?php } ?>


                        <div id="container"></div>
<!--                         <button id="plain">Plain</button>
                        <button id="inverted">Inverted</button>
                        <button id="polar">Polar</button>
 -->

                        </div>
                    </div>
                </div>


<?php include $backend_footer_file; ?>



<script type="text/javascript" src="vendor/select2/dist/js/select2.min.js"></script>


<!-- 
<script>
  init({
    title: 'selected/disabled options',
    desc: 'Multiple Select can take from selected and disabled options',
    links: ['multiple-select.css'],
    scripts: ['multiple-select.js']
  })
</script>

 -->

<script>
  // Here the mounted function is same as $(function() {})
$(document).ready(function(){
    $('select').multipleSelect({
        filter: true
    })
});
</script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="http://code.highcharts.com/modules/offline-exporting.js"></script>
<script src="http://highcharts.github.io/export-csv/export-csv.js"></script>



<script type="text/javascript">

var chart = Highcharts.chart('container', {

    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ' Project Type '
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },


    // series: [{
    //     name: 'Brands',
    //     colorByPoint: true,
    //     data: [{
    //         name: 'Chrome',
    //         y: 61.41,
    //         sliced: true,
    //         selected: true
    //     }, {
    //         name: 'Internet Explorer',
    //         y: 11.84
    //     }, {
    //         name: 'Firefox',
    //         y: 10.85
    //     }, {
    //         name: 'Edge',
    //         y: 4.67
    //     }, {
    //         name: 'Safari',
    //         y: 4.18
    //     }, {
    //         name: 'Sogou Explorer',
    //         y: 1.64
    //     }, {
    //         name: 'Opera',
    //         y: 1.6
    //     }, {
    //         name: 'QQ',
    //         y: 1.2
    //     }, {
    //         name: 'Other',
    //         y: 2.61
    //     }]
    // }]

    credits: {
        enabled: false
    },

    exporting: {
        csv: {
            columnHeaderFormatter: function(item, key) {
                if (item instanceof Highcharts.Series) {
                    return 'Count'
                }

                return 'Project Type'
            }
        }
    },
    
    series: [{
        name: 'Total',
        colorByPoint: true,
        data: [
            <?php 
                $total = count($a);
                $i=1;
                foreach ($a as $key => $value) { 
            ?>
                {
                    name: '<?php echo $key; ?>',
                    y: <?php echo $value; ?>
                }

                <?php 

                    if($total != $i) {
                        echo ",";
                    }
                    $i++;
                ?>


            <?php } ?>
        ]
    }]


});

</script>


<script type="text/javascript">
$(document).ready(function(){
    $(".downloadBtn").html("<i class='fa fa-download'></i> Excel ");  
});


$(document).ready(function(){
    $(".downloadBtn").click(function() {

        window.open('phpexcel/Examples/project-report-download.php');

    });
});
</script>


<?php if(!empty($_POST)) { ?>

<script type="text/javascript">
    $("#select2-demo-1").val('<?php echo $arr_month; ?>');
</script>


<?php } ?>
