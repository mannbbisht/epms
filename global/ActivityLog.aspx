﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ActivityLog.aspx.cs" Inherits="Test_GD.ActivityLog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            hi welcome<br />
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <asp:GridView ID="gvCustomers" runat="server" />
            </div>
        </div>
    </div>
</div>
<div>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"
        rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">
    </script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css" />
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">
$(function () {
    $('[id$=gvCustomers]').prepend($("<thead></thead>").append($('[id$=gvCustomers]').find("tr:first"))).DataTable({
        "responsive": true,
        "sPaginationType": "full_numbers",
    });
    });
    </script>
    <style type="text/css">
        input[type="search"]
        {
            width: 70%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857;
            color: #555;
            background-color: #FFF;
            background-image: none;
            border: 1px solid #CCC;
            border-radius: 4px;
            box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.075) inset;
            transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
        }
    </style>
</div>
    </form>
</body>
</html>
