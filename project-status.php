<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>

<style type="text/css">
.error{ color: red; }
#table{
    width: 100%; 
    overflow-x: scroll;
}
</style>

<!-- MULTI SELECT CSS -->
<link rel="stylesheet" href="vendor/select2/dist/css/select2.min.css">

<?php

$sql_fetch_location = mysql_query("SELECT * FROM `$gd`.`location_dropdown` where country  != 'India' and country != 'Nuremberg' order by field(country, 'Quincy', 'UK', 'Finland', 'Parma', 'Bad Neustadt', 'Simmern', 'Schopfheim', 'Nash-Nuremberg', 'Nash-Schopfheim', 'Bentleyville', 'Sheboygan', 'Furstenfeldbruck', 'Frankfurt') ", $connect_db);
// $loc_count = 1;
// $total_countries = mysql_num_rows($sql_fetch_location);
while ($row_fetch_location = (mysql_fetch_array($sql_fetch_location)) ){
extract($row_fetch_location);

    $all_countries[] = $country;

}


$sql_fetch_status = mysql_query("SELECT * FROM `$gd`.`project_status_dropdown` where project_status != 'Scope Clarity' order by field(project_status, 'Completed', 'In Progress', 'To be started', 'New Requirement', 'Under Customer Review', 'Proposal Under Devlpt', 'Hold', 'Opportunity Lost')   ", $connect_db);
while ($row_fetch_status = (mysql_fetch_array($sql_fetch_status)) ){
extract($row_fetch_status);

    $all_status[] = $project_status;

}

?>

            <div class="site-content">
                <!-- Content -->
                <div class="content-area py-1">
                    <div class="container-fluid">
                        <h4> Project Status YTD </h4>
                        <ol class="breadcrumb no-bg mb-1">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <!-- <li class="breadcrumb-item"><a href="#">Forms</a></li> -->
                            <li class="breadcrumb-item active"> Project Status YTD </li>


                        </ol>


                        <div id="animated_image"></div>
                        <div id="result_container"></div>



                        <div class="box box-block bg-white">

                            <div class="dt-buttons btn-group DownloadExcel">
                                <a href="#" class="btn btn-secondary"> <i class="fa fa-download"></i> Excel </a>
                            </div>

                            <br/><br/><br/>

                            <div id="container"></div>

                            <div id="table">
                                <table class="table">
                                  <thead>
                                    <tr>

                                        <th>  </th>
                                        <?php
                                            foreach ($all_countries as $key => $value) {

                                                echo "<th scope='col'> $value </th>"; 

                                            } 


                                        ?>                                   
                                    </tr>
                                  </thead>
                                  <tbody>

                                    <?php 

                                        // $array = array();
                                        foreach ($all_status as $status_name) {

                                            foreach ($all_countries as $key1 => $country_name) {

                                                $sql_fetch_project_count = mysql_query(" SELECT *  FROM `$gd`.`projects` where  location = '$country_name' and status = '$status_name'  ", $connect_db);



                                                $count1 = count($all_countries);
                                                for ($a=0; $a <= $count1 ; $a++) { 

                                                    if($a == $key1) {
                                                        $array[$a] = mysql_num_rows($sql_fetch_project_count);
                                                    }

                                                }

                                            }

                                            // print_r($array);


                                    ?>


                                    <tr>
                                        <th scope="row"> <?php echo $status_name; ?></th>

                                        <?php 

                                        $count2 = count($all_countries);
                                        for ($i=0; $i <= $count2 ; $i++) { 

                                        ?>

                                        <td> <?php echo $array[$i]; ?> </td>

                                        <?php } ?>

                                    </tr>

                                    <?php } ?>


                                  </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>


<?php include $backend_footer_file; ?>


<!-- validation library -->    
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>


<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="http://code.highcharts.com/modules/offline-exporting.js"></script>
<script src="http://highcharts.github.io/export-csv/export-csv.js"></script>


<!-- multiple select js -->
<script type="text/javascript" src="vendor/select2/dist/js/select2.min.js"></script>


<script type="text/javascript">
    $('[data-plugin="select2"]').select2($(this).attr('data-options'));
</script>


<script type="text/javascript">
var buttons = Highcharts.getOptions().exporting.buttons.contextButton.menuItems;


var chart = Highcharts.chart('container', {

 
   chart: {
        type: 'column'
    },

    title: {
        text: 'Project Status YTD'
    },


    xAxis: {
        categories: [

            <?php 

                foreach ($all_countries as $key => $value) {
                    echo "'" . $value . "',";
                }


            ?>

        ],
        crosshair: true
    },


    credits: {
        enabled: false
    },



    exporting: {
        buttons: {
            contextButton: {
                menuItems: buttons.slice(3,6)
            }
        }
    },
    

    yAxis: {
        title: {
            text: 'No. of Projects'
        }
    },


    series: 
    [


    <?php 


        $total_countries = count($all_status);
        $x = 0;
        foreach ($all_status as $country_name1) {

        // SET COLORS THAT APPEAR WITHIN IN THE STACKED BAR 

        if($country_name1 == "Nash-Nuremberg") { $color = "Red"; }
        if($country_name1 == "Bentleyville") { $color = "#FF7F50"; }
        if($country_name1 == "Sheboygan") { $color = "Yellow"; }
        if($country_name1 == "Furstenfeldbruck") { $color = "Pink"; }
        if($country_name1 == "Nuremberg") { $color = "Blue"; }
        if($country_name1 == "UK") { $color = "#2E8B57"; }
        if($country_name1 == "Quincy") { $color = "#00CED1"; }
        if($country_name1 == "Simmern") { $color = "#B0E0E6"; }
        if($country_name1 == "Schopfheim") { $color = "#483D8B"; }
        if($country_name1 == "Bad Neustadt") { $color = "#EE82EE"; }
        if($country_name1 == "Finland") { $color = "#4B0082"; }
        if($country_name1 == "Parma") { $color = "#D2691E"; }
        if($country_name1 == "Nash-Schopfheim") { $color = "#800000"; }
        if($country_name1 == "India") { $color = "#2F4F4F"; }
        if($country_name1 == "Frankfurt") { $color = "#6B8E23"; }


            foreach ($all_countries as $key => $value) {

                $sql_fetch_project_count_1 = mysql_query(" SELECT count(*) as total FROM `$gd`.`projects` where  status = '$country_name1' and location = '$value'  ", $connect_db);
                $row_fetch_project_count_1 = mysql_fetch_array($sql_fetch_project_count_1);

                $final_count[] = $row_fetch_project_count_1['total'];

            }

        ?>

            {

                name: "<?php echo $country_name1; ?>",
                color: "<?php echo $color; ?>",
                data: [ <?php echo implode(", ", $final_count); ?> ]

            } 


        <?php

            unset($final_count);
            // unset($color);

            if($x <= $total_countries ) { echo ","; }


        } // end of $sql_fetch_location


        ?>

    ]

});
</script>





<script>   
$(document).ready(function(){
  $("#esg_report_form").validate({
    debug: false,
    submitHandler: function(form) {


        scrollToTop();

        $("#animated_image").show();
        $("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

        $.post('<?php echo $file_path[31]; ?>', $("#esg_report_form").serialize() + '&updateESGFormValues=updateESGFormValues', function(response) {

            $("#animated_image").hide();
            $("#result_container").html(response);

        }); // END OF POST REQUEST 


    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>



<script type="text/javascript">
$(document).ready(function(){

    // $(".toggleForm, .highcharts-legend-item").hide();

    $(".toggleForm").hide();

    $(".ToggleEditableValues").click(function(){
        $(".toggleForm").toggle("fast");
    });


});
</script>




<script type="text/javascript">
$(document).ready(function(){
    $(".DownloadExcel").click(function() {

        window.open('phpexcel/Examples/export.php?file=project-status');

    });
});
</script>