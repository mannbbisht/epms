<?php 
include "php/global_constants.php"; 
include $dynamic_links; 
?>

<title>Reset</title>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<style type="text/css">
/* Credit to bootsnipp.com for the css for the color graph */
.colorgraph {
  height: 5px;
  border-top: 0;
  background: #c4e17f;
  border-radius: 5px;
  background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
}    
.error{
  color: red;
}
</style>


<?php

/********************************************** if user is logged in then login.php and register.php shouldn't open **********************************************/ 

if (  (strpos($complete_url, 'login') == true) || (strpos($complete_url, 'reset') == true) )  {  

   if(!empty($session_employee_id)) {
      echo " <script type='text/javascript'> window.location = '$domain'; </script>";
   }

}

?>



<div class="container">

    <div class="row" style="margin-top:20px">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form id="forgot_password_form" name="forgot_password_form">
                <fieldset>

                    <h2> Reset Password </h2>

                    <hr/>

                    <div id="animated_image"></div>
                    <div id="result_container"></div>

                    <hr class="colorgraph">

                    <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" autocomplete="off" required>
                    </div>


                    <div class="form-group">
                        <input type="password" name="confirm_password" id="confirm_password" class="form-control input-lg" placeholder="Confirm Password" autocomplete="off" required>
                    </div>


                    <input type="hidden" name="email_id" value="<?php echo base64_decode($_GET['q']); ?>">

<!--                     <span class="button-checkbox">
                        <a href="" class="btn btn-link pull-right">Forgot Password?</a>
                    </span> -->

                    <hr class="colorgraph">

                    <div class="row">

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <input type="submit" class="btn btn-lg btn-primary btn-block" value="Submit" />
                        </div>

                    </div>

                    <input type="hidden" name="source" value="reset_password_by_email">

                </fieldset>
            </form>
        </div>
    </div>

</div>


<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>


<!-- validation libs -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>


<!--- new_project form validation --> 
<script>   
$(document).ready(function(){
  $("#forgot_password_form").validate({
    debug: false,
    submitHandler: function(form) {

        $("#animated_image").show();
        $("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

        $.post('<?php echo $file_path[31]; ?>', $("#forgot_password_form").serialize() + '&reset_password=reset_password', function(response) {

            $("#animated_image").hide();
            $("#result_container").html(response);

        }); // END OF POST REQUEST 


    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>
