<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>



<!-- DATEPICKER CSS -->
<link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<!-- DATATABLES CSS -->
<link rel="stylesheet" href="vendor/DataTables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.bootstrap4.min.css">


<!-- JS GRID CSS -->
<link rel="stylesheet" href="vendor/jsgrid/dist/jsgrid.css">
<link rel="stylesheet" href="vendor/jsgrid/dist/jsgrid-theme.min.css">



<style type="text/css">
.col-md-1{
    margin: 0 !important;
    width: auto !important;
}
.jsgrid-header-row>.jsgrid-header-cell{
    color: black !important;
}
td,th{
    text-align: center !important;
}
@-webkit-keyframes yellow-fade {
   0% {background: #2980B9; color: white;}
   100% {background: none; color: auto;}
}
@keyframes yellow-fade {
   0% {background: #2980B9; color: white;}
   100% {background: none; color: auto;}
}

.new_li {
   -webkit-animation: yellow-fade 2s ease-in 1;
   animation: yellow-fade 2s ease-in 1;
}
.hide{
    display: none;
}
td{
    word-wrap: break-word;    
}
</style>




<?php /********************************************* APPLY FILTER ON PAGE SUBMIT *********************************************/ ?>




<?php

if(isset($_POST['submit']) && !empty($_POST['submit'])) {


    $field_type = $_POST['field_type'];


    // SPLIT UP ELEMENTS OF ARRAY
    $field_type = implode(",", $field_type);



} else {

    $field_type = "user-creation";

} 

        
        // $sql_insert_in_filters = "INSERT into  `$gd`.`filters` (fk_employee_id,months,employee,project_type,country,order_number,added_on) VALUES ('$session_employee_id','$all_months','$all_employees','$all_types','$all_countries','$all_order_number','$india_time')  ";

        // mysql_query($sql_insert_in_filters, $connect_db);
    
    // }



?>






            <div class="site-content">
                <!-- Content -->
                <div class="content-area py-1">
                    <div class="container-fluid">
                        <h4> Add Fields </h4>
                        <ol class="breadcrumb no-bg mb-1">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active"> Add Fields </li>
                        </ol>



                        <div id="animated_image"></div>
                        <div id="result_container"></div>


                        <?php if(!empty($_POST)) { ?>

                        <div class="alert alert-success-fill alert-dismissible fade in">

                            <?php 

                                $message = "You have selected: ";
                                $selected_field = str_replace("-", " ", $field_type);
                                $selected_field = ucwords($selected_field);

                                if(!empty($selected_field)) {

	                                echo "$message $selected_field";

                            	}

                            ?>

                        </div>

                        <?php } ?>



                        <nav class="box box-block bg-white">
                            <p class="mb-1"> Select:  </p>

                            <form method="post" id="filter_options" name="filter_options">

                                <div class="row">


                                    <div class="col-md-1">
                                        <select id="field_type" name="field_type[]" class="form-control field_type" data-plugin="select2">

                                            <option value="user-creation"> User Creation </option>

                                            <option value="location" <?php if ($field_type == "location") { ?> selected <?php } ?>> Location </option>

                                            <option value="project-type" <?php if ($field_type == "project-type") { ?> selected <?php } ?>> Project Type </option>

                                            <option value="project-status" <?php if ($field_type == "project-status") { ?> selected <?php } ?>> Project Status </option>

                                            <option value="project-division" <?php if ($field_type == "project-division") { ?> selected <?php } ?>> Project Division </option>

                                        </select>
                                    </div>         



                                    <div class="col-md-2">
                                        <input type="submit" class="btn btn-primary" name="submit" value="Submit" />
                                    </div>


                                </div>


                            </form>



                        </nav>                        


                        <div class="box box-block bg-white">
<!--
                                <div class="dt-buttons btn-group">
                                    <a href="#" class="btn btn-secondary">  </a>
                                </div>
-->


                            <p class="mb-1"> </p> 
                            <div id="jsGrid-basic"></div>
                        </div>


                    </div>
                </div>



<?php include $backend_footer_file; ?>


<!-- <script type="text/javascript" src="js/tables-jsgrid.js"></script> -->
<script type="text/javascript" src="vendor/jquery-fullscreen-plugin/jquery.fullscreen-min.js"></script>
<script type="text/javascript" src="vendor/waves/waves.min.js"></script>
<script type="text/javascript" src="vendor/switchery/dist/switchery.min.js"></script>
<script type="text/javascript" src="vendor/jsgrid/dist/jsgrid.min.js"></script>
<script type="text/javascript" src="vendor/jsgrid/demos/db.js"></script>





<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>



<!-- edit modal validation -->
<script>   
$(document).ready(function() {
  $("#filter_options").validate();
});  // END OF FUNCTION 
</script>



<script type="text/javascript">
$(document).ready(function() {

    window.db = db;




    <?php
        /*********************************** JUST FOR DISPLAYING VALUE IN EXISTING ROWS ***********************************/ 

        $query_role_list =  mysql_query("SELECT distinct role FROM `$gd`.`employees` ", $connect_db);
        while($row_role_list = mysql_fetch_array($query_role_list)) {
            extract($row_role_list);

            $output_role[] = array(
                "role" => $role
            );
        }

        $query_country_list =  mysql_query("SELECT * FROM `$gd`.`location_dropdown` ", $connect_db);
        while($row_country_list = mysql_fetch_array($query_country_list)) {
            extract($row_country_list);

            $output_countries[] = array(
                "id" => $pk_location_id,
                "country" => $country
            );
        }


    ?>            

    role_list = <?php echo json_encode($output_role); ?>;
    country_list = <?php echo json_encode($output_countries); ?>;



    jsGrid.setDefaults({
        tableClass: "jsgrid-table table table-striped table-hover jsgrid_body"
    }), jsGrid.setDefaults("text", {
        _createTextBox: function() {
            return $("<input>").attr("type", "text").attr("class", "form-control input-sm")
        }
    }), jsGrid.setDefaults("number", {
        _createTextBox: function() {
            return $("<input>").attr("type", "number").attr("class", "form-control input-sm")
        }
    }), jsGrid.setDefaults("textarea", {
        _createTextBox: function() {
            return $("<input>").attr("type", "textarea").attr("class", "form-control")
        }
    }), jsGrid.setDefaults("control", {
        _createGridButton: function(cls, tooltip, clickHandler) {
            var grid = this._grid;
            return $("<button>").addClass(this.buttonClass).addClass(cls).attr({
                type: "button",
                title: tooltip
            }).on("click", function(e) {
                clickHandler(grid, e)
            })
        }
    }), jsGrid.setDefaults("select", {
        _createSelect: function() {
            var $result = $("<select>").attr("class", "form-control form-control-sm"),
                valueField = this.valueField,
                textField = this.textField,
                selectedIndex = this.selectedIndex;
            return $.each(this.items, function(index, item) {
                var value = valueField ? item[valueField] : index,
                    text = textField ? item[textField] : item,
                    $option = $("<option>").attr("value", value).text(text).appendTo($result);
                $option.prop("selected", selectedIndex === index)
            }), $result
        }
    }),

    function() {

        $("#jsGrid-basic").jsGrid({
            height: "1000px",
            width: "100%",
            filtering: false,
            selecting: true,

            editing: true,

            inserting: true,
            sorting: true,
			editing: true,
            paging: true,

            autoload: true,
            pageSize: 1000,
            insertRowLocation: "top",
            noDataContent: "No records found",
            pageButtonCount: 5,
            deleteConfirm: "Do you really want to delete this record?",
            controller: db,
            onItemEditing: function(args, item, grid){

                setTimeout(function(){
                    $(".jsgrid-cell .date").val(args.item.RecordDate);
                }, 100);

                // console.log(args.item.RecordDate);
            },
            onItemInserted: function(args) {  // on done of controller.insertItem
                // document.getElementByClassName("jsgrid-row")[0].add("mystyle");
                // $('.jsgrid-row:first').addClass('new_li');
                // $('.jsgrid-row').eq(2).addClass('new_li');
                // jQuery("#" + args.rowId, "#myGrid").addClass("new_li");

            },
            onItemUpdated: function(args) {
                // console.log(args.row);
                $(args.row).animate({backgroundColor: 'green'}, 'slow');
            },
            onOptionChanging: function() {

            },
            controller: {


                loadData: function(filter) {
                    <?php if(!empty($field_type)) { ?>

                    return $.ajax({
                        type: "GET",
                        url : "php/jsgrid_loadmore.php?url=<?php echo $field_type; ?>",
                        data: filter
                    });

                    <?php } ?> 
                },

                insertItem: function(item){
                    return $.ajax({
                        type: "POST",
                        url : "php/jsgrid_dynamic_fields.php",
                        data: item,
                        success: function() {
                            $("#jsGrid-basic").jsGrid("loadData");

                            // location.reload();
                        }
                    });
                },
                updateItem: function(item){
                    return $.ajax({
                        type: "PUT",
                        url : "php/jsgrid_dynamic_fields.php",
                        data: item,
                        success: function() {
                            $("#jsGrid-basic").jsGrid("loadData");
                            // location.reload();
                        }
                    });
                },
                deleteItem: function(item){
                    return $.ajax({
                        type: "DELETE",
                        url : "php/jsgrid_dynamic_fields.php",
                        data: item
                    });
                }

            },


            <?php if($field_type == 'user-creation') { ?>

            fields: [
                { name: "PkEmployeeId", visible: false, type: "text", width: 0 },

                { name: "FieldType", visible: false,  width: 0  },


                { 
                    name: "FullName", 
                    type: "text", 
                    title: "Full Name", 
                    validate: "required",
                    width: 50, 

                    editTemplate: function(value) { 
                        this.editControl = $("<input>").val(value);
                        update_on_enter(this.editControl);
                        return this.editControl;
                    }

                },




                { 
                    name: "EmailId", 
                    type: "text", 
                    title: "Email ID", 
                    validate: "required",
                    width: 50,

                    editTemplate: function(value) { 
                        this.editControl = $("<input>").val(value);
                        update_on_enter(this.editControl);
                        return this.editControl;
                    }

                },



                { 
                    name: "Role", 
                    type: "select", 
                    title: "Role", 
                    validate: "required",
                    width: 50, 
                    items: role_list,
                    valueField: "role",
                    textField: "role"


                },



                { 
                    type: "control"

                }




            ]


            <?php } else if($field_type == 'location') { ?>

            fields: [

                { name: "PkLocationId", visible: false, type: "text", width: 0 },


                { name: "FieldType", visible: false, type: "text" },

                { 
                    name: "Country", 
                    type: "text", 
                    title: "Country",

                    insertTemplate: function(value) { 
                        this.insertControl = $("<input>");
                        insert_on_enter(this.insertControl);
                        return this.insertControl;
                    },

                    editTemplate: function(value) { 
                        this.editControl = $("<input>").val(value);
                        update_on_enter(this.editControl);
                        return this.editControl;
                    }

                },

                { 
                    type: "control"

                }


            ]

            <?php } else if($field_type == 'project-type') { ?>

            fields: [

                { name: "PkDropdownId", visible: false, type: "text", width: 0 },


                { name: "FieldType", visible: false, type: "text" },

                { 
                    name: "ProjectType", 
                    type: "text", 
                    title: "Project Type",

                    insertTemplate: function(value) { 
                        this.insertControl = $("<input>");
                        insert_on_enter(this.insertControl);
                        return this.insertControl;
                    },

                    editTemplate: function(value) { 
                        this.editControl = $("<input>").val(value);
                        update_on_enter(this.editControl);
                        return this.editControl;
                    }

                },

                { 
                    type: "control"

                }


            ]


            <?php } else if($field_type == 'project-status') { ?>

            fields: [

                { name: "PkStatusId", visible: false, type: "text", width: 0 },


                { name: "FieldType", visible: false, type: "text" },

                { 
                    name: "ProjectStatus", 
                    type: "text", 
                    title: "Project Status",

                    insertTemplate: function(value) { 
                        this.insertControl = $("<input>");
                        insert_on_enter(this.insertControl);
                        return this.insertControl;
                    },

                    editTemplate: function(value) { 
                        this.editControl = $("<input>").val(value);
                        update_on_enter(this.editControl);
                        return this.editControl;
                    }

                },

                { 
                    type: "control"

                }


            ]



            <?php } else if($field_type == 'project-division') { ?>

            fields: [

                { name: "PkDivisionId", visible: false, type: "text", width: 0 },


                { name: "FieldType", visible: false, type: "text" },

                { 
                    name: "ProjectDivision", 
                    type: "text", 
                    title: "Project Division",

                    insertTemplate: function(value) { 
                        this.insertControl = $("<input>");
                        insert_on_enter(this.insertControl);
                        return this.insertControl;
                    },

                    editTemplate: function(value) { 
                        this.editControl = $("<input>").val(value);
                        update_on_enter(this.editControl);
                        return this.editControl;
                    }

                },

                { 
                    type: "control"

                }


            ]

            <?php } ?>

        });


    }()



}); 
</script>












<script type="text/javascript">
$(document).ready(function(){
    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel ");  
});


$(document).ready(function(){
    $(".dt-buttons a").click(function() {

        window.open('phpexcel/Examples/download-file.php?month=<?php echo $all_months; ?>&order_number=<?php echo $all_order_number; ?>&employee_name=<?php echo $all_employees; ?>&type=<?php echo $all_types; ?>&country=<?php echo $all_countries; ?>');

    });
});
</script>





<script type="text/javascript">
function insert_on_enter(field) {
  field.on("keydown", function(e) {
    if(e.keyCode === 13) {
      $("#jsGrid-basic").jsGrid("insertItem");
      $("#jsGrid-basic").jsGrid("clearInsert");
      return false;
    }
  });
}
</script>


<script type="text/javascript">
function update_on_enter(field) {
  field.on("keydown", function(e) {
    if(e.keyCode === 13) {
      $("#jsGrid-basic").jsGrid("updateItem");
      return false;
    }
  });
}    
</script>





