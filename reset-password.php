<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>

<style type="text/css">
.error{ color: red; }
</style>


			<div class="site-content">
				<!-- Content -->
				<div class="content-area py-1">
					<div class="container-fluid">
						<h4> Reset Password </h4>
						<ol class="breadcrumb no-bg mb-1">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<!-- <li class="breadcrumb-item"><a href="#">Forms</a></li> -->
							<li class="breadcrumb-item active"> Reset Password </li>
						</ol>

						<div id="animated_image"></div>
						<div id="result_container"></div>

						<div class="box box-block bg-white">
							<!-- <h5>Create new project</h5> -->
							<!-- <p class="font-90 text-muted mb-1"> Please reset your password to continue. </p> -->
							<form id="reset_form" name="reset_form">

								<div class="form-group row">
									<label for="project_name" class="col-sm-2 col-form-label"> Current Password: </label>
									<div class="col-sm-4">
										<input type="password" class="form-control" name="current_password" placeholder="Current pasword" required>
									</div>
								</div>


								<div class="form-group row">
									<label for="project_name" class="col-sm-2 col-form-label"> New Password: </label>
									<div class="col-sm-4">
										<input type="password" class="form-control" name="password" placeholder="Your new pasword" required>
									</div>
								</div>


								<div class="form-group row">
									<label for="project_name" class="col-sm-2 col-form-label"> Confirm Password: </label>
									<div class="col-sm-4">
										<input type="password" class="form-control" name="confirm_password" placeholder="Confirm pasword" required>
									</div>
								</div>

								<input type="hidden" name="email_id" value="<?php echo $session_email_id; ?>">

								<button type="submit" class="btn btn-primary w-min-sm mb-0-25 waves-effect waves-light">Submit</button>

							</form>
						</div>
					</div>
				</div>



<?php include $backend_footer_file; ?>



<!-- validation libs -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>


<!--- new_project form validation --> 
<script>   
$(document).ready(function(){
  $("#reset_form").validate({
    debug: false,
    submitHandler: function(form) {

      	$("#animated_image").show();
      	$("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

      	$.post('<?php echo $file_path[31]; ?>', $("#reset_form").serialize() + '&reset_password=reset_password', function(response) {

			$("#animated_image").hide();
			$("#result_container").html(response);

      	}); // END OF POST REQUEST 


    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>


<!-- <script type="text/javascript">
$(document).ready(function(){
	$("a").attr("href", "#");
	$("a.logout").attr("href", "logout.php");
});
</script> -->