<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>

<style type="text/css">
.error{ color: red; }
</style>

<!-- MULTI SELECT CSS -->
<link rel="stylesheet" href="vendor/select2/dist/css/select2.min.css">

<?php

$months_name = array_reverse(GetLast12MonthsName());
$months_number = array_reverse(GetLast12MonthsNumber());

$sql_fetch_location = mysql_query("SELECT * FROM `$gd`.`location_dropdown` where country != 'India'  and country != 'Nuremberg' order by field(country, 'Quincy', 'UK', 'Finland', 'Parma', 'Bad Neustadt', 'Simmern', 'Schopfheim', 'Nash-Nuremberg', 'Nash-Schopfheim', 'Bentleyville', 'Sheboygan', 'Furstenfeldbruck', 'Frankfurt', 'Training') ", $connect_db);
// $loc_count = 1;
// $total_countries = mysql_num_rows($sql_fetch_location);
while ($row_fetch_location = (mysql_fetch_array($sql_fetch_location)) ){
extract($row_fetch_location);

    $all_countries[] = $country;

}

array_push($all_countries, "Training");


?>

            <div class="site-content">
                <!-- Content -->
                <div class="content-area py-1">
                    <div class="container-fluid">
                        <h4> Region Loading Report </h4>
                        <ol class="breadcrumb no-bg mb-1">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <!-- <li class="breadcrumb-item"><a href="#">Forms</a></li> -->
                            <li class="breadcrumb-item active"> Region Loading Report </li>


                        </ol>


                        <div id="animated_image"></div>
                        <div id="result_container"></div>



                        <div class="box box-block bg-white">
                            <div class="dt-buttons btn-group DownloadExcel">
                                <a href="#" class="btn btn-secondary"> <i class="fa fa-download"></i> Excel </a>
                            </div>

                            <br/><br/>

                            <div id="container"></div>

                            <div id="table">
                                <table class="table">
                                  <thead>
                                    <tr>

                                        <th> Country </th>
                                        <?php
                                            foreach ($months_name as $key => $value) {

                                                echo "<th scope='col'> $value </th>"; 

                                            } 


                                        ?>                                   
                                    </tr>
                                  </thead>
                                  <tbody>

                                    <?php 

                                        foreach ($all_countries as $country_id => $country_name) {

                                            foreach ($months_number as $key1 => $value1) {

                                                if($country_name != 'Training') {
                                                    $sql_fetch_project_count = mysql_query(" SELECT  sum(working_hours) as total_working_hours FROM `$gd`.`activity` where  country = '$country_name' and date_year = '$value1' ", $connect_db);
                                                } else {
                                                    $sql_fetch_project_count = mysql_query(" SELECT  sum(working_hours) as total_working_hours FROM `$gd`.`activity` where  order_number = '$country_name' and date_year = '$value1'  ", $connect_db);
                                                }

                                                $row_fetch_project_count = mysql_fetch_array($sql_fetch_project_count);
                                                $working_hours = $row_fetch_project_count['total_working_hours'];



                                                $sql_fetch_actual_hours = mysql_query(" SELECT actual_hours,training  FROM `$gd`.`resource_utilization_data` where   month = '$value1'  ", $connect_db);

                                                $row_fetch_actual_hours = mysql_fetch_array($sql_fetch_actual_hours);
                                                $actual_hours = $row_fetch_actual_hours['actual_hours'];
                                                $training_hours = $row_fetch_actual_hours['training'];


                                                if($country_name != "Training") {
                                                    $count_percent = round(number_format($working_hours * 100 / $actual_hours, 2)) . " %";
                                                } else {
                                                    $count_percent = round(number_format($training_hours * 100 / $actual_hours, 2)) . " %";
                                                }


                                                if($key1 == 0) {
                                                    $country_wise_count_1 = $count_percent;
                                                } else if ($key1 == 1) {
                                                    $country_wise_count_2 = $count_percent;
                                                } else if ($key1 == 2) {
                                                    $country_wise_count_3 = $count_percent;
                                                } else if ($key1 == 3) {
                                                    $country_wise_count_4 = $count_percent;
                                                } else if ($key1 == 4) {
                                                    $country_wise_count_5 = $count_percent;
                                                } else if ($key1 == 5) {
                                                    $country_wise_count_6 = $count_percent;
                                                } else if ($key1 == 6) {
                                                    $country_wise_count_7 = $count_percent;
                                                } else if ($key1 == 7) {
                                                    $country_wise_count_8 = $count_percent;
                                                } else if ($key1 == 8) {
                                                    $country_wise_count_9 = $count_percent;
                                                } else if ($key1 == 9) {
                                                    $country_wise_count_10 = $count_percent;
                                                } else if ($key1 == 10) {
                                                    $country_wise_count_11 = $count_percent;
                                                }



                                            }

                                    ?>


                                    <tr>
                                        <th scope="row"> <?php echo $country_name; ?></th>
                                        <td> <?php echo $country_wise_count_1; ?> </td>
                                        <td> <?php echo $country_wise_count_2; ?> </td>
                                        <td> <?php echo $country_wise_count_3; ?> </td>
                                        <td> <?php echo $country_wise_count_4; ?> </td>
                                        <td> <?php echo $country_wise_count_5; ?> </td>
                                        <td> <?php echo $country_wise_count_6; ?> </td>
                                        <td> <?php echo $country_wise_count_7; ?> </td>
                                        <td> <?php echo $country_wise_count_8; ?> </td>
                                        <td> <?php echo $country_wise_count_9; ?> </td>
                                        <td> <?php echo $country_wise_count_10; ?> </td>
                                        <td> <?php echo $country_wise_count_11; ?> </td>
                                    </tr>

                                    <?php } ?>


                                  </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>


<?php include $backend_footer_file; ?>


<!-- validation library -->    
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>


<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="http://code.highcharts.com/modules/offline-exporting.js"></script>
<script src="http://highcharts.github.io/export-csv/export-csv.js"></script>


<!-- multiple select js -->
<script type="text/javascript" src="vendor/select2/dist/js/select2.min.js"></script>


<script type="text/javascript">
    $('[data-plugin="select2"]').select2($(this).attr('data-options'));
</script>


<script type="text/javascript">

var buttons = Highcharts.getOptions().exporting.buttons.contextButton.menuItems;
    
var chart = Highcharts.chart('container', {

 
   chart: {
        type: 'column'
    },

    title: {
        text: 'Region wise Loading'
    },


    xAxis: {
        categories: [

            <?php 

                foreach ($months_name as $key => $value) {
                    echo "'" . $value . "',";
                }


            ?>

        ],
        crosshair: true
    },

    credits: {
        enabled: false
    },

    yAxis: {
        title: {
            text: '% Loaded'
        }
    },

    tooltip: { enabled: false },



    exporting: {
        buttons: {
            contextButton: {
                menuItems: buttons.slice(3,6)
            }
        }
    },


    series: 
    [


    <?php 


        $total_countries = count($all_countries);
        $x = 0;
        foreach ($all_countries as $country_name1) {

        // SET COLORS THAT APPEAR WITHIN IN THE STACKED BAR 

        if($country_name1 == "Nash-Nuremberg") { $color = "Red"; }
        if($country_name1 == "Bentleyville") { $color = "#FF7F50"; }
        if($country_name1 == "Sheboygan") { $color = "#808080"; }
        if($country_name1 == "Furstenfeldbruck") { $color = "Pink"; }
        if($country_name1 == "Nuremberg") { $color = "Blue"; }
        if($country_name1 == "UK") { $color = "#2E8B57"; }
        if($country_name1 == "Quincy") { $color = "#00CED1"; }
        if($country_name1 == "Simmern") { $color = "#B0E0E6"; }
        if($country_name1 == "Schopfheim") { $color = "#483D8B"; }
        if($country_name1 == "Bad Neustadt") { $color = "#EE82EE"; }
        if($country_name1 == "Finland") { $color = "#4B0082"; }
        if($country_name1 == "Parma") { $color = "#D2691E"; }
        if($country_name1 == "Nash-Schopfheim") { $color = "#800000"; }
        if($country_name1 == "India") { $color = "#2F4F4F"; }
        if($country_name1 == "Frankfurt") { $color = "#6B8E23"; }


            foreach ($months_number as $key => $value2) {

                if($country_name1 != 'Training') {

                    $sql_fetch_project_count_2 = mysql_query(" SELECT  sum(working_hours) as total_working_hours_2 FROM `$gd`.`activity` where  country = '$country_name1' and date_year = '$value2'  ", $connect_db);

                } else {

                    $sql_fetch_project_count_2 = mysql_query(" SELECT  sum(working_hours) as total_working_hours_2 FROM `$gd`.`activity` where  order_number = '$country_name1' and date_year = '$value2'  ", $connect_db);

                }


                $row_fetch_project_count_2 = mysql_fetch_array($sql_fetch_project_count_2);
                $working_hours_2 = $row_fetch_project_count_2['total_working_hours_2'];



                $sql_fetch_actual_hours_2 = mysql_query(" SELECT actual_hours  FROM `$gd`.`resource_utilization_data` where   month = '$value2'  ", $connect_db);

                $row_fetch_actual_hours_2 = mysql_fetch_array($sql_fetch_actual_hours_2);
                $actual_hours_2 = $row_fetch_actual_hours_2['actual_hours'];

                $final_count[] = round(number_format($working_hours_2 * 100 / $actual_hours_2, 2));

            }

        ?>

            {

                name: "<?php echo $country_name1; ?>",
                color: "<?php echo $color; ?>",
                data: [ <?php echo implode(", ", $final_count); ?> ]



            } 


        <?php

            unset($final_count);
            // unset($color);

            if($x <= $total_countries ) { echo ","; }


        } // end of $sql_fetch_location


        ?>

    ]

});
</script>





<script>   
$(document).ready(function(){
  $("#esg_report_form").validate({
    debug: false,
    submitHandler: function(form) {


        scrollToTop();

        $("#animated_image").show();
        $("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

        $.post('<?php echo $file_path[31]; ?>', $("#esg_report_form").serialize() + '&updateESGFormValues=updateESGFormValues', function(response) {

            $("#animated_image").hide();
            $("#result_container").html(response);

        }); // END OF POST REQUEST 


    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM
}); // END OF DOCUMENT READY FUNCTION 
</script>



<script type="text/javascript">
$(document).ready(function(){

    // $(".toggleForm, .highcharts-legend-item").hide();

    $(".toggleForm").hide();

    $(".ToggleEditableValues").click(function(){
        $(".toggleForm").toggle("fast");
    });


});
</script>


<script type="text/javascript">
$(document).ready(function(){
    $(".DownloadExcel").click(function() {

        window.open('phpexcel/Examples/export.php?file=region-loading-report');

    });
});
</script>