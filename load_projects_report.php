<?php 
include "php/global_constants.php"; 
?>


<?php 

$month = $_POST['month_value'];

if ($month == "all") {
	$sql_fetch_projects = mysql_query("SELECT * FROM `$gd`.`projects` order by pk_project_id desc ", $connect_db);
} else {
	$sql_fetch_projects = mysql_query("SELECT * FROM `$gd`.`projects` where order_recieved_date LIKE '0$month/__/____' order by pk_project_id desc ", $connect_db);
}

?>

<table class="table table-striped table-bordered dataTable" id="table-2">
	<thead>
		<tr>
			<th> Project Number </th>
			<th> Location </th>
			<th> From	 </th>
			<th> Project Title </th>
			<th> Assign To </th>
			<th> Order Received Date </th>
			<th> Customer Want Date </th>
			<th> Actual Completion </th>
			<th> Status </th>
			<th> Description </th>
			<th> Edit </th>
			<th> Delete </th>
		</tr>
	</thead>
	<tbody>

		<?php 

		while ($row_fetch_projects = (mysql_fetch_array($sql_fetch_projects)) ){
		extract($row_fetch_projects);

		?>

		<tr>

			<td> 
				<span style="display: none;"><?php print_value($pk_project_id); ?></span> 
				<span><?php print_value($order_number); ?></span> 
			</td>
			<td> <?php print_value($location); ?> </td>
			<td> <?php print_value($request_from); ?> </td>
			<td> <?php print_value($project_name); ?> </td>
			<td> <?php print_value($assign_to); ?> </td>
			<td> <?php print_value($order_recieved_date); ?> </td>
			<td> <?php print_value($customer_want_date); ?> </td>
			<td> <?php print_value($actual_completion_date); ?> </td>
			<td> <?php print_value($status); ?> </td>
			<td> <?php print_value($description); ?> </td>
			<td> <a href="#!" onclick="load_project_details(<?php echo $pk_project_id; ?>);"> Edit </a> </td>
			<td> <a href="#!" onclick="delete_project(<?php echo $pk_project_id; ?>);"> Delete </a> </td>
		</tr>


		<?php } ?>
	</tbody>
</table>




<script type="text/javascript" src="vendor/DataTables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Responsive/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Responsive/js/responsive.bootstrap4.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.bootstrap4.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/JSZip/jszip.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/pdfmake/build/pdfmake.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/pdfmake/build/vfs_fonts.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.print.min.js"></script>
<script type="text/javascript" src="vendor/DataTables/Buttons/js/buttons.colVis.min.js"></script>


<!-- <script type="text/javascript" src="js/tables-datatable.js"></script> -->

<script type="text/javascript">
    $('#table-2').DataTable( {
        dom: 'Bfrtip',
	    "order": [[ 0, "desc" ]],
 		pageLength: 50,
        buttons: [
            // 'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            // 'pdfHtml5'
        ]
    } );
</script>