<?php 

if(isset($_POST['reset_filters'])) {
	header("Location: $domain/dashboard.php");
}


include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>

<link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<link rel="stylesheet" href="vendor/DataTables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.bootstrap4.min.css">



<!-- MULTI SELECT CSS -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.2/multiple-select.min.css">




<link rel="stylesheet" href="css/dashboard.css">

<style type="text/css">
#report_container{
	/*overflow-y: scroll !important;*/
}	
.text-center{
	text-align: center;
}
.multiselect-container>li>a>label.checkbox{
	padding: 5px 10px !important;	
}
.col-md-1{
	margin: 0 !important;
	width: auto !important;
}
.list-items li img{
	height: 100px;
}
.list-items li img{
	border-radius: 50px;
	float: right;
	height: 35px;
	float: right !important;

}
.list-title, .list-items li {
	font-size: 15px !important;
}
@-webkit-keyframes yellow-fade {
   0% {background: #2980B9; color: white;}
   100% {background: none; color: auto;}
}
@keyframes yellow-fade {
   0% {background: #2980B9; color: white;}
   100% {background: none; color: auto;}
}
.new_li {
   -webkit-animation: yellow-fade 2s ease-in 1;
   animation: yellow-fade 2s ease-in 1;
}
#filter_options select{
	width: 130px !important;
}
</style>



<?php /********************************************* APPLY FILTER ON PAGE SUBMIT *********************************************/ ?>

<?php

if(isset($_POST['submit']) && !empty($_POST['submit'])) {


	$param_month = $_POST['choose_month'];
	$param_project_num = $_POST['choose_project_number'];
	$param_emp_name = $_POST['choose_employee'];
	$param_country = $_POST['choose_country'];
	$param_type = $_POST['choose_project_type'];

	// echo "test $param_month $param_project_num";

	$all_month = implode ( ",", $param_month );

    // SPLIT UP ELEMENTS OF ARRAY IN SINGLE QUOTES TO BE USED IN MYSQL QUERY
	$arr_month = "'" . implode ( "', '", $param_month ) . "'";
	$arr_order_number = "'" . implode ( "', '", $param_project_num ) . "'";
	$arr_employees = "'%" . implode ( "%', '%", $param_emp_name ) . "'%";
	$arr_employees_1 = "'" . implode ( "', '", $param_emp_name ) . "'";
	$arr_countries = "'" . implode ( "', '", $param_country ) . "'";
	$arr_project_types = "'" . implode ( "', '", $param_type ) . "'";

    // INSERT INTO FILTERS TABLE
        
    $sql_insert_in_filters = "INSERT into  `$gd`.`filters` (fk_employee_id,months,employee,project_type,country,order_number,url,added_on) VALUES ('$session_employee_id','$all_month','$arr_employees_1','$arr_project_types','$arr_countries','$arr_order_number','$complete_url','$india_time')  ";

    mysql_query($sql_insert_in_filters, $connect_db);

} else {

	// $param_month = "$this_month";

}




if($member_role == "admin") {




	$condition;
	if(!empty($param_month)) {

		$condition = "  date_year IN ($arr_month)  ";

	} 

	if(!empty($param_project_num)) {

		if(!empty($condition)) {
			$condition .= " AND ";
		}
		$condition .= " order_number IN ($arr_order_number)  ";

	}


	if(!empty($param_emp_name)) {

		if(!empty($condition)) {
			$condition .= " AND ";
		}


		$condition .= "(";

		$i = 1;
		foreach ($param_emp_name as $key => $value) {

			$condition .= "  assign_to like '%$value%'  ";

			if($i < count($param_emp_name)) {
				$condition .= " OR ";
			}

			$i++;

		}


		$condition .= ")";

	} 



	if(!empty($param_type)) {

		if(!empty($condition)) {
			$condition .= " AND ";
		}
		$condition .= " project_type IN ($arr_project_types)  ";

	} 

	if(!empty($param_country)) {

		if(!empty($condition)) {
			$condition .= " AND ";
		}
		$condition .= " location IN ($arr_countries)  ";

	}


	if(empty($condition)){
		$condition = " order_number != '' ";
	}


	$query = "SELECT * FROM `$gd`.`projects` where  $condition  order by pk_project_id desc ";

	$sql_fetch_projects = mysql_query("  $query ", $connect_db);





} else {

 
	if(empty($param_month) && !empty($param_project_num) ) {


		if($param_project_num[0] != "all" ) {

			$sql_fetch_projects = mysql_query("SELECT * FROM `$gd`.`projects` where order_number IN ($arr_order_number) and  assign_to like '%$session_full_name%' ", $connect_db);

		} else {

			$sql_fetch_projects = mysql_query("SELECT * FROM `$gd`.`projects` where   assign_to like '%$session_full_name%' ", $connect_db);

		}



	} else if(!empty($param_month) && empty($param_project_num) ) {

		// print_r($param_month);

		if($param_month[0] != "all") {

			foreach ($param_month as $key => $value) {

				$sel_months_array = implode(",", $param_month);

			}

			$sql_fetch_projects = mysql_query("SELECT * FROM `$gd`.`projects` where  assign_to like '%$session_full_name%' and month_acronym IN ($arr_month) ", $connect_db);

			// echo "SELECT * FROM `$gd`.`projects` where  assign_to like '%$session_full_name%' and month_acronym IN ($arr_month)";

		} else {

			$sql_fetch_projects = mysql_query("SELECT * FROM `$gd`.`projects` where  assign_to like '%$session_full_name%'  ", $connect_db);

		}
 


	} else if(!empty($param_month) && !empty($param_project_num) ) {

		$quo_project_number = "'" . implode ( "', '", $param_project_num ) . "'";

		if($param_month[0] != "all" || $param_project_num[0] != "all" ) {

			foreach ($param_month as $key => $value) {

				$sel_months_array = implode(",", $param_month);

			}

			$sql_fetch_projects = mysql_query("  SELECT * FROM `$gd`.`projects` where order_number IN ($quo_project_number) AND  assign_to like '%$session_full_name%' and order_recieved_date IN ($sel_months_array) ", $connect_db);

		} else {

			$sql_fetch_projects = mysql_query("SELECT * FROM `$gd`.`projects` where  assign_to like '%$session_full_name%'  ", $connect_db);

		}
 
	} else if(empty($param_month) && empty($param_project_num) ) {

		$sql_fetch_projects = mysql_query("SELECT * FROM `$gd`.`projects` where   assign_to like '%$session_full_name%' ", $connect_db);

	}
 
}


while ($row_fetch_projects = (mysql_fetch_array($sql_fetch_projects)) ){
	extract($row_fetch_projects);

	// print_r($row_fetch_projects);


		if($status == "Scope Clarity") {
			$scope_clarity[] = $order_number;
			$project_assigned_to[] = $assign_to;
			$order_date_1[] = $order_recieved_date;
		} else if($status == "To be started") {
			$to_be_started[] = $order_number;			
			$project_assigned_to_2[] = "$assign_to";
			$order_date_2[] = $order_recieved_date;
		} else if($status == "In Progress" || $status == "In progress") {
			$in_progress[] = $order_number;			
			$project_assigned_to_3[] = "$assign_to";
			$order_date_3[] = $order_recieved_date;
		} else if($status == "Under Customer Review") {
			$under_review[] = $order_number;			
			$project_assigned_to_4[] = "$assign_to";
			$order_date_4[] = $order_recieved_date;
		} else if($status == "Completed") {


		    $month1 = substr($actual_completion_date, 3, 2); 
		    $year1 = substr($actual_completion_date, 6, 4); 

		    $completed_month_year = "$month1-$year1";

		    if($completed_month_year == $this_month) {
				$completed[] = $order_number;			
				$project_assigned_to_5[] = "$assign_to";
			}				




		}



		$scope_clear_date[] = $order_recieved_date;

		// print_r($scope_clear_date);

}

?>


			<div class="site-content">
				<!-- Content -->
				<div class="content-area py-1">
					<div class="container-fluid">
						<h4> Dashboard</h4>
						<ol class="breadcrumb no-bg mb-1">
							<li class="breadcrumb-item"><a href="#">welcomeHome</a></li>
							<li class="breadcrumb-item active">Dashboard</li>
						</ol>



						<?php if(!empty($_POST)) { ?>

						<div class="alert alert-success-fill alert-dismissible fade in">

							<?php 

								$message = "You have selected: ";

								if(!empty($param_month) && $param_month[0] != "all"  ) {

									$message .= "Month: " ;

									foreach ($param_month as $key => $value) {
										$message .= date("F", mktime(0, 0, 0, $param_month[$key], 10));
		
										$count = count($param_month);
										$count = $count - 1;
										
										if($key < $count) {
											$message .= ", ";
										}


									}

									$showComma = 1;

								} else {
									$message .= "Month: All";
								}


								if(!empty($param_project_num)) {
									if(!empty($param_month)) {
										$message .= ",";
									}
									$message .= " Order Number: " . implode(",", $param_project_num);
								}


								if(!empty($param_emp_name)) {
									if(!empty($param_project_num)) {
										$message .= ",";
									}
									$message .= " Employee: " . implode(",", $param_emp_name);
								}

								if(!empty($param_country)) {
									if(!empty($param_emp_name)) {
										$message .= ",";
									}
									if(count($param_country) > 1) {
										$message .= " Countries: " . implode(",", $param_country);
									} else {
										$message .= " Country: " . implode(",", $param_country);
									}
								}

								if(!empty($param_type)) {
									if(!empty($param_country)) {
										$message .= ",";
									}
									$message .= " Project Type: " . implode(",", $param_type);
								}


								echo $message;

							?>

						</div>

						<?php } ?>









						<?php if($member_role == "admin") { ?>

						<nav class="box box-block bg-white">
							<h5 class="mb-1"> Filters </h5>

							<form id="filter_options" name="filter_options" method="post">

	                            <div class="row">
	                                <div class="col-md-1">

										<select id="dates-field2" name="choose_month[]" data-placeholder="Select Month"  multiple>

                                            <?php echo Last12Months_Dropdown(); ?>

										</select>
									</div>


	                                <div class="col-md-1">
										<select id="dates-field2" name="choose_project_number[]" data-placeholder="Select Order Number"  multiple="multiple">
											<!-- <option value=""> -- Select Project Number -- </option> -->

												<?php 

													if($member_role == "admin") {
														$sql_fetch_project = mysql_query("SELECT * FROM `$gd`.`projects` order by pk_project_id desc ", $connect_db);
													} else {
														$sql_fetch_project = mysql_query("SELECT * FROM `$gd`.`projects` where  assign_to like '%$session_full_name%' order by pk_project_id desc ", $connect_db);
													}

													while ($row_fetch_project = mysql_fetch_array($sql_fetch_project)) {
														extract($row_fetch_project);
												?>

														<option value="<?php echo $order_number; ?>" ><?php echo $order_number; ?></option>

												<?php } // END OF WHILE ?>

										</select>

									</div>




	                                <div class="col-md-1">
										<select id="dates-field2" name="choose_employee[]" data-placeholder="Select Employee"  multiple>


											<!-- <option value=""> -- Select Employee Name -- </option> -->
											<?php 
												$sql_fetch_all_employees = mysql_query("SELECT * FROM `$gd`.`employees` order by full_name asc ", $connect_db);
												while ($row_fetch_all_employees = mysql_fetch_array($sql_fetch_all_employees)) {
													extract($row_fetch_all_employees);

												?>

												<option value="<?php echo $full_name; ?>"  ><?php echo $full_name; ?></option>

												<?php }
											?>


										</select>
									</div>


	                                <div class="col-md-1">
										<select id="dates-field2" name="choose_project_type[]" data-placeholder="Select Type"  multiple>


											<!-- <option value=""> -- Select Employee Name -- </option> -->
											<?php 
												$sql_fetch_type = mysql_query("SELECT * FROM `$gd`.`project_type_dropdown` ", $connect_db);
												while ($row_fetch_type = mysql_fetch_array($sql_fetch_type)) {
													extract($row_fetch_type);

												?>

												<option value="<?php echo $project_type; ?>"><?php echo $project_type; ?></option>

												<?php }
											?>


										</select>
									</div>

	                                <div class="col-md-1">
										<select id="dates-field2" name="choose_country[]" data-placeholder="Select Location"  multiple>


											<!-- <option value=""> -- Select Employee Name -- </option> -->
											<?php 
												$sql_fetch_all_loc = mysql_query("SELECT * FROM `$gd`.`location_dropdown`  order by country asc ", $connect_db);
												while ($row_fetch_all_loc = mysql_fetch_array($sql_fetch_all_loc)) {
													extract($row_fetch_all_loc);

												?>

												<option value="<?php echo $country; ?>"><?php echo $country; ?></option>

												<?php }
											?>


										</select>
									</div>




									<div class="col-md-2">
										<input type="submit" class="btn btn-primary" name="submit" /> 

										<!-- <input type="submit" class="btn btn-primary" id="reset_filters" name="reset_filters" value="Reset" />  -->
									</div>

								</div>

							</form>

						</nav>


						<?php } ?>


						<div id="animated_image"></div>
						<div id="result_container"></div>


						<div class="box box-block bg-white">
							<!-- <h5>Create new project</h5> -->
							<!-- <p class="font-90 text-muted mb-1">To use, add <code>.material-*</code> to the form.</p> -->

							<div id="report_container">


								<div class="row">
								<section class="lists-container">

									<div class="list">

										<h3 class="list-title"> <i class="fa fa-tags"></i> <span class="text-center"> Scope Clarity </span>  </h3>

										<ul class="list-items ul_scope_clarity">

											<?php

											$a=0;
											foreach (array_combine($scope_clarity, $project_assigned_to) as $key => $value) {

											?>  

													<li id="<?php echo $key; ?>" onclick="EditProject('<?php echo $key; ?>');">  <?php echo "$key"; ?>

													<?php

													$name1 = explode(", ", $value);
													$get_img = "'" . implode ( "', '", $name1 ) . "'";

													// echo "$get_img";

													$sql_fetch_pic = mysql_query(" SELECT * FROM `employees` where full_name in ($get_img);  ", $connect_db);
													while ($row_fetch_pic = (mysql_fetch_array($sql_fetch_pic)) ){
														extract($row_fetch_pic);


													if(empty($avatar)) {
														$avatar_1 = "img/avatars/avatar.png";
													} else {
														$avatar_1 = "img/avatars/$avatar";
													}

									                if(file_exists($avatar_1)) { 
									                	$avatar = $avatar_1; 
									                } 

													?>

<!-- 													<img src="<?php echo $avatar; ?>"  /> <br/>  													
 -->
													<img data-toggle="tooltip" title="<?php echo $full_name; ?>" src="<?php echo $avatar; ?>" alt="<?php echo $avatar; ?>"  />

													<?php } ?>		



														<?php

															$sql_fetch_scope_date = mysql_query(" SELECT * FROM   projects where order_number = '$key'   ", $connect_db);
															while ($row_fetch_scope_date = (mysql_fetch_array($sql_fetch_scope_date)) ){
																	extract($row_fetch_scope_date);

															}

														?>

														<br/>
														<span><i class="fa fa-calendar"> &nbsp; <?php echo $order_recieved_date; ?> &nbsp;</i></span> 


													</li> 

												<?php $a++; 

											} // END OF FOREACH ?> 


											<?php if(count($scope_clarity) === 0 ) {
												echo "<li class='no_result_found'> No results found </li>";
											} ?>




										</ul>

										<!-- <button class="add-card-btn btn">Add a card</button> -->

									</div>

									<div class="list">

										<h3 class="list-title"> <i class="fa fa-plane"></i> <span class="text-center"> To be started </span>  </h3>

										<ul class="list-items ul_to_be_started">

											<?php

											$b=0;
											foreach (array_combine($to_be_started, $project_assigned_to_2) as $key => $value) {
											?>  

													<li id="<?php echo $key; ?>" onclick="EditProject('<?php echo $key; ?>');">  <?php echo "$key  "; ?>  

													<?php

													$name1 = explode(", ", $value);
													$get_img = "'" . implode ( "', '", $name1 ) . "'";

													// echo "$get_img";

													$sql_fetch_pic = mysql_query(" SELECT * FROM `employees` where full_name in ($get_img);  ", $connect_db);
													while ($row_fetch_pic = (mysql_fetch_array($sql_fetch_pic)) ){
														extract($row_fetch_pic);


													if(empty($avatar)) {
														$avatar_1 = "img/avatars/avatar.png";
													} else {
														$avatar_1 = "img/avatars/$avatar";
													}

									                if(file_exists($avatar_1)) { 
									                	$avatar = $avatar_1; 
									                } 

													?>


														<img data-toggle="tooltip" title="<?php echo $full_name; ?>" src="<?php echo $avatar; ?>" alt="<?php echo $avatar; ?>"  />


													<?php } ?>		
													

													<?php

														$sql_fetch_dates_1 = mysql_query(" SELECT * FROM   project_revision where scope_cleared_date != ''  ", $connect_db);
														while ($row_fetch_dates_1 = (mysql_fetch_array($sql_fetch_dates_1)) ){
																extract($row_fetch_dates_1);


															$sql_fetch_pro_id_1 = mysql_query(" SELECT * FROM   projects where pk_project_id = '$fk_project_id' and status = 'To be started'  ", $connect_db);
															while ($row_fetch_pro_id_1 = (mysql_fetch_array($sql_fetch_pro_id_1)) ){
																	extract($row_fetch_pro_id_1);

																	if($key === $order_number) {
																		$show_scope_clarity_date = " <br/> <span><i class='fa fa-calendar'> &nbsp; $scope_cleared_date  &nbsp;</i></span> ";
																	} 
 

															}


														// print_r($dates);

													?>

														<?php echo $show_scope_clarity_date; ?>

													<?php unset($show_scope_clarity_date); } ?>

													</li>

												<?php $b++; 

											} // END OF FOREACH ?> 


											<?php if(count($to_be_started) === 0 ) {
												echo "<li class='no_result_found'> No results found </li>";
											} ?>


										</ul>

										<!-- <button class="add-card-btn btn">Add a card</button> -->

									</div>

									<div class="list">

										<h3 class="list-title"> <i class="fa fa-clock-o"></i> <span class="text-center"> In Progress </span>  </h3>

										<ul class="list-items ul_in_progress">


											<?php

											$c = 0;
											foreach (array_combine($in_progress, $project_assigned_to_3) as $key => $value) {


											?>  

													<li id="<?php echo $key; ?>" onclick="EditProject('<?php echo $key; ?>');">  <?php echo "$key  "; ?> 

													<?php

													$name1 = explode(", ", $value);
													$get_img = "'" . implode ( "', '", $name1 ) . "'";

													// echo "$get_img";

													$sql_fetch_pic = mysql_query(" SELECT * FROM `employees` where full_name in ($get_img);  ", $connect_db);
													while ($row_fetch_pic = (mysql_fetch_array($sql_fetch_pic)) ){
														extract($row_fetch_pic);


													if(empty($avatar)) {
														$avatar_1 = "img/avatars/avatar.png";
													} else {
														$avatar_1 = "img/avatars/$avatar";
													}

									                if(file_exists($avatar_1)) { 
									                	$avatar = $avatar_1; 
									                } 

													?>


														<img data-toggle="tooltip" title="<?php echo $full_name; ?>" src="<?php echo $avatar; ?>" alt="<?php echo $avatar; ?>"  />


													<?php } ?>													


													<?php

														$sql_fetch_dates_2 = mysql_query(" SELECT * FROM   project_revision where project_start_date != ''  ", $connect_db);
														while ($row_fetch_dates_2 = (mysql_fetch_array($sql_fetch_dates_2)) ){
																extract($row_fetch_dates_2);

															$commit_date = "$self_commitment_date";

															$sql_fetch_pro_id_2 = mysql_query(" SELECT * FROM   projects where pk_project_id = '$fk_project_id' and status = 'In Progress'  ", $connect_db);
															while ($row_fetch_pro_id_2 = (mysql_fetch_array($sql_fetch_pro_id_2)) ){
																	extract($row_fetch_pro_id_2);

																	if($key === $order_number) {
																		$show_in_progress_dates = "  <span><i class='fa fa-calendar'> &nbsp; $project_start_date, $commit_date  &nbsp;</i></span> ";
																	}


															}


														// print_r($dates);

													?>

														<?php echo $show_in_progress_dates; ?>

													<?php unset($show_in_progress_dates); } ?>

													</li> 

												<?php $c++; 

											} // END OF FOREACH ?> 


											<?php if(count($in_progress) === 0 ) {
												echo "<li class='no_result_found'> No results found </li>";
											} ?>

										</ul>

										<!-- <button class="add-card-btn btn">Add a card</button> -->

									</div>



									<div class="list">

										<h3 class="list-title"> <i class="fa fa-eye"></i> <span class="text-center"> Under Customer Review </span>  </h3>

										<ul class="list-items ul_under_review">

											<?php

											$d = 0;
											foreach (array_combine($under_review, $project_assigned_to_4) as $key => $value) {


											?>  

													<li id="<?php echo $key; ?>" onclick="EditProject('<?php echo $key; ?>');"> <?php echo "$key "; ?>  
 
													<?php

													$name1 = explode(", ", $value);
													$get_img = "'" . implode ( "', '", $name1 ) . "'";

													// echo "$get_img";

													$sql_fetch_pic = mysql_query(" SELECT * FROM `employees` where full_name in ($get_img);  ", $connect_db);
													while ($row_fetch_pic = (mysql_fetch_array($sql_fetch_pic)) ){
														extract($row_fetch_pic);


													if(empty($avatar)) {
														$avatar_1 = "img/avatars/avatar.png";
													} else {
														$avatar_1 = "img/avatars/$avatar";
													}

									                if(file_exists($avatar_1)) { 
									                	$avatar = $avatar_1; 
									                } 

													?>

														<img data-toggle="tooltip" title="<?php echo $full_name; ?>" src="<?php echo $avatar; ?>" alt="<?php echo $avatar; ?>"  />


													<?php } ?>													

													<?php

														$sql_fetch_dates_3 = mysql_query(" SELECT * FROM   project_revision where submission_date != ''  ", $connect_db);
														while ($row_fetch_dates_3 = (mysql_fetch_array($sql_fetch_dates_3)) ){
																extract($row_fetch_dates_3);


															$sql_fetch_pro_id_3 = mysql_query(" SELECT * FROM   projects where pk_project_id = '$fk_project_id' and status = 'Under Customer Review'  ", $connect_db);
															while ($row_fetch_pro_id_3 = (mysql_fetch_array($sql_fetch_pro_id_3)) ){
																	extract($row_fetch_pro_id_3);

																	if($key === $order_number) {
																		$show_under_review_date = " <br/> <span><i class='fa fa-calendar'> &nbsp; $submission_date  &nbsp;</i></span> ";
																	}


															}


														// print_r($dates);

													?>

														<?php echo $show_under_review_date; ?>

													<?php unset($show_under_review_date); } ?>

													</li> 

												<?php $d++; 

											} // END OF FOREACH ?> 

											<?php if(count($under_review) === 0 ) {
												echo "<li class='no_result_found'> No results found </li>";
											} ?>

										</ul>

										<!-- <button class="add-card-btn btn">Add a card</button> -->

									</div>



									<div class="list">


										<h3 class="list-title"> <i class="fa fa-check"></i> <span class="text-center"> Completed </span>  </h3>

										<ul class="list-items ul_completed">

											<?php

											$e = 0;
											foreach (array_combine($completed, $project_assigned_to_5) as $key => $value) {

											?>  

													<li>  <?php echo "$key "; ?> 

													<?php

													$name1 = explode(", ", $value);
													$get_img = "'" . implode ( "', '", $name1 ) . "'";

													// echo "$get_img";

													$sql_fetch_pic = mysql_query(" SELECT * FROM `employees` where full_name in ($get_img);  ", $connect_db);
													while ($row_fetch_pic = (mysql_fetch_array($sql_fetch_pic)) ){
														extract($row_fetch_pic);


													if(empty($avatar)) {
														$avatar_1 = "img/avatars/avatar.png";
													} else {
														$avatar_1 = "img/avatars/$avatar";
													}

									                if(file_exists($avatar_1)) { 
									                	$avatar = $avatar_1; 
									                } 

													?>

														
														<img data-toggle="tooltip" title="<?php echo $full_name; ?>" src="<?php echo $avatar; ?>" alt="<?php echo $avatar; ?>"  />


													<?php } ?>	

													<?php

														$sql_fetch_dates_4 = mysql_query(" SELECT * FROM   project_revision where customer_feedback_date != ''  ", $connect_db);
														while ($row_fetch_dates_4 = (mysql_fetch_array($sql_fetch_dates_4)) ){
																extract($row_fetch_dates_4);


															$sql_fetch_pro_id_4 = mysql_query(" SELECT * FROM   projects where pk_project_id = '$fk_project_id' and status = 'Completed'  ", $connect_db);
															while ($row_fetch_pro_id_4 = (mysql_fetch_array($sql_fetch_pro_id_4)) ){
																	extract($row_fetch_pro_id_4);

																	if($key === $order_number) {
																		$show_completed_date = "  <span><i class='fa fa-calendar'> &nbsp; $customer_feedback_date  &nbsp;</i></span> ";
																	}


															}


														// print_r($dates);

													?>

														<?php echo $show_completed_date; ?>

													<?php unset($show_completed_date); } ?>


													</li> 

												<?php $e++; 

											} // END OF FOREACH ?> 

											<?php if(count($completed) === 0 ) {
												echo "<li class='no_result_found'> No results found </li>";
											} ?>

										</ul>

										<!-- <button class="add-card-btn btn">Add a card</button> -->

									</div>

<!-- 									<div class="list">

										<h3 class="list-title"> Other </h3>

										<ul class="list-items">


											<li> EB30001 </li>
											<li> SI80003 </li>
											<li> F500004 </li>
											<li> R200022 </li>
											<li> R200023 </li>
											<li> N400010 </li>
											<li> PC60006 </li>
											<li> N400004 </li>
											<li> EB30001 </li>
											<li> SI80003 </li>
											<li> F500004 </li>
											<li> R200022 </li>
											<li> R200023 </li>
											<li> PC60002 </li>
											<li> R200011 </li>
											<li> R200012 </li>
											<li> R200013 </li>

										</ul>

										<button class="add-card-btn btn">Add a card</button>

									</div> -->

<!-- 									<div class="list">

										<h3 class="list-title">Web Dev YouTube Channels</h3>

										<ul class="list-items">
											<li>Adam Khoury</li>
											<li>Brad Hussey</li>
											<li>CSS-Tricks (Chris Coyier)</li>
											<li>Derek Banas</li>
											<li>DevTips (Travis Neilson)</li>
											<li>Free Code Camp</li>
											<li>Fun Fun Function (Mattias Petter Johansson)</li>
											<li>Google Chrome Developers</li>
											<li>Layout Land (Jen Simmons)</li>
											<li>Learn Code Academy</li>
											<li>Level Up Tuts (Scott Tolinski)</li>
											<li>Mackenzie Child</li>
											<li>Rachel Andrew</li>
											<li>The Net Ninja (Shaun Pelling)</li>
											<li>The New Boston (Bucky Roberts)</li>
											<li>Traversy Media (Brad Traversy)</li>
											<li>Wes Bos</li>
										</ul>

										<button class="add-card-btn btn">Add a card</button>

									</div> -->

<!-- 									<div class="list">

										<h3 class="list-title">CodePen Ideas</h3>

										<ul class="list-items">
											<li>Something cool with CSS Grid</li>
											<li>Something cool with CSS Flexbox</li>
											<li>Something cool with CSS animations</li>
											<li>Something cool with CSS gradients</li>
											<li>Something cool with CSS pseudo-elements</li>
											<li>Something cool with SVG</li>
											<li>Something cool with JavaScript</li>
											<li>Something cool with all of the above</li>
										</ul>

										<button class="add-card-btn btn">Add a card</button>

									</div> -->

<!-- 									<div class="list">

										<h3 class="list-title">Practise Website Ideas</h3>

										<ul class="list-items">
											<li>Airsoft/Paintballing Centre</li>
											<li>Bar/Pub</li>
											<li>Bicycle Shop/Repair</li>
											<li>Cafe/Coffee Shop</li>
											<li>Car Showroom/Garage/Repair/Parts</li>
											<li>Construction Company</li>
											<li>Fitness/Gym/Leisure Centre</li>
											<li>Nightclub</li>
											<li>Party Planning Company</li>
											<li>PC Build/Repair Service</li>
											<li>Portfolio/CV</li>
											<li>Real Estate/AirBnB</li>
											<li>Restaurant</li>
											<li>Skiing/Snowboarding Centre/Company</li>
											<li>Streaming Service for Movies/TV</li>
											<li>Streaming Service for Video Games</li>
											<li>Taxi Service</li>
											<li>Travel Agency</li>
											<li>Zoo/Safari Park</li>
										</ul>

										<button class="add-card-btn btn">Add a card</button>

									</div>
 -->
<!-- 									<div class="list">

										<h3 class="list-title">JavaScript Project Ideas</h3>

										<ul class="list-items">
											<li>Analog Clock</li>
											<li>Basic Quiz</li>
											<li>Bill/Cost Splitter</li>
											<li>Countdown Timer</li>
											<li>Form Validator</li>
											<li>Geolocation (Find places near you etc.)</li>
											<li>Gif Search</li>
											<li>Note Taking App</li>
											<li>Random Name Picker</li>
											<li>Secret Message Encoder/Decoder</li>
											<li>Sortable Image Gallery</li>
											<li>Sortable Table</li>
											<li>Tip Calculator</li>
											<li>To-Do List</li>
											<li>Unit Converter</li>
										</ul>

										<button class="add-card-btn btn">Add a card</button>

									</div> -->

									<!-- <button class="add-list-btn btn">Add a list</button> -->

								</section>
								<!-- End of lists container -->

								</div>

							</div>
						</div> <!-- box-block -->
					</div>
				</div>


			

<?php include $backend_footer_file; ?>

	



<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>






<!-- <script type="text/javascript" src="js/tables-datatable.js"></script> -->



<script type="text/javascript">
function loadReport() {

	var month = $('#choose_month').find(":selected").val();

    $("#animated_image").show();
    $("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo loading_data; ?></span>');

	$.post('load_dashboard_report.php', '&month_value='+month , function(response) {
		$("#animated_image").hide();
		$('#report_container').html(response);
	});
}	
</script>







<!-- delete onclick function -->
<script type="text/javascript">

function EditProject(order_number) {

	$.post('dashboard_editpart.php', '&order_number='+order_number, function(response){
		$('#edit_project_form').html(response);
	});

    $('#EditProjectModal').modal();

}



// $(".list-items li").click(function(){
$(document).on("click", ".list-items li", function(){

    window.project_id = $(this).attr("id");
    window.val = $(this).text();

    // console.log(window.project_id + ' ' + window.val);

});


function getNewStatus(new_status){


	console.log(new_status);	

	if(new_status == "Scope Clarity") {


	    $('#'+window.project_id).prependTo('.ul_scope_clarity').addClass('new_li');

	 //    $('#'+window.project_id).clone().appendTo('.ul_scope_clarity');
	 //    $('#'+window.project_id).remove();

		if ( $('.ul_scope_clarity li').length > 1 ) {
			$('.ul_scope_clarity .no_result_found').hide();
		} else {
			$('.ul_scope_clarity .no_result_found').show();
		}

	} 


	if(new_status == "To be started" || new_status == "To be started 1") {

	    $('#'+window.project_id).prependTo('.ul_to_be_started').addClass('new_li');

		// // alert('test');
	 //    $('#'+window.project_id).clone().appendTo('.ul_to_be_started');
	 //    $('#'+window.project_id).remove();

		if ( $('.ul_to_be_started li').length > 1 ) {
			$('.ul_to_be_started .no_result_found').hide();
		} else {
			$('.ul_to_be_started .no_result_found').show();
		}

	} 

	if(new_status == "In Progress 1" || new_status == "In Progress") {

		// alert('in Progress');

	    $('#'+window.project_id).prependTo('.ul_in_progress').addClass('new_li');

	 //    $('#'+window.project_id).clone().appendTo('.ul_in_progress');
	 //    $('#'+window.project_id).remove();

		if ( $('.ul_in_progress li').length > 1 ) {
			$('.ul_in_progress .no_result_found').hide();
		} else {
			$('.ul_in_progress .no_result_found').show();
		}

	}

	if(new_status == "Under Customer Review") {

		// alert('in Progress');

	    $('#'+window.project_id).prependTo('.ul_under_review').addClass('new_li');

	 //    $('#'+window.project_id).clone().appendTo('.ul_under_review');
	 //    $('#'+window.project_id).remove();

		if ( $('.ul_under_review li').length > 1 ) {
			$('.ul_under_review .no_result_found').hide();
		} else {
			$('.ul_under_review .no_result_found').show();
		}

	}


	if(new_status == "Completed") {

		// alert('in Progress');

	    $('#'+window.project_id).prependTo('.ul_completed').addClass('new_li');

	 //    $('#'+window.project_id).clone().appendTo('.ul_completed');
	 //    $('#'+window.project_id).remove();

		if ( $('.ul_completed li').length > 1 ) {
			$('.ul_completed .no_result_found').hide();
		} else {
			$('.ul_completed .no_result_found').show();
		}

	}


	window.project_id = "";
	window.val = "";

}

</script>




<!-- load project edit details in modal -->
<div id="EditProjectModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

				<form id="edit_project_form" name="edit_project_form" class="form-material material-primary">


				</form>

            </div>
        </div>
</div>




<script type="text/javascript">
$(document).ready(function(){

  $("#edit_project_form").validate({
    debug: false,
    submitHandler: function(form) {

    	var user_sel_status = $(".project_status").find(':selected').val();
    	var user_sel_status = user_sel_status.trim();

      	$("#animated_image").show();
      	$("#animated_image").fadeIn(400).html('<img src="<?php echo $file_path[37]; ?>" align="absmiddle">&nbsp;<span class="loading"><?php echo saving_data; ?></span>');

      	$.post('<?php echo $file_path[31]; ?>', $("#edit_project_form").serialize() + '&update_project_status=update_project_status' , function(response) {

			$("#animated_image").hide();
			$("#result_container").html(response);

			$.post("php/trigger_email.php");

      	}); // END OF POST REQUEST 

	    $('#EditProjectModal').modal('hide');

    	scrollToTop();
		getNewStatus(user_sel_status);


    } // END OF SUBMIT HANDLER
  });  // END OF REGISTRATION FORM

});
</script>









<script src="//cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.2/multiple-select.min.js"></script>


<!-- <script>
  init({
    title: 'selected/disabled options',
    desc: 'Multiple Select can take from selected and disabled options',
    links: ['multiple-select.css'],
    scripts: ['multiple-select.js']
  })
</script> -->



<script>
  // Here the mounted function is same as $(function() {})
$(document).ready(function(){
    $('select').multipleSelect({
    	filter: true
    })
});
</script>







<script type="text/javascript">
$(document).ready(function(){
	$('body').addClass('compact-sidebar');

	if ($(window).width() <= 800) {
		$('.lists-container').css('grid-auto-columns', '8rem');
	} else 	if ($(window).width() <= 900) {
		$('.lists-container').css('grid-auto-columns', '9rem');
	} else 	if ($(window).width() <= 1000) {
		$('.lists-container').css('grid-auto-columns', '11rem');
	} else 	if ($(window).width() <= 1100) {
		$('.lists-container').css('grid-auto-columns', '13rem');
	} else 	if ($(window).width() <= 1200) {
		$('.lists-container').css('grid-auto-columns', '15rem');
	} else 	if ($(window).width() <= 1300) {
		$('.lists-container').css('grid-auto-columns', '16rem');
	} else 	if ($(window).width() <= 1400) {
		$('.lists-container').css('grid-auto-columns', '17rem');
	} else 	if ($(window).width() <= 1500) {
		$('.lists-container').css('grid-auto-columns', '19rem');
	} else 	if ($(window).width() <= 1600) {
		$('.lists-container').css('grid-auto-columns', '20rem');
	} else 	if ($(window).width() <= 1700) {
		$('.lists-container').css('grid-auto-columns', '22rem');
	} else 	if ($(window).width() <  1800) {
		$('.lists-container').css('grid-auto-columns', '23rem');
	} else 	if ($(window).width() <  1850) {
		$('.lists-container').css('grid-auto-columns', '25rem');
	} else 	if ($(window).width() <= 1900) {
		$('.lists-container').css('grid-auto-columns', '25rem');
	} else 	if ($(window).width() > 1900) {
		$('.lists-container').css('grid-auto-columns', '26rem');
	} else 	if ($(window).width() > 1950) {
		$('.lists-container').css('grid-auto-columns', '26rem');
	}

});


$(".toggle-button").click(function(){


	if ($(window).width() <= 800) {
		$('.lists-container').css('grid-auto-columns', '8rem');
	} else 	if ($(window).width() <= 900) {

		if ($('body').hasClass("compact-sidebar")) {
			$('.lists-container').css('grid-auto-columns', '8rem');
		} else {
			$('.lists-container').css('grid-auto-columns', '9rem');
		}

	} else 	if ($(window).width() <= 1000) {

		if ($('body').hasClass("compact-sidebar")) {
			$('.lists-container').css('grid-auto-columns', '9rem');
		} else {
			$('.lists-container').css('grid-auto-columns', '11rem');
		}

	} else 	if ($(window).width() <= 1100) {

		if ($('body').hasClass("compact-sidebar")) {
			$('.lists-container').css('grid-auto-columns', '11rem');
		} else {
			$('.lists-container').css('grid-auto-columns', '13rem');
		}

	} else 	if ($(window).width() <= 1200) {

		if ($('body').hasClass("compact-sidebar")) {
			$('.lists-container').css('grid-auto-columns', '13rem');
		} else {
			$('.lists-container').css('grid-auto-columns', '15rem');
		}

	} else 	if ($(window).width() <= 1300) {

		if ($('body').hasClass("compact-sidebar")) {
			$('.lists-container').css('grid-auto-columns', '14rem');
		} else {
			$('.lists-container').css('grid-auto-columns', '16rem');
		}

	} else 	if ($(window).width() <= 1400) {

		if ($('body').hasClass("compact-sidebar")) {
			$('.lists-container').css('grid-auto-columns', '15rem');
		} else {
			$('.lists-container').css('grid-auto-columns', '17rem');
		}

	} else 	if ($(window).width() <= 1500) {

		if ($('body').hasClass("compact-sidebar")) {
			$('.lists-container').css('grid-auto-columns', '16rem');
		} else {
			$('.lists-container').css('grid-auto-columns', '19rem');
		}

	} else 	if ($(window).width() <= 1600) {

		if ($('body').hasClass("compact-sidebar")) {
			$('.lists-container').css('grid-auto-columns', '18rem');
		} else {
			$('.lists-container').css('grid-auto-columns', '20rem');
		}

	} else 	if ($(window).width() <= 1700) {

		if ($('body').hasClass("compact-sidebar")) {
			$('.lists-container').css('grid-auto-columns', '20rem');
		} else {
			$('.lists-container').css('grid-auto-columns', '23rem');
		}

	} else 	if ($(window).width() <= 1800) {

		if ($('body').hasClass("compact-sidebar")) {
			$('.lists-container').css('grid-auto-columns', '21rem');
		} else {
			$('.lists-container').css('grid-auto-columns', '23rem');
		}

	} else 	if ($(window).width() <= 1900) {

		if ($('body').hasClass("compact-sidebar")) {
			$('.lists-container').css('grid-auto-columns', '23rem');
		} else {
			$('.lists-container').css('grid-auto-columns', '25rem');
		}

	} else 	if ($(window).width() > 1900) {

		if ($('body').hasClass("compact-sidebar")) {
			$('.lists-container').css('grid-auto-columns', '23rem');
		} else {
			$('.lists-container').css('grid-auto-columns', '25rem');
		}

	} else 	if ($(window).width() > 1950) {

		if ($('body').hasClass("compact-sidebar")) {
			$('.lists-container').css('grid-auto-columns', '25rem');
		} else {
			$('.lists-container').css('grid-auto-columns', '26rem');
		}

	}




});

</script>


<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip(); 
});
</script>


<script type="text/javascript">
	$(".toggleBtn").click(function(){
		$(".toggleDiv").toggle();
	});
</script>