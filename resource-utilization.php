<?php 
include "php/global_constants.php"; 
block_unknown_user();
?>

<?php include $backend_header_file; ?>

<link rel="stylesheet" href="vendor/DataTables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="vendor/DataTables/Buttons/css/buttons.bootstrap4.min.css">

<style type="text/css">
.error{ color: red; }
.dt-buttons{ float: right !important; margin: 0 0 0 20px; }
.buttons-copy{display: none}
/*.buttons-excel::before { Content: 'Download as ' }
.buttons-csv::before { Content: 'Download as ' }
.buttons-pdf::before { Content: 'Download as ' }
*/td,th {
	text-align: center;
}
#table-2{
	width: auto !important;
}
</style>


			<div class="site-content">
				<!-- Content -->
				<div class="content-area py-1">
					<div class="container-fluid">
						<h4> Resource Utilization </h4>
						<ol class="breadcrumb no-bg mb-1">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<li class="breadcrumb-item active">Resource Utilization</li>
						</ol>


<!-- 						<nav class="box box-block bg-white">
							<h5 class="mb-1"> Resource Utilization - Report </h5>

							<div class="row">
								<div class="col-sm-2">
									<select id="choose_month" name="choose_month" class="form-control" onchange="loadReport()">
										<?php echo allMonths(); ?>
										<option value="all"> All </option>
									</select>
								</div>
							</div>

						</nav>
 -->

						<div id="animated_image"></div>
						<div id="result_container"></div>


						<div class="box-block bg-white">

							<table class="table table-striped table-bordered dataTable" id="table-2">
								<thead>
									<tr>
										<th> User </th>
										<th> Order Number </th>
										<th> Project Name </th>
										<th> Status </th>
										<th> Date </th>
									</tr>
								</thead>
								<tbody>





									<?php 
										$sql_fetch_date = mysql_query("SELECT distinct pk_project_id,order_number, assign_to,project_name,status,order_recieved_date,actual_completion_date  FROM `$gd`.`projects`  where order_number != 'Leave' and order_number != 'Training'  order by assign_to   ", $connect_db);

										while ($row_fetch_date = (mysql_fetch_array($sql_fetch_date)) ){
										extract($row_fetch_date);


									?>			


										<tr>

											<td>  <?php echo $assign_to; ?> </td>

											<td>  <?php echo $order_number; ?> </td>

											<td>  <?php echo $project_name; ?> </td>

											<td>  <?php echo $status; ?> </td>


											<td>  
												<?php 
													if($status == "Completed") {
														echo $actual_completion_date; 
													} else if ($status == "Scope Clarity") {
														echo "$order_recieved_date";
													}

													else {

														$sql_fetch_date_1 = mysql_query("SELECT distinct scope_cleared_date, project_start_date  FROM `$gd`.`project_revision`  where fk_project_id = '$pk_project_id'  ", $connect_db);

														while ($row_fetch_date_1 = (mysql_fetch_array($sql_fetch_date_1)) ){
														extract($row_fetch_date_1);

															if($status == "To be started") {

																echo "   $scope_cleared_date   ";

															} else if($status == "In progress") {

																echo "  $project_start_date   ";

															} 


														}

													}
												?> 
											</td>






										</tr>

									<?php }  ?>





								</tbody>
							</table>

						</div> <!-- box-block -->

					</div>
				</div>

			</div>


<?php include $backend_footer_file; ?>

	


<script type="text/javascript">
$(document).ready(function(){
    $(".dt-buttons a").html("<i class='fa fa-download'></i> Excel ");  
});
</script>